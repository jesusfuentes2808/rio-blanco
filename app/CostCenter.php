<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CostCenter extends Model
{
    use SoftDeletes;
    //
    protected $table = "basic_cost_centers";
    protected $dates = ['deleted_at'];

    protected $fillable = [
    						'id',
    						'parent_id',
    						'project_id',
    						'company_id',
    						'cost_type_id',
    						'cost_center_type_id',
    						'code',
    						'name',
    						'unit_id',
    						'section_id',
    						'managements_id',
    						'work_group_id',
    						'status',
    						'supply_other_project',
                            'supply_transfer_project',
    ];

    public function unitMeasure()
    {
        return $this->belongsTo('App\UnitMeasure','unit_id');
    }

    public function management()
    {
        return $this->belongsTo('App\Management','managements_id');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function costType()
    {
        return $this->belongsTo('App\CostType');
    }

    public function costCenterType()
    {
        return $this->belongsTo('App\CostCenterType');
    }
}
