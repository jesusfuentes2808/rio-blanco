<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use SoftDeletes;

    protected $table = 'admin_companies';

    protected $dates = ['deleted_at'];
    
    protected $fillable = [
    	'name',
        'ruc',
        'slogan',
        'street',
        'street2',
        'reference',
        'state_id',
        'province_id',
        'district_id',
        'zip',
        'anotherCity',
        'website',
        'contact',
        'phone',
        'cellphone',
        'fax',
        'email',
        'register',
        'currency_id',
        'picture',
        'status',
    ];

    public function __construct()
    {
        //$this->table = 'admin_' . $this->getTable();
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\District');
    }
}
