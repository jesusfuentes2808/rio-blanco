<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleRate extends Model
{
    protected $fillable = ['name'];

    public function __construct()
    {
        $this->table = 'basic_' . $this->getTable();
    }

}
