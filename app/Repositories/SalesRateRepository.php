<?php

namespace App\Repositories;

use App\SaleRate;

class SalesRateRepository {

	public function create($data)
	{
		$saleRate = new SaleRate();
		$saleRate->name = $data['name'];
		$saleRate->save();
		
		return $saleRate;
	}

	public function find($id)
	{
		return SaleRate::find($id);
	}

	public function update($data, $id)
	{
		$s = SaleRate::find($id);

		$s->update($data);
	}

	public function destroy($id)
	{
		return SaleRate::destroy($id);
	}

}