<?php

namespace App\Repositories;

use App\ResponsibleType;

class ResponsibleTypesRepository {

	public function all()
	{
		return ResponsibleType::all();
	}

	public function create($data) 
	{
		return ResponsibleType::create($data);
	}

	public function find($id)
	{
		return ResponsibleType::find($id);
	}

	public function update($data, $id)
	{
		$responsibleType = ResponsibleType::find($id);

		return $responsibleType->update($data);
	}

	public function destroy($id)
	{
		return ResponsibleType::destroy($id);
	}
}
