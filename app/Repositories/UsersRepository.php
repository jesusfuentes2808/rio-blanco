<?php

namespace App\Repositories;

use App\User;

use Intervention\Image\ImageManagerStatic;
use DB;
use Exception;

class UsersRepository {

    public function user()
    {
        try {
            $user = User::with(['role'])->get();
            
            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function all()
    {
        return User::all();
    }
    
    public function store($request)
    {   
        try {
            DB::beginTransaction();

            $user = new User();
            
            $user->username = $request->username;
            $user->name = $request->name;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->password = \Hash::make($request->password);
            $user->signature = $request->signature?:'';
            $user->picture = '';
            $user->picture_name_random = '';
            $user->role_id = $request->role_id;
            
            if($request->hasFile('picture')){
                $filePhotoProfile = $request->file('picture');
                $photoProfile = $this->createFile($filePhotoProfile,'photo-profile');
                
                $user->picture             = $photoProfile['nameOriginal'];
                $user->picture_name_random = $photoProfile['name'];
            }

            

            $user->save();
            
            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function find($id)
    {
        try {
            $user = User::find($id);

            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
            
        }  catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function update($request, $id)
    {
        try {
            DB::beginTransaction();

            $user = User::find($id);
            
            if ($user->username != $request->username) {
                $otherUser = User::where([
                                            'username' => $request->username
                                        ]);

                if ($otherUser->count() > 0) {
                    return response(json_encode(['Ya existe un usuario con este username']), 422);
                }
            }
            
            $user->username = $request->username;
            $user->name = $request->name;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->signature = $request->signature?:'';
            $user->role_id = $request->role_id;

            if ($request->password) {
                $user->password = \Hash::make($request->password);
            }

            if($request->hasFile('picture')){
                $filePhotoProfile = $request->file('picture');
                $photoProfile = $this->createFile($filePhotoProfile,'photo-profile');
                
                $user->picture             = $photoProfile['nameOriginal'];
                $user->picture_name_random = $photoProfile['name'];
            }

            $user->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function where($look)
    {
        $user = User::orderBy('id', 'DESC');

        foreach ($look as $key => $value) {
            $user->where($key, $value);
        }

        return $user->get();
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $userDelete = User::find($id);
            $user = $userDelete;
            $userDelete->delete();
            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
        
    }

    public function createFile($file, $filePrefix = 'photo', $index = 0)
    {
        $arrFile = array();
        $file = ($file) ? $file : null;
        if ($file === null) {
            return null;
        }
        list($width, $height) = getimagesize($file);
        $fileNameOriginal = $file->getClientOriginalName();
        $fileExtension    = $file->getClientOriginalExtension();
        $fileName         = $filePrefix.'-' . (time()+$index) . '.' . $fileExtension;
        if($filePrefix === 'photo-gallery'){
            $filePath     = $file->storeAs('public/event/thumbs', $fileName);
            $this->imageManagerStatic('fit-gallery', $fileName, 160, 167);
        }else{
            $filePath     = $file->storeAs('public/event', $fileName);
            if ($filePrefix === 'photo-profile') {
                $this->imageManagerStatic('fit', $fileName, 150, 150);
            } else {
                $this->imageManagerStatic('fit', $fileName, 70, 70);
                $this->imageManagerStatic('fit', $fileName, 118, 118);
                $this->imageManagerStatic('fit', $fileName, 130, 130);
                $this->imageManagerStatic('fit', $fileName, 186, 186);
            }
        }
        $arrFile['nameOriginal'] = $fileNameOriginal;
        $arrFile['extension']    = $fileExtension;
        $arrFile['name']         = $fileName;
        $arrFile['path']         = $filePath;
        return $arrFile;
    }

    public function imageManagerStatic($type, $fileName, $width = null, $height = null, $x = null, $y = null) 
    {
        $prefix = $width.'x'.$height.'_';
        switch ($type) {
            case 'resizeRatio':
                ImageManagerStatic::make(storage_path("app/public/event/").$fileName)
                ->resize($width, $height, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })
                ->save(storage_path("app/public/event/").$prefix.$fileName);
                break;
            case 'crop':
                # code...
                break;
            case 'fit':
                ImageManagerStatic::make(storage_path("app/public/event/").$fileName)
                ->fit($width, $height, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })
                ->save(storage_path("app/public/event/").$prefix.$fileName);
                break;
            case 'fit-gallery':
                ImageManagerStatic::make(storage_path("app/public/event/thumbs/").$fileName)
                ->fit($width, $height, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })
                ->save(storage_path("app/public/event/thumbs/").$prefix.$fileName);
                break;
            default:
                # code...
                break;
        }
    }

}
