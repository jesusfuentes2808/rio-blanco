<?php

namespace App\Repositories;

use App\TypeAddressStreet;

class TypeAddressStreetRepository {

	public function all()
	{
		return TypeAddressStreet::all();
	}

	public function create($data) 
	{
		//return CostType::create($data);
	}

	public function find($id)
	{
		//return CostType::find($id);
	}

	public function update($data, $id)
	{
		/*$costType = CostType::find($id);

		return $costType->update($data);*/
	}

	public function destroy($id)
	{
		//return CostType::destroy($id);
	}
}
