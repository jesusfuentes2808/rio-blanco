<?php

namespace App\Repositories;

use App\TypeAddress;

class TypeAddressesRepository {

	public function all()
	{
		return TypeAddress::all();
	}

	public function create($data)
	{
		return TypeAddress::create($data);
	}

	public function find($id)
	{
		return TypeAddress::find($id);
	}

	public function update($id, $data)
	{
		$a = $this->find($id);
		$a->update($data);
	}

	public function destroy($id)
	{
		return TypeAddress::destroy($id);
	}
}