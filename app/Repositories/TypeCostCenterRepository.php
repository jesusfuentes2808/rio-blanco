<?php

namespace App\Repositories;

use App\CostCenterType;

class TypeCostCenterRepository {

	public function all()
	{
		return CostCenterType::all();
	}

	public function create($data) 
	{
		return CostCenterType::create($data);
	}

	public function find($id)
	{
		return CostCenterType::find($id);
	}

	public function update($data, $id)
	{
		$costCenterType = CostCenterType::find($id);

		return $costCenterType->update($data);
	}

	public function destroy($id)
	{
		return CostCenterType::destroy($id);
	}
}