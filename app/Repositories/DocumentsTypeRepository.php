<?php

namespace App\Repositories;

use App\DocumentType;

class DocumentsTypeRepository {

	public function create($data)
	{	
		$documentType = new DocumentType();
		
		$documentType->code = $data['code'];
		$documentType->name = $data['name'];
		$documentType->short_name = $data['short_name'];
		
		$documentType->save();
		
		return $documentType;
	}

	public function find($id)
	{
		return DocumentType::find($id);
	}

	public function update($data, $id)
	{
		$d = DocumentType::find($id);
		$d->update($data);
	}

	public function destroy($id)
	{
		return DocumentType::destroy($id);
	}
}