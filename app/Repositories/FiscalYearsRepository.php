<?php

namespace App\Repositories;

use App\FiscalYear;

use Carbon\Carbon;
use DB;
use Exception;

class FiscalYearsRepository {
	
	public function fiscalYears()
    {
        try {

            $fiscalYear = FiscalYear::get();
            return response()->json([
                'status' => 'success',
                'data' => $fiscalYear
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function find($id)
    {
        try {

            $fiscalYear = FiscalYear::find($id);
            $date = new Carbon($fiscalYear->date_from);
            
            $fiscalYear->currentYear = $date->year;

            return response()->json([
                'status' => 'success',
                'data' => $fiscalYear
            ], 200);
            
        }  catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function store($request)
    {
        try {
            $fiscalYear = new FiscalYear();

            if(isset($request->year) && is_numeric($request->year)){
                $fiscalYear->code      = $request->code;
                $fiscalYear->name      = $request->name;
                $fiscalYear->date_from = $request->year.'-01-01';
                $fiscalYear->date_to   = $request->year.'-12-31';
                $fiscalYear->status    = ($request->status)? 1 : 0;
                $fiscalYear->save();
            }

            return response()->json([
                'status' => 'success',
                'data' => $fiscalYear
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function update($request,$id)
    {
        try {
            
            $fiscalYear = FiscalYear::find($id);
            
            if($fiscalYear->code != $request->code){
                $fiscalYearC = FiscalYear::where('code',$request->code);        
                if($fiscalYearC->count() == 0){

                    $fiscalYear->code      = $request->code;
                } else {
                    throw new Exception('The code has already been taken.');
                }

            }


            $fiscalYear->name      = $request->name;
            $fiscalYear->status    = ($request->status)? 1 : 0;

            $fiscalYear->save();

            return response()->json([
                'status' => 'success',
                'data' => $fiscalYear
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function currentYear()
    {   

        $fiscalYear = FiscalYear::select(DB::raw('YEAR(date_from) as annio'))->get()->pluck('annio')->toArray();
        
        try {
            $year = date("Y");
            $listYear = array();
            
            for ($i=($year-3); $i <=($year+3) ; $i++) { 
                if(!in_array($i,$fiscalYear)){
                    array_push($listYear,['year' => $i]);    
                }
            }

            return response()->json([
                'status' => 'success',
                'data' => $listYear,
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }
    
}