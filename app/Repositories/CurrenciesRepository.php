<?php

namespace App\Repositories;

use App\Currency;

class CurrenciesRepository {

    public function all()
    {
        return Currency::all();
    }

    public function create($data)
    {
        return Currency::create($data);
    }

    public function find($id)
    {
    	return Currency::find($id);
    }

    public function update($id, $data)
    {
        Currency::where('id', $id)->update($data);
    }
}
