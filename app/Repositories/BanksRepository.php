<?php

namespace App\Repositories;

use App\Bank;

class BanksRepository {

	public function create($request) 
	{
		$data = [
			'country_id' => $request->country_id,
			'swift_code' => $request->swift_code,
			'vat_code' => $request->vat_code,
			'name' => $request->name,
			'city' => $request->city,
			'branch_code' => $request->branch_code,
			'address' => $request->address,
			'zip' => $request->zip,
			'active' => $request->status
		];

		return Bank::create($data);
	}

	public function find($id)
	{
		return Bank::find($id);
	}

	public function update($request, $id)
	{
		$bank = Bank::find($id);

		return $bank->update($request->all());
	}

	public function destroy($id)
	{
		return Bank::destroy($id);
	}
}