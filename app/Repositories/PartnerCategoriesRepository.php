<?php

namespace App\Repositories;

use App\PartnerCategory;

class PartnerCategoriesRepository {

	public function all()
	{
		return PartnerCategory::all();
	}

	public function create($data)
	{

		$partnerCategory = new PartnerCategory();
		$partnerCategory->name = $data['name'];
		$partnerCategory->save();

		return $partnerCategory;
	}

	public function find($id)
	{
		return PartnerCategory::find($id);
	}

	public function update($data, $id)
	{
		$p = PartnerCategory::find($id);
		return $p->update($data);
	}

	public function destroy($id)
	{
		return PartnerCategory::destroy($id);
	}
}