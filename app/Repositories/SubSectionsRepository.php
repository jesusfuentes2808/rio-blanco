<?php

namespace App\Repositories;

use App\SubSection;

class SubSectionsRepository {

	public function all()
	{
		return SubSection::with('section')->get();
	}

	public function create($data) 
	{
		return SubSection::create($data);
	}

	public function find($id)
	{
		return SubSection::with('section')->find($id);
	}

	public function update($data, $id)
	{
		$SubSection = SubSection::find($id);

		return $SubSection->update($data);
	}

	public function destroy($id)
	{
		return SubSection::destroy($id);
	}
}
