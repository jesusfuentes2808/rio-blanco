<?php

namespace App\Repositories;

use App\TypeContact;

class DataContactsRepository {

	public function all()
	{
		return TypeContact::all();
	}

	public function create($data)
	{
		return TypeContact::create($data);
	}

	public function find($id)
	{
		return TypeContact::find($id);
	}

	public function update($id, $data)
	{
		$d = $this->find($id);
		$d->update($data);
	}

	public function destroy($id)
	{
		return TypeContact::destroy($id);
	}
}