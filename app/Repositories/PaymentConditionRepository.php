<?php

namespace App\Repositories;

use App\PaymentCondition;

class PaymentConditionRepository {

	public function create($data)
	{
		return PaymentCondition::create($data);
	}

	public function find($id)
	{
		return PaymentCondition::find($id);
	}

	public function update($data, $id)
	{
		$p = $this->find($id);

		return $p->update($data);
	}

	public function destroy($id)
	{
		return PaymentCondition::destroy($id);
	}
}