<?php

namespace App\Repositories;

use App\TypeAddressZone;

class TypeAddressZoneRepository {

	public function all()
	{
		return TypeAddressZone::all();
	}

	public function create($data) 
	{
		//return CostType::create($data);
	}

	public function find($id)
	{
		//return CostType::find($id);
	}

	public function update($data, $id)
	{
		/*$costType = CostType::find($id);

		return $costType->update($data);*/
	}

	public function destroy($id)
	{
		//return CostType::destroy($id);
	}
}
