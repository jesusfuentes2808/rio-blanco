<?php

namespace App\Repositories;

use App\Client;
use App\ClientContact;
use App\BankAccount;
use App\PartnerContact;
use App\PartnerAddress;

class ClientsRepository {

    public function all()
    {   
        
        return Client::get();

        return Client::orderBy('id', 'DESC')->with(['state', 'province', 'district', 'partnerCategory', 'contact', 'contacts', 'accounts'])->get();
    }

    public function create($data)
    {
        return Client::create($data);
    }

    public function find($id)
    {
    	return Client::where('id', $id)->with(['state', 'province', 'district', 'partnerCategory', 'contact', 'contacts', 'addresses', 'accounts'])->first();
    }

    public function createContact($data, $contact = null)
    {
        if (!$contact) {
            ClientContact::create($data);

            return;
        }

        ClientContact::where('client_id', $contact->client_id)->update($data);
    }

    public function createContacts($contacts)
    {
        foreach ($contacts as $contact) {
            PartnerContact::create($contact);
        }

        return true;
    }

    public function createAddresses($addresses)
    {
        foreach ($addresses as $address) {
            PartnerAddress::create($address);
        }

        return true;
    }

    public function createBankAccount($bankAccounts)
    {
        foreach ($bankAccounts as $bankAccount) {
            BankAccount::create($bankAccount);
        }

        return true;
    }

    public function deleteReference($data, $reference)
    {
        if (count($data) < 1) {
            return;
        }

        switch ($reference) {
            case 'deleteAddresses':
                PartnerAddress::destroy($data);
            break;

            case 'deleteContacts':
                PartnerContact::destroy($data);
            break;

            case 'deleteBanks':
                BankAccount::destroy($data);
            break;
        }
    }
}
