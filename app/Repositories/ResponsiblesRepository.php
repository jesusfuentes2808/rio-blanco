<?php

namespace App\Repositories;

use App\Responsible;

class ResponsiblesRepository {

    public function all()
    {
        return Responsible::with('type')->get();
    }

    public function create($data)
    {
        return Responsible::create($data);
    }

    public function find($id)
    {
    	return Responsible::with('type')->find($id);
    }

    public function update($data, $id)
    {
        Responsible::where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        return Responsible::destroy($id);
    }
}
