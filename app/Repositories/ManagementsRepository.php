<?php

namespace App\Repositories;

use App\Management;

class ManagementsRepository {

	public function all()
	{
		return Management::all();
	}

	public function create($data) 
	{
		return Management::create($data);
	}

	public function find($id)
	{
		return Management::find($id);
	}

	public function update($data, $id)
	{
		$management = Management::find($id);

		return $management->update($data);
	}

	public function destroy($id)
	{
		return Management::destroy($id);
	}
}
