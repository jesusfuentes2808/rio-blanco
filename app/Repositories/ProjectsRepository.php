<?php

namespace App\Repositories;

use App\Project;

use DB;

class ProjectsRepository {

	public function all()
	{
		return Project::with('clients')
			->with('typeProjects')
				->with('users')
					->get();
	}

	public function create($data) 
	{
		//return Project::create($data);

		return DB::table('project_projects')->insert($data);
	}

	public function find($id)
	{
		return Project::find($id);
	}

	public function update($data, $id)
	{
		//$project = Project::find($id);

		$project = DB::table('project_projects')->where('id', $id);

		return $project->update($data);
	}

	public function destroy($id)
	{
		return Project::destroy($id);
	}
}