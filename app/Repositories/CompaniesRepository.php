<?php

namespace App\Repositories;

use App\Company;

use Intervention\Image\ImageManagerStatic;

use DB;

class CompaniesRepository {

    public function all()
    {
        return Company::with(['state', 'province', 'district'])->whereNull('deleted_at')->orderBy('id', 'DESC')->get();
    }

    public function allParent()
    {
        return Company::whereNull('parent_id')->whereNull('deleted_at')->orderBy('id', 'DESC')->get();
    }

    public function store($request)
    {   
        
        try {

            DB::beginTransaction();
            $company = new Company();

            $company->name                 = $request->name;
            $company->ruc                  = $request->ruc;        
            $company->slogan               = $request->slogan?:'';        
            $company->street               = $request->street;
            $company->street2              = $request->street2?:'';
            $company->zone_reference       = $request->reference?:'';
            $company->state_id             = $request->state_id;
            $company->province_id          = $request->province_id;
            $company->district_id          = $request->district_id;
            $company->zip                  = $request->zip?:'';
            $company->additional_reference = $request->anotherCity?:'';
            $company->website              = $request->website;
            $company->contact              = $request->contact;
            $company->phone                = $request->phone;
            $company->cellphone            = $request->cellphone;
            $company->fax                  = $request->fax?:'';
            $company->email                = $request->email;
            $company->register             = $request->register;
            $company->currency_id          = $request->currency_id;
            $company->picture              = '';
            $company->status               = $request->status;

            $company->street_type_id       = $request->viaType;
            $company->zone_type_id         = $request->zoneType;
            $company->address_number       = $request->addressNumber;
            $company->address_inside       = $request->addressDepartament;
            //$company->streetReference      = $request->anotherCity;

            $company->parent_id = ($request->matrix!='-1')?$request->matrix:null;
            $company->header_report = $request->header;
            $company->header_report1 = $request->header1;        
            $company->header_report2 = $request->header2;
            $company->signature = $request->signature;
            $company->notes = $request->notes;        
            $company->footer_report = $request->footer;
            $company->footer_report1 = $request->footer1;

            if($request->hasFile('picture')){
                $filePhotoProfile = $request->file('picture');
                $photoProfile = $this->createFile($filePhotoProfile,'photo-company');
                
                $company->picture = $photoProfile['name'];
            }

            $company->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => $company
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function update($request)
    {   
        
        try {

            DB::beginTransaction();

            $company = Company::find($request->id);

            $company->name                 = $request->name;
            $company->ruc                  = $request->ruc;        
            $company->slogan               = $request->slogan?:'';        
            $company->street               = $request->street;
            $company->street2              = $request->street2?:'';
            $company->zone_reference       = $request->reference?:'';
            $company->state_id             = $request->state_id;
            $company->province_id          = $request->province_id;
            $company->district_id          = $request->district_id;
            $company->zip                  = $request->zip?:'';
            $company->additional_reference = $request->anotherCity?:'';
            $company->website              = $request->website;
            $company->contact              = $request->contact;
            $company->phone                = $request->phone;
            $company->cellphone            = $request->cellphone;
            $company->fax                  = $request->fax?:'';
            $company->email                = $request->email;
            $company->register             = $request->register;
            $company->currency_id          = $request->currency_id;
            $company->picture              = '';
            $company->status               = $request->status;

            $company->street_type_id       = $request->viaType;
            $company->zone_type_id         = $request->zoneType;
            $company->address_number       = $request->addressNumber;
            $company->address_inside       = $request->addressDepartament;

            $company->parent_id = ($request->matrix!='-1')?$request->matrix:null;
            $company->header_report = $request->header;
            $company->header_report1 = $request->header1;        
            $company->header_report2 = $request->header2;
            $company->signature = $request->signature;
            $company->notes = $request->notes;        
            $company->footer_report = $request->footer;
            $company->footer_report1 = $request->footer1;

            if($request->hasFile('picture')){
                $filePhotoProfile = $request->file('picture');
                $photoProfile = $this->createFile($filePhotoProfile,'photo-company');
                
                $company->picture = $photoProfile['name'];
            }

            $company->save();

            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => $company
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function find($id)
    {
        try {
            $user = Company::where('id', $id)->with(['state', 'province', 'district'])->first();

            return response()->json([
                'status' => 'success',
                'data' => $user
            ], 200);
            
        }  catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }
    	

    public function delete($id){         
        /*if ($company->register) {
            unlink(public_path() . '/files/companies/' . $company->register);
        }*/

        try {
            DB::beginTransaction();
            $companyDelete = Company::find($id);
            $company = $companyDelete;
            $companyDelete->delete();
            DB::commit();

            return response()->json([
                'status' => 'success',
                'data' => $company
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }
    

    public function createFile($file, $filePrefix = 'photo', $index = 0)
    {
        $arrFile = array();
        $file = ($file) ? $file : null;
        if ($file === null) {
            return null;
        }
        list($width, $height) = getimagesize($file);
        $fileNameOriginal = $file->getClientOriginalName();
        $fileExtension    = $file->getClientOriginalExtension();
        $fileName         = $filePrefix.'-' . (time()+$index) . '.' . $fileExtension;
        if($filePrefix === 'photo-gallery'){
            $filePath     = $file->storeAs('public/event/thumbs', $fileName);
            $this->imageManagerStatic('fit-gallery', $fileName, 160, 167);
        }else{
            $filePath     = $file->storeAs('public/company', $fileName);
            if ($filePrefix === 'photo-company') {
                $this->imageManagerStatic('fit', $fileName, 150, 150);
            } else {
                $this->imageManagerStatic('fit', $fileName, 70, 70);
                $this->imageManagerStatic('fit', $fileName, 118, 118);
                $this->imageManagerStatic('fit', $fileName, 130, 130);
                $this->imageManagerStatic('fit', $fileName, 186, 186);
            }
        }
        $arrFile['nameOriginal'] = $fileNameOriginal;
        $arrFile['extension']    = $fileExtension;
        $arrFile['name']         = $fileName;
        $arrFile['path']         = $filePath;
        return $arrFile;
    }

    public function imageManagerStatic($type, $fileName, $width = null, $height = null, $x = null, $y = null) 
    {
        $prefix = $width.'x'.$height.'_';
        switch ($type) {
            case 'resizeRatio':
                ImageManagerStatic::make(storage_path("app/public/company/").$fileName)
                ->resize($width, $height, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })
                ->save(storage_path("app/public/company/").$prefix.$fileName);
                break;
            case 'crop':
                # code...
                break;
            case 'fit':
                ImageManagerStatic::make(storage_path("app/public/company/").$fileName)
                ->fit($width, $height, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })
                ->save(storage_path("app/public/company/").$prefix.$fileName);
                break;
            case 'fit-gallery':
                ImageManagerStatic::make(storage_path("app/public/company/thumbs/").$fileName)
                ->fit($width, $height, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })
                ->save(storage_path("app/public/company/thumbs/").$prefix.$fileName);
                break;
            default:
                # code...
                break;
        }
    }
}
