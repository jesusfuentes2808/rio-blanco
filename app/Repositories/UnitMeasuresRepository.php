<?php

namespace App\Repositories;

use App\UnitMeasure;

class UnitMeasuresRepository {

    public function all()
    {
        return UnitMeasure::with('category')->get();
    }

    public function create($data)
    {
        return UnitMeasure::create($data);
    }

    public function find($id)
    {
    	return UnitMeasure::find($id);
    }

    public function update($data, $id)
    {
        UnitMeasure::where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        return UnitMeasure::destroy($id);
    }
}
