<?php

namespace App\Repositories;

use App\Section;

class SectionsRepository {

	public function all()
	{
		return Section::all();
	}

	public function create($data)
	{
		return Section::create($data);
	}

	public function find($id)
	{
		return Section::find($id);
	}

	public function update($data, $id)
	{
		$d = Section::find($id);
		$d->update($data);
	}

	public function destroy($id)
	{
		return Section::destroy($id);
	}
}