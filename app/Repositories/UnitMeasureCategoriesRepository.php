<?php

namespace App\Repositories;

use App\UnitMeasureCategory;

class UnitMeasureCategoriesRepository {

    public function all()
    {
        return UnitMeasureCategory::all();
    }

    public function create($data)
    {
        return UnitMeasureCategory::create($data);
    }

    public function find($id)
    {
    	return UnitMeasureCategory::find($id);
    }

    public function update($data, $id)
    {
        UnitMeasureCategory::where('id', $id)->update($data);
    }

    public function destroy($id)
    {
        return UnitMeasureCategory::destroy($id);
    }
}
