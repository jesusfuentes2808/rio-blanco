<?php

namespace App\Repositories;

use App\Period;

use Carbon\Carbon;
use DB;
use Exception;

class PeriodsRepository {
	
    public function getById($request)
    {
        try {

            $periods = Period::where('fiscalyear_id',$request->id)->get();
            
            return response()->json([
                'status' => 'success',
                'data' => $periods
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

	public function periods()
    {
        try {
            $periods = Period::get();
            
            return response()->json([
                'status' => 'success',
                'data' => $periods
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function find($id)
    {
        try {
            $period = Period::find($id);

            return response()->json([
                'status' => 'success',
                'data' => $period
            ], 200);
            
        }  catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function store($request)
    {
        try {
            
            $dateTempFrom = $request->date_from_format;
            $dateTempTo = $request->date_to_format;
            
            $periodExist = Period::where('fiscalyear_id',$request->idFiscalYear)->get();

            $flag = true;
            foreach ($periodExist as $key => $value) {
                $iniDate = Carbon::parse($value->date_from);
                $endDate = Carbon::parse($value->date_to);
                
                if($dateTempFrom >= $iniDate && $dateTempFrom <= $endDate){
                    $flag = false;
                    break;
                }

                if($dateTempTo >= $iniDate && $dateTempTo <= $endDate){
                    $flag = false;
                    break;
                }
                /*
                echo "--------------------------------<br/>";
                echo "TEMP: INICIO: ".$iniDate."<br/>";
                echo "TEMP: FIN: ".$endDate."<br/>";
                // Comparar si fecha es mayor y menor a end Date
                if($dateTempFrom >= $iniDate && $dateTempFrom <= $endDate){
                    echo "NO PASA INI: ".$dateTempFrom."<br>";
                    $flag = false;
                    break;
                } else {
                    echo "PASA INI: ".$dateTempFrom."<br>";
                }

                if($dateTempTo >= $iniDate && $dateTempTo <= $endDate){
                    echo "NO PASA END: ".$dateTempTo."<br>";
                    $flag = false;
                    break;
                } else {
                    echo "PASA END: ".$dateTempTo."<br>";
                    
                }
                echo "--------------------------------<br/>";
                //dump($iniDate);
                //dump($endDate);
                */
            }

            if($flag){
                $period = new Period();
            
                $period->code          = $request->code;
                $period->name          = $request->name;
                $period->date_from     = $request->date_from_format;
                $period->date_to       = $request->date_to_format;
                $period->status        = 1;
                $period->company_id    = 1;
                $period->fiscalyear_id = $request->idFiscalYear;
                
                $period->save();
            } else {
                throw new Exception('Date Interval Not Valid');
            }
            
            return response()->json([
                'status' => 'success',
                'data' => $period
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function update($request,$id)
    {
        try {
            $dateTempFrom = $request->date_from_format;
            $dateTempTo = $request->date_to_format;
            
            $period = Period::find($request->id);

            $flag = true;
            if(($request->date_from_format !== $period->date_from) || ($request->date_to_format !== $period->date_to)){
                $periodExist = Period::where('fiscalyear_id',$request->idFiscalYear)->get();

                foreach ($periodExist as $key => $value) {
                    $iniDate = Carbon::parse($value->date_from);
                    $endDate = Carbon::parse($value->date_to);
                    
                    if($dateTempFrom >= $iniDate && $dateTempFrom <= $endDate){
                        $flag = false;
                        break;
                    }

                    if($dateTempTo >= $iniDate && $dateTempTo <= $endDate){
                        $flag = false;
                        break;
                    }
                    
                }    
            }
            

            if($flag){
                $period = Period::find($request->id);
                
                if($period->code != $request->code){
                    $validateCodeP = Period::where('code',$request->code)
                                            ->where('fiscalyear_id', $request->idFiscalYear);        
                    if($validateCodeP->count() == 0){

                        $period->code      = $request->code;
                    } else {
                        throw new Exception('The code has already been taken.');
                    }

                }

                $period->code          = $request->code;
                $period->name          = $request->name;
                $period->date_from     = $request->date_from_format;
                $period->date_to       = $request->date_to_format;
                
                $period->fiscalyear_id = $request->idFiscalYear;
                
                $period->save();
            } else {
                throw new Exception('The date range has already been selected');
            }
            
            return response()->json([
                'status' => 'success',
                'data' => $period
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function delete($id){
        try {
            DB::beginTransaction();
            $periodDelete = Period::find($id);
            $period = $periodDelete;
            $periodDelete->delete();
        
            DB::commit();
            return response()->json([
                'status' => 'success',
                'data' => $period
            ], 200);
            
        }  catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }

    public function currentYear()
    {   

        $fiscalYear = FiscalYear::select(DB::raw('YEAR(date_from) as annio'))->get()->pluck('annio')->toArray();
        
        try {
            $year = date("Y");
            $listYear = array();
            
            for ($i=($year-3); $i <=($year+3) ; $i++) { 
                if(!in_array($i,$fiscalYear)){
                    array_push($listYear,['year' => $i]);    
                }
            }

            return response()->json([
                'status' => 'success',
                'data' => $listYear,
            ], 200);
        } catch (Exception $e) {
            
            return response()->json([
                'status' => 'error',
                'data' => $e->getCode(),
                'message' => $e->getMessage()
            ], 400);
        }
    }
    
}