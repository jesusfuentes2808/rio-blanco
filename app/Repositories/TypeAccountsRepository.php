<?php

namespace App\Repositories;

use App\BankTypeAccount;

use DB;

class TypeAccountsRepository {


	public function create($data)
	{
		//return BankTypeAccount::create($data);
		return DB::table('admin_bank_type_accounts')->insert($data);
	}

	public function find($id)
	{
		return BankTypeAccount::find($id);
	}

	public function update($data, $id)
	{
		$t = BankTypeAccount::find($id);
		$t->update($data);
	}

	public function destroy($id)
	{
		BankTypeAccount::destroy($id);
	}	
 
}