<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeContact extends Model
{
    //protected $table = 'admin_type_contacts';
    //TODO
	//protected $table = 'admin_contacts_types';
	protected $table = 'admin_contacts_type';

    protected $fillable = ['name'];
}
