<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankTypeAccount extends Model
{
    protected $fillable = ['name'];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }

}
