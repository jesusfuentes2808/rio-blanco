<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerAddress extends Model
{
    protected $fillable = [
    	'partner_id',
        'type',
        'address',
        'address_aditional',
        'reference',
        'city',
        'zip',
        'state',
        'province',
        'district',
    ];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }
}
