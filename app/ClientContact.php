<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientContact extends Model
{
    protected $fillable = [
    	'client_id',
    	'name',
        'phone',
        'fax',
        'cellphone',
        'email',
        'web'
    ];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }

}
