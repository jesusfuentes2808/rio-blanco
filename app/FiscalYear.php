<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FiscalYear extends Model
{
    //
    protected $table = 'admin_fiscalyear';
    
    protected $fillable = ['id','code','name','date_to','date_from','status'];
}
