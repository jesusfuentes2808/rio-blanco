<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }
}
