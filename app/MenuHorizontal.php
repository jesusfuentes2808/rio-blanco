<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuHorizontal extends Model
{
	public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }

    public function menuVerticals()
    {
    	return $this->hasMany('App\MenuVertical', 'menu_horizontal_id');
    }
}
