<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Management extends Model
{
    protected $table = 'basic_managements';

    protected $fillable = [
    	'code',
    	'name',
    	'position'
    ];
}
