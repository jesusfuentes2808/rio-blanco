<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'basic_sections';

    protected $fillable = [
    	'name',
    	'code'
    ];
}
