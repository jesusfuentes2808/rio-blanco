<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuVertical extends Model
{
	public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }

    public function subMenu()
    {
    	return $this->hasMany('App\SubMenuVertical', 'menu_vertical_id');
    }
}
