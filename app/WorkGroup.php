<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkGroup extends Model
{
    //
    protected $table = 'basic_work_groups';

    protected $fillable = ['id',
    						'code',
    						'name'] ;
}
