<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsibleType extends Model
{
	protected $table = 'basic_responsible_types'; 

    protected $fillable = [
    	'code',
    	'name'
    ];
}
