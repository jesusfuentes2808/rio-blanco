<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
	public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }

    public function menuHorizontal()
    {
    	return $this->belongsTo('App\MenuHorizontal', 'menu_id');
    }
}
