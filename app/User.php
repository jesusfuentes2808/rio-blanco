<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'username',
        'name',
        'lastname',
        'email',
        'password',
        'status',
        'signature',
        'picture',
        'rol'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }
    */

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function Projects()
    {
        return $this->hasMany('App\Project');
    }
}
