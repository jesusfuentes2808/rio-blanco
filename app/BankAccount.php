<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    protected $fillable = [
    	'bank_id',
        'type_account_id',
        'currency_id',
        'company_id',
        'client_id',
        'account_number',
        'account_number_format',
        'cci',
        'disabled',
        'show_in_comprobants',
    ];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }

}
