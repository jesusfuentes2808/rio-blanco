<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostCenterType extends Model
{
    protected $table = 'basic_cost_center_types';

    protected $fillable = [
    	'code',
    	'name',
    ];
}
