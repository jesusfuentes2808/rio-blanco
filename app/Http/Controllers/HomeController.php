<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Country;

use Tecactus\Reniec\DNI;
use Tecactus\Sunat\RUC;

class HomeController extends Controller
{
    public function index()
    {
    	return view('welcome');
    }

    public function slogin()
    {
    	return view('login');
    }

    public function signIn(Request $request)
    {
    	$request->validate([
    		'username' => 'required',
    		'password' => 'required'
    	]);

    	$data = array_except($request->all(), '_token');

    	if (\Auth::attempt($data)) {
    		return redirect('/');
    	}

    	session()->flash('message', 'Usuario o contraseña incorrectos');

    	return back();
    }

    public function authUser(Request $request)
    {
        return response()->json(auth()->user());
    }

    public function menuHorizontal()
    {
        $menus = [];

        foreach (auth()->user()->role->menuHorizontal as $menu) {
            $menus[] = $menu->menuHorizontal;
        }

        return response()->json($menus);
    }

    public function getMenuVertical($horizontal_id)
    {
        $menu = auth()->user()->role->menuHorizontal->where('menu_id', $horizontal_id)->first();

        $subMenuVerticals = [];
        $menuVerticals = $menu->menuHorizontal->menuVerticals;

        $verticalTemp = null;

        foreach ($menuVerticals as  $index => $vertical) {
            // $subMenuVerticals[] = $vertical->subMenu;

            $verticalTemp = $vertical->subMenu;

            $menuVerticals[$index]->sub = $vertical->subMenu;
        }

        return response()->json($menuVerticals);
    }

    public function countries()
    {
        return response()->json(Country::all());
    }

    public function queryInformation(Request $request)
    {
        $request->validate([
            'ruc' => 'nullable|digits:11',
            'dni' => 'nullable|digits:8'
        ]);

        if ($request->ruc) {
            $sunat = new RUC('d8NCotG2TwcVn4kBdgpiBl4xjL0XJkioEVlFZA6i');

            return response()->json($sunat->getByRuc($request->ruc));
        }

        $reniec = new DNI('d8NCotG2TwcVn4kBdgpiBl4xjL0XJkioEVlFZA6i');

        return response()->json($reniec->get($request->dni));
    }
}
