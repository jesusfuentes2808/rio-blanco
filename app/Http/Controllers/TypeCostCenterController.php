<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\TypeCostCenterRepository;

class TypeCostCenterController extends Controller
{
    protected $typeCost;

    public function __construct(TypeCostCenterRepository $typeCost)
    {
        $this->typeCost = $typeCost;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->typeCost->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|min:1',
            'name' => 'required|min:2',
        ]);

        $data = array_except($request->all(), ['_token']);

        $typeCost = $this->typeCost->create($data);

        return response()->json($typeCost);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->typeCost->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id'   => 'required',
            'code' => 'required|min:1',
            'name' => 'required|min:2',
        ]);

        $data = array_except($request->all(), ['_token']);

        $typeCost = $this->typeCost->update($data, $id);

        return response()->json($typeCost);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->typeCost->destroy($id);
    }
}
