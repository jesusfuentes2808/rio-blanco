<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PaymentConditionRepository;

use App\PaymentCondition;

class PaymentConditionController extends Controller
{

    protected $paymentconditions;

    public function __construct(PaymentConditionRepository $paymentconditions)
    {
        $this->paymentconditions = $paymentconditions;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(PaymentCondition::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'days_number' => 'required|numeric|min:1'
        ]);

        $data = $request->all();

        $payment = $this->paymentconditions->create($data);

        return response()
            ->json([
                'payment' => $payment
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = $this->paymentconditions->find($id);

        return response()
            ->json([
                'payment' => $payment
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'days_number' => 'required|numeric|min:1'
        ]);

        $data = $request->all();

        $payment = $this->paymentconditions->update($data, $id);

        return response()
            ->json([
                'payment' => $payment
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->paymentconditions->destroy($id);
    }
}
