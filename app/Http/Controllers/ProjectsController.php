<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ProjectsRepository;
use Auth;


class ProjectsController extends Controller
{
    protected $projects;

    public function __construct(ProjectsRepository $projects)
    {
        $this->projects = $projects;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->projects->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'contract_number' => 'required|numeric',
            'reference' => 'required',
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'delivery_date' => 'required',
            'delivery_conditions' => 'required',
            'comment' => 'required',
        ]);

        $data = [
            'company_id' => '1',
            'type_id' => $request->project_type,
            'code' => $request->code,
            'contract_number' => $request->contract_number,
            'partner_id' => $request->client,
            'user_id' => Auth::user()->id,
            'reference' => $request->reference,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'delivery_date' => $request->delivery_date,
            'delivery_conditions' => $request->delivery_conditions,
            'comment' => $request->comment,
            'status' => 'ACTIVO'
        ];

        $project = $this->projects->create($data);

        return response()->json(['project' => $project]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->projects->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = [
            'company_id' => '1',
            'type_id' => $request->project_type,
            'code' => $request->code,
            'contract_number' => $request->contract_number,
            'partner_id' => $request->client,
            'user_id' => Auth::user()->id,
            'reference' => $request->reference,
            'name' => $request->name,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'delivery_date' => $request->delivery_date,
            'delivery_conditions' => $request->delivery_conditions,
            'comment' => $request->comment,
            'status' => 'ACTIVO'
        ];

        array_forget($data, '_token');

        $unitMeasure = $this->projects->update($data, $id);

        return $unitMeasure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->projects->destroy($id);
    }
}
