<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PeriodsRepository;

class PeriodsController extends Controller
{
    //
    protected $periodsRepo;

    public function __construct(PeriodsRepository $periodsRepo)
    {
        $this->periodsRepo = $periodsRepo;
    }

    public function index()
    {
        return $this->periodsRepo->periods();
    }

    public function store(Request $request)
    {
        $request->validate([
            'code' => 'unique:admin_periods|required|max:14',
            'name' => 'required',
            'date_from_format' => 'required|date_format:"Y-m-d"',
            'date_to_format' => 'required|date_format:"Y-m-d"|after:date_from_format',
        ]);

        return $this->periodsRepo->store($request);
    }

    public function getById(Request $request)
    {
        return $this->periodsRepo->getById($request);
    }

    public function edit($id)
    {	
        return $this->periodsRepo->find($id);
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'code' => 'required|max:14',
            'name' => 'required',
            'date_from_format' => 'required|date_format:"Y-m-d"',
            'date_to_format' => 'required|date_format:"Y-m-d"|after:date_from_format',
        ]);

        return $this->periodsRepo->update($request,$id);
    }

    public function destroy($id)
    {
        return $this->periodsRepo->delete($id);
    }

}
