<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\PartnerCategoriesRepository;
use App\PartnerCategory;


class PartnerCategoriesController extends Controller
{

    protected $partnerCategories;

    public function __construct(PartnerCategoriesRepository $partnerCategories)
    {
        $this->partnerCategories = $partnerCategories;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->partnerCategories->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $partner = $this->partnerCategories->create($data);

        return response()
            ->json([
                'partner' => $partner
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = $this->partnerCategories->find($id);

        return response()
            ->json([
                'partner' => $partner
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $partner = $this->partnerCategories->update($data, $id);

        return response()
            ->json([
                'partner' => $partner
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->partnerCategories->destroy($id);
    }
}
