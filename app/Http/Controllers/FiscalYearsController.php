<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\FiscalYearsRepository;
use App\FiscalYear;

class FiscalYearsController extends Controller
{
    //
    protected $fiscalYearRepo;

    public function __construct(FiscalYearsRepository $fiscalYearRepo)
    {
        $this->fiscalYearRepo = $fiscalYearRepo;
    }

    public function index()
    {
        return $this->fiscalYearRepo->fiscalYears();
    }

    public function edit($id)
    {   
        return $this->fiscalYearRepo->find($id);
        //return response()->json(['user' => $this->userRepo->find($id)]);
    }

    public function store(Request $request)
    {   
        //dd($request->all());
        $request->validate([
            'code' => 'unique:admin_fiscalyear|required|max:8',
            'name' => 'required',
            'year' => 'required|not_in:-1,null'
        ]);

        return $this->fiscalYearRepo->store($request);
    }

    public function update(Request $request, $id)
    {   
        $request->validate([
            'code' => 'required|max:8',
            'name' => 'required',
        ]);
        return $this->fiscalYearRepo->update($request, $id);
    }

    public function currentYear(){
        return $this->fiscalYearRepo->currentYear();
    }
}
