<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\UsersRepository;
use App\User;

use Image;
use Carbon\Carbon;

use Intervention\Image\ImageManagerStatic;
use Exception;

class UsersController extends Controller
{
    protected $userRepo;

    public function __construct(UsersRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->userRepo->user();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'username' => 'required|unique:users|min:3',
            'name' => 'required|min:3',
            'lastname' => 'required|min:3',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:3',
            'pass2' => 'required|min:3|same:password',
            'role_id' => 'required|not_in:-1'
        ]);

        return $this->userRepo->store($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        return $this->userRepo->find($id);
        //return response()->json(['user' => $this->userRepo->find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'username' => 'required|min:3',
            'name' => 'required|min:3',
            'lastname' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'nullable|min:3',
            'pass2' => 'nullable|min:3|same:password',
            'role_id' => 'required|not_in:-1'
        ]);

        
        return $this->userRepo->update($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->userRepo->delete($id);
    }

    
}
