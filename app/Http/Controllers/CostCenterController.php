<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CostCenter;

class CostCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return CostCenter::with(['unitMeasure','management','section','costCenterType','costType'])->get();
    }

    public function listParent()
    {
        //
        return CostCenter::where('parent_id',null)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'project' => 'required|not_in:-1,null',
            'costType' => 'required|not_in:-1,null',
            'costCenterType' => 'required|not_in:-1,null',
            'unit' => 'required|not_in:-1,null',
            'section' => 'required|not_in:-1,null',
            'management' => 'required|not_in:-1,null',
            'workGroup' => 'required|not_in:-1,null',
            'code' => 'unique:basic_cost_centers|required',
            'name' => 'required',
        ]);

        $costCenter = new CostCenter;
        
        $costCenter->parent_id = $request->parent;
        $costCenter->project_id = $request->project;
        $costCenter->company_id = 1;
        $costCenter->cost_type_id = $request->costType;
        $costCenter->cost_center_type_id = $request->costCenterType;
        $costCenter->code = $request->code;
        $costCenter->name = $request->name;
        $costCenter->unit_id = $request->unit;
        $costCenter->section_id = $request->section;
        $costCenter->managements_id = $request->management;
        $costCenter->work_group_id = $request->workGroup;
        $costCenter->status = 1;
        $costCenter->supply_other_project = ($request->centerCost)?'1':'0';
        $costCenter->supply_transfer_project = ($request->supplyTransfer)?'1':'0';


        $costCenter->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         return CostCenter::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'project' => 'required|not_in:-1,null',
            'costType' => 'required|not_in:-1,null',
            'costCenterType' => 'required|not_in:-1,null',
            'unit' => 'required|not_in:-1,null',
            'section' => 'required|not_in:-1,null',
            'management' => 'required|not_in:-1,null',
            'workGroup' => 'required|not_in:-1,null',
            'code' => 'required',
            'name' => 'required',
        ]);

        $costCenter = CostCenter::find($id);

        if($costCenter->code != $request->code){
            $costCenterC = CostCenter::where('code',$request->code);        
                if($costCenterC->count() == 0){
                    $costCenter->code = $request->code;
                }else{
                    throw new Exception('The code has already been taken.');
                }
        }
        
        $costCenter->parent_id = $request->parent;
        $costCenter->project_id = $request->project;
        $costCenter->company_id = 1;
        $costCenter->cost_type_id = $request->costType;
        $costCenter->cost_center_type_id = $request->costCenterType;
        
        $costCenter->name = $request->name;
        $costCenter->unit_id = $request->unit;
        $costCenter->section_id = $request->section;
        $costCenter->managements_id = $request->management;
        $costCenter->work_group_id = $request->workGroup;
        $costCenter->status = 1;
        $costCenter->supply_other_project = ($request->centerCost)?'1':'0';
        $costCenter->supply_transfer_project = ($request->supplyTransfer)?'1':'0';

        $costCenter->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $costCenter = CostCenter::find($id);
        $costCenter->delete();
    }
}
