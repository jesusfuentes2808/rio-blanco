<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ResponsibleTypesRepository;

class ResponsibleTypesController extends Controller
{
    protected $responsibleType;

    public function __construct(ResponsibleTypesRepository $responsibleType)
    {
        $this->responsibleType = $responsibleType;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsibleTypes = $this->responsibleType->all();

        return response()->json($responsibleTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|min:2',
            'name' => 'required|min:2',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $responsibleType = $this->responsibleType->create($data);

        return response()->json($responsibleType);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->responsibleType->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->responsibleType->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code' => 'required|min:2',
            'name' => 'required|min:2',
            'id' => 'required|numeric'
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $responsibleType = $this->responsibleType->update($data, $id);

        return response()->json($responsibleType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->responsibleType->destroy($id);
    }
}
