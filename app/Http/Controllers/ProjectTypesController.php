<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ProjectType;

class ProjectTypesController extends Controller
{
    public function index()
    {
    	$projectType = ProjectType::all();

    	return response()->json(['projectType' => $projectType]);
    }

    public function store(Request $request)
    {
    	$request->validate([
            'name' => 'required',
        ]);

    	$data = [
    		'name'	=>	$request->name
    	];

    	$projectType = ProjectType::create($data);

    	return response()->json(['projectType' => $projectType]);
    }

    public function edit(Request $request, $id)
    {
    	/*$request->validate([
            'name' => 'required',
        ]);*/

    	$projectType = ProjectType::find($id);

    	return response()->json([$projectType]);
    }

    public function update(Request $request, $id)
    {
    	$data = [
    		'name'	=>	$request->name
    	];

    	$find = ProjectType::find($id);

    	$projectType = $find->update($data);

    	return response()->json(['projectType' => $projectType]);
    }

    public function destroy(Request $request, $id)
    {
    	return ProjectType::destroy($id);
    }
}
