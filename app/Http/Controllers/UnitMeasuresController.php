<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\UnitMeasuresRepository;

class UnitMeasuresController extends Controller
{
    protected $unitMeasures;

    public function __construct(UnitMeasuresRepository $unitMeasures)
    {
        $this->unitMeasures = $unitMeasures;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->unitMeasures->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sunat_code'    => 'required|min:2',
            'sunat_name'    => 'required|min:2',
            'own_code'      => 'required|min:2',
            'own_name'      => 'required|min:2',
            'coment'        => 'required|min:2',
            'um_type'       => 'required|min:2',
            'rounding_um'   => 'required|numeric|between:0,100',
            'factor_um'     => 'required|numeric|between:0,100',
            'active'        => 'boolean',

            'basic_unit_measure_category_id' => 'required',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $unitMeasure = $this->unitMeasures->create($data);

        return $unitMeasure;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->unitMeasures->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sunat_code'    => 'required|min:2',
            'sunat_name'    => 'required|min:2',
            'own_code'      => 'required|min:2',
            'own_name'      => 'required|min:2',
            'coment'        => 'required|min:2',
            'um_type'       => 'required|min:2',
            'rounding_um'   => 'required|numeric|between:0,100',
            'factor_um'     => 'required|numeric|between:0,100',
            'active'        => 'boolean',

            'basic_unit_measure_category_id' => 'required',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $unitMeasure = $this->unitMeasures->update($data, $id);

        return $unitMeasure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->unitMeasures->destroy($id);
    }
}
