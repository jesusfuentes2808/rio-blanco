<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Bank;
use App\BankTypeAccount;

use App\Repositories\BanksRepository;

class BanksController extends Controller
{

    protected $banks;

    public function __construct(BanksRepository $banks)
    {
        $this->banks = $banks;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return response()->json(Bank::with('country')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'swift_code' => 'required|unique:admin_banks|min:5',
            //'vat_code' => 'required|unique:admin_banks|min:5',
            'name' => 'required|min:3',
            'city' => 'required|min:3',
            'branch_code' => 'required|min:3',
            'address' => 'required|min:8',
            'zip' => 'required|min:3|numeric',
            'status' => 'required'
        ]);

        $bank = $this->banks->create($request);

        return response()
            ->json([
                'bank' => $bank
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = $this->banks->find($id);

        return response()
            ->json([
                'bank' => $bank
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'swift_code' => 'nullable|min:5',
            'vat_code' => 'nullable|min:5',
            'name' => 'required|min:3',
            'city' => 'required|min:3',
            'branch_code' => 'required|min:5',
            'address' => 'required|min:8',
            'zip' => 'required|min:3|numeric',
            'status' => 'required'
        ]);

        $bank = $this->banks->update($request, $id);

        return response()
            ->json([
                'bank' => $bank
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->banks->destroy($id);
    }

    public function typeAccounts()
    {
        return response()->json(BankTypeAccount::all());
    }
}
