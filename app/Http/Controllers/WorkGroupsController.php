<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WorkGroupsRepository;

class WorkGroupsController extends Controller
{
    //
    protected $workGroupsRepo;

    public function __construct(WorkGroupsRepository $workGroupsRepo)
    {
        $this->workGroupsRepo = $workGroupsRepo;
    }

    public function index(){
    	return $this->workGroupsRepo->all();
    }

}
