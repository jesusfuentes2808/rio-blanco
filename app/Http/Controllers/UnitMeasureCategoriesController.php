<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\UnitMeasureCategoriesRepository;

class UnitMeasureCategoriesController extends Controller
{
    protected $unitMeasure;

    public function __construct(UnitMeasureCategoriesRepository $unitMeasure)
    {
        $this->unitMeasure = $unitMeasure;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->unitMeasure->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2'
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $unitMeasure = $this->unitMeasure->create($data);

        return $unitMeasure;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->unitMeasure->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|min:2'
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $unitMeasure = $this->unitMeasure->update($data, $id);

        return $unitMeasure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->unitMeasure->destroy($id);
    }
}
