<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ClientsRepository;

use App\Client;

use Image;
use Carbon\Carbon;

class ClientsController extends Controller
{

    protected $clients;

    public function __construct(ClientsRepository $clients)
    {
        $this->clients = $clients;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->authorize('viewClient', auth()->user());

        return response()->json($this->clients->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'partner_category_id' => 'required',
            'document_type' => 'required',
            'document_number' => 'required',
            'name' => 'required',
        ]);

        if (!$request->international) {
            $request->validate([
                'state_id' => 'required',
                'province_id' => 'required',
                'district_id' => 'required',
            ]);
        }

        $data = $request->all();

        if($request->picture){
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($request->picture, 0, strpos($request->picture, ';')))[1])[1];

            Image::make($request->picture)->save(public_path() . '/img/clients/' . $fileName);

            $data['picture'] = $fileName;
        }else{
            array_forget($data, 'picture');
        }

        // $data['partner_category_id'] = $data['partner_category_id']['id'];

        // dd($data['contacts']);

        $client = $this->clients->create($data);

        if (array_has($data, 'contact')) {
            $contact = array_pull($data, 'contact');

            $contact = array_add($contact, 'client_id', $client->id);

            $this->clients->createContact($contact);
        }

        if (array_has($data, 'contacts')) {
            $contacts = array_pull($data, 'contacts');

            if (count($contacts) > 0) {
                data_fill($contacts, '*.partner_id', $client->id);
                // dd($contacts);
                $this->clients->createContacts($contacts);
            }
        }

        if (array_has($data, 'addresses')) {
            $addressesFormat = array_pull($data, 'addresses');

            if (count($addressesFormat) > 0) {
                $addresses = [];

                foreach ($addressesFormat as $address) {                    
                    $address = array_add($address, 'partner_id', $client->id);

                    $addresses[] = $address;
                }

                $this->clients->createAddresses($addresses);
            }
        }

        if (array_has($data, 'bank_accounts')) {
            $bankAccountsFormat = array_pull($data, 'bank_accounts');

            if (count($bankAccountsFormat) > 0) {
                $bankAccount = [];

                foreach ($bankAccountsFormat as $account) {

                    $account['bank_id'] = $account['bank'];
                    $account['type_account_id'] = $account['type_account'];
                    $account['currency_id'] = $account['currency'];
                    
                    $account = array_add($account, 'client_id', $client->id);

                    array_forget($account, 'bank');
                    array_forget($account, 'currency');
                    array_forget($account, 'type_account');

                    $bankAccount[] = $account;
                }

                $this->clients->createBankAccount($bankAccount);
            }
        }

        return response()->json($client);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = $this->clients->find($id);

        return response()->json($client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $request->validate([
            'partner_category_id' => 'required',
            'document_type' => 'required',
            'document_number' => 'required',
            'name' => 'required',
        ]);

        if (!$request->international) {
            $request->validate([
                'state_id' => 'required',
                'province_id' => 'required',
                'district_id' => 'required',
            ]);
        }

        $data = $request->all();

        if($request->picture && $data['picture'] != 'no-picture.png'){

            if ($client->picture != 'no-picture.png') {
                unlink(public_path() . '/img/clients/' . $client->picture);
            }

            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($request->picture, 0, strpos($request->picture, ';')))[1])[1];

            Image::make($request->picture)->save(public_path() . '/img/clients/' . $fileName);

            $data['picture'] = $fileName;
        }else{
            array_forget($data, 'picture');
        }

        $client->update($data);

        if (array_has($data, 'contact')) {
            $contact = array_pull($data, 'contact');

            if ($client->contact) {
                $this->clients->createContact($contact, $client->contact);
            }else{
                $contact = array_add($contact, 'client_id', $client->id);

                $this->clients->createContact($contact);
            }
        }

        if (array_has($data, 'contacts')) {
            $contacts = array_pull($data, 'contacts');

            if (count($contacts) > 0) {
                data_fill($contacts, '*.partner_id', $client->id);

                $contacts = array_where($contacts, function ($value, $key) {
                    if (!array_has($value, 'id')) {
                        return $value;
                    }
                });

                $this->clients->createContacts($contacts);
            }
        }

        if (array_has($data, 'addresses')) {
            $addressesFormat = array_pull($data, 'addresses');

            if (count($addressesFormat) > 0) {
                $addresses = [];

                foreach ($addressesFormat as $address) {

                    if (array_has($address, 'id')) {
                        continue;
                    }
                    
                    $address = array_add($address, 'partner_id', $client->id);

                    $addresses[] = $address;
                }

                $this->clients->createAddresses($addresses);
            }
        }

        if (array_has($data, 'bank_accounts')) {
            $bankAccountsFormat = array_pull($data, 'bank_accounts');

            if (count($bankAccountsFormat) > 0) {
                $bankAccount = [];

                foreach ($bankAccountsFormat as $account) {

                    $account['bank_id'] = $account['bank'];
                    $account['type_account_id'] = $account['type_account'];
                    $account['currency_id'] = $account['currency'];
                    
                    $account = array_add($account, 'client_id', $client->id);

                    array_forget($account, 'bank');
                    array_forget($account, 'currency');
                    array_forget($account, 'type_account');

                    $bankAccount[] = $account;
                }

                $this->clients->createBankAccount($bankAccount);
            }
        }

        if (array_has($data, 'deleteAddresses')) {
            $this->clients->deleteReference($data['deleteAddresses'], 'deleteAddresses');
        }

        if (array_has($data, 'deleteContacts')) {
            $this->clients->deleteReference($data['deleteContacts'], 'deleteContacts');
        }

        if (array_has($data, 'deleteBanks')) {
            $this->clients->deleteReference($data['deleteBanks'], 'deleteBanks');
        }
        return response()->json($client);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
