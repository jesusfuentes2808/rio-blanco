<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\CompaniesRepository;

use App\Company;

use Carbon\Carbon;

class CompaniesController extends Controller
{
    protected $companies;

    public function __construct(CompaniesRepository $companies)
    {
        $this->companies = $companies;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->companies->all());
    }

    public function allParent()
    {
        return response()->json($this->companies->allParent());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'name' => 'required|min:3',
            'ruc' => 'unique:admin_companies|required',
            'street' => 'required|min:5',
            'reference' => 'required|min:3',
            'contact' => 'required|min:3',
            'phone' => 'required',
            'cellphone' => 'required',
            'email' => 'required|email',
            'viaType' => 'required|not_in:-1,null',
            'zoneType' => 'required|not_in:-1,null',
        ]);


        return $this->companies->store($request);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->companies->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3',
            'ruc' => 'required',
            'street' => 'required|min:5',
            'reference' => 'required|min:3',
            'contact' => 'required|min:3',
            'phone' => 'required',
            'cellphone' => 'required',
            'email' => 'required|email',
            'viaType' => 'required|not_in:-1,null',
            'zoneType' => 'required|not_in:-1,null',
        ]);


        return $this->companies->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->companies->delete($id);
    }
}
