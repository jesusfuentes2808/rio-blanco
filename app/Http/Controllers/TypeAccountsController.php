<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BankTypeAccount;
use App\Repositories\TypeAccountsRepository;

class TypeAccountsController extends Controller
{

    protected $typeAccounts;

    public function __construct(TypeAccountsRepository $typeAccounts)
    {
        $this->typeAccounts = $typeAccounts;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $type = BankTypeAccount::all();

        return response()->json($type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $t = $this->typeAccounts->create($data);

        return response()
            ->json([
                'type' => $t
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $t = $this->typeAccounts->find($id);

        return response()
            ->json([
                'type' => $t
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $data = $request->all();

        $t = $this->typeAccounts->update($data, $id);

        return response()
            ->json([
                'type' => $t
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->typeAccounts->destroy($id);
    }
}
