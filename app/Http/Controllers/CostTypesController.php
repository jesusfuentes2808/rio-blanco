<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\CostTypesRepository;

class CostTypesController extends Controller
{
    protected $costTypes;

    public function __construct(CostTypesRepository $costTypes)
    {
        $this->costTypes = $costTypes;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $costTypes = $this->costTypes->all();

        return response()->json($costTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|min:1',
            'name' => 'required|min:2',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $costType = $this->costTypes->create($data);

        return response()->json($costType);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json($this->costTypes->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->costTypes->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code' => 'required|min:1',
            'name' => 'required|min:2',
            'id' => 'required|numeric'
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $costType = $this->costTypes->update($data, $id);

        return response()->json($costType);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->costTypes->destroy($id);
    }
}
