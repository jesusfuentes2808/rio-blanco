<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\DocumentsTypeRepository;
use App\DocumentType;

class DocumentsTypeController extends Controller
{

    protected $documentsType;

    public function __construct(DocumentsTypeRepository $documentsType)
    {
        $this->documentsType = $documentsType;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(DocumentType::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'code' => 'required|unique:admin_document_types|max:1',
            'short_name' => 'required|max:3'
        ]);

        $data = $request->all();

        $documents = $this->documentsType->create($data);

        return response()
            ->json([
                'documents' => $documents
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $documents = $this->documentsType->find($id);

        return response()
            ->json([
                'documents' => $documents
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code' => 'required|min:1',
            'name' => 'required'
        ]);

        $data = $request->all();

        $documents = $this->documentsType->update($data, $id);

        return response()
            ->json([
                'documents' => $documents
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->documentsType->destroy($id);
    }
}
