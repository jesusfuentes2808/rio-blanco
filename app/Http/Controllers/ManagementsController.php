<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ManagementsRepository;

class ManagementsController extends Controller
{
    protected $managements;

    public function __construct(ManagementsRepository $managements)
    {
        $this->managements = $managements;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managements = $this->managements->all();

        return response()->json($managements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|min:3',
            'name' => 'required|min:3',
            'position' => 'required|min:3',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $management = $this->managements->create($data);

        return response()->json($management);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->managements->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code' => 'required|min:3',
            'name' => 'required|min:3',
            'position' => 'required|min:3',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $this->managements->update($data, $id);

        return response()->json([true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->managements->destroy($id);
    }
}
