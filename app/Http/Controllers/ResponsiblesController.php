<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ResponsiblesRepository;

class ResponsiblesController extends Controller
{
    protected $responsibles;

    public function __construct(ResponsiblesRepository $responsibles)
    {
        $this->responsibles = $responsibles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $responsibles = $this->responsibles->all();

        return response()->json($responsibles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'responsible_type_id' => 'required|numeric',
            'name' => 'required|min:2',
            'position' => 'required|min:2',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $responsible = $this->responsibles->create($data);

        return response()->json($responsible);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->responsibles->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'responsible_type_id' => 'required|numeric',
            'name' => 'required|min:2',
            'position' => 'required|min:2',
        ]);

        $data = $request->all();

        array_forget($data, '_token');

        $this->responsibles->update($data, $id);

        return response()->json([true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->responsibles->destroy($id);
    }
}
