<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\SubSectionsRepository;

class SubSectionsController extends Controller
{
    protected $subSections;

    public function __construct(SubSectionsRepository $subSections)
    {
        $this->subSections = $subSections;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sections = $this->subSections->all();

        return response()->json($sections);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'section' => 'required',
        ]);

        $data = [];

        $data['name'] = $request->name;
        $data['basic_section_id'] = $request->section;

        $section = $this->subSections->create($data);

        return response()->json($section);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json($this->subSections->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3',
            'section' => 'required',
        ]);

        $data = [];

        $data['name'] = $request->name;
        $data['basic_section_id'] = $request->section;

        $section = $this->subSections->update($data, $id);

        return response()->json($section);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->subSections->destroy($id);
    }
}
