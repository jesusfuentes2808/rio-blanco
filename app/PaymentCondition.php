<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCondition extends Model
{
	protected $table = 'basic_payment_conditions';

    protected $fillable = ['name', 'days_number'];
}
