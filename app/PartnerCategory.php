<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerCategory extends Model
{
    protected $fillable = ['name'];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }
}
