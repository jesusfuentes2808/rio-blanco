<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
	protected $table = 'admin_states';

    public function provinces()
    {
    	return $this->hasMany('App\Province', 'state_id');
    }
}
