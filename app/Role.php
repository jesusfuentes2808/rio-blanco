<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'admin_roles';

	public function menuHorizontal()
	{
		return $this->hasMany('App\RoleMenu');
	}

    public function actions()
    {
    	return $this->hasMany('App\RoleAction');
    }
}
