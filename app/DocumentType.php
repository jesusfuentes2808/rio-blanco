<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $fillable = ['code', 'short_name', 'name'];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }
}
