<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerContact extends Model
{
    protected $fillable = [
    	'partner_id',
        'type',
        'name',
        'position',
        'phone',
        'fax',
        'cellphone',
        'email',
        'website',
        'comment',
    ];

    public function __construct()
    {
        $this->table = 'admin_' . $this->getTable();
    }
}
