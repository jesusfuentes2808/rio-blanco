<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAddressStreet extends Model
{
    //
    protected $table = 'admin_address_street_type';

    protected $fillable = ['id','code','short_name','name'];
}
