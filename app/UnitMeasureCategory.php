<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitMeasureCategory extends Model
{
    protected $table = 'basic_unit_measure_categories';

    protected $fillable = [
    	'name'
    ];
}
