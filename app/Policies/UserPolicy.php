<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function view(User $user)
    {
        $action = $user->role->actions->where('module', 'users');

        if($action->count() == 0){
            return false;
        }

        $action = $action->first();

        return str_contains($action->actions, 'VIEW');
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $action = $user->role->actions->where('module', 'users');

        if($action->count() == 0){
            return false;
        }

        $action = $action->first();

        return str_contains($action->actions, 'CREATE');
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user)
    {
        $action = $user->role->actions->where('module', 'users');

        if($action->count() == 0){
            return false;
        }

        $action = $action->first();

        return str_contains($action->actions, 'UPDATE');
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user)
    {
        $action = $user->role->actions->where('module', 'users');

        if($action->count() == 0){
            return false;
        }

        $action = $action->first();

        return str_contains($action->actions, 'DELETE');
    }

    public function viewClient(User $user)
    {
        $action = $user->role->actions->where('module', 'clients');

        if($action->count() == 0){
            return false;
        }

        $action = $action->first();

        return str_contains($action->actions, 'VIEW');
    }
}
