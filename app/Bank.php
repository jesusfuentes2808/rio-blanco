<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{

    protected $table = "admin_banks";

    protected $fillable = [
            'country_id',
            'swift_code',
            'short_name',
            'name','city',
            'short_name',
            'branch_code',
            'address',
            'short_name',
            'zip',
            'active'
        ];

    public function country()
    {
    	return $this->belongsTo('App\Country');
    }

    public function currency()
    {
    	return $this->belongsTo('App\Currency');
    }
}
