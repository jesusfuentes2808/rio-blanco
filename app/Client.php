<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'admin_clients';

    protected $fillable = [
		'company_id',
        'partner_category_id',
        'client_type',
        'document_type',
        'document_number',
        'name',
        'first_lastname',
        'second_lastname',
        'address',
        'street',
        'street2',
        'reference',
        'state_id',
        'province_id',
        'district_id',
        'use_address',
        'another_id',
        'ean_code',
        'comercial_name',
        'credit_limit',
        'debit_limit',
        'comment',
        'is_client',
        'sale_rate_id',
        'client_payment_condition',
        'is_provider',
        'provider_currency',
        'provider_payment_condition',
        'is_international',
        'country_id',
        'picture',
        'status',
    ];

    public function __construct()
    {
        //$this->table = 'admin_' . $this->getTable();
        //$this->table = 'admin_partners';
    }


    public function partnerCategory()
    {
        return $this->belongsTo('App\PartnerCategory');
    }

    public function state()
    {
        return $this->belongsTo('App\State');
    }

    public function province()
    {
        return $this->belongsTo('App\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function contact()
    {
        return $this->hasOne('App\ClientContact');
    }

    public function contacts()
    {
        return $this->hasMany('App\PartnerContact', 'partner_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\PartnerAddress','partner_id');
    }

    public function accounts()
    {
        return $this->hasMany('App\BankAccount');
    }

    public function Projects()
    {
        return $this->hasMany('App\Project');
    }
}
