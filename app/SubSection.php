<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSection extends Model
{
    protected $table = 'basic_sub_sections';

    protected $fillable = [
    	'basic_section_id',
    	'name',
    ];

    public function section()
    {
    	return $this->belongsTo('App\Section', 'basic_section_id');
    }
}
