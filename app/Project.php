<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    //protected $table = 'admin_projects';
    protected $table = 'project_projects';

    protected $fillable = [
    	'company_id',
    	'type_id',
    	'code',
    	'contract_number',
    	'partner_id',
    	'user_id',
    	'reference',
    	'name',
    	'date_start',
    	'date_end',
    	'delivery_date',
    	'comment',
    	'status'
    ];

    public function clients()
    {
      return $this->belongsTo('App\Client', 'partner_id', 'id');
    }

    public function typeProjects()
    {
      return $this->belongsTo('App\ProjectType', 'type_id', 'id');
    }

    public function users()
    {
      return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
