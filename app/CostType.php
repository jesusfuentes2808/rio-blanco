<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CostType extends Model
{
    protected $table = 'basic_cost_types';

    protected $fillable = [
    	'name',
    	'code',
    ];
}
