<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAddressZone extends Model
{
    //
    protected $table = 'admin_address_zone_type';

    protected $fillable = ['id','code','short_name','name'];
}
