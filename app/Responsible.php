<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
	protected $table = 'basic_responsibles';

    protected $fillable = [
    	'responsible_type_id',
    	'name',
    	'position',
    ];

    public function type()
    {
    	return $this->belongsTo('App\ResponsibleType', 'responsible_type_id');
    }
}
