<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitMeasure extends Model
{
    protected $table = 'basic_unit_measures';

    protected $fillable = [
    	'sunat_code',
    	'sunat_name',
    	'own_code',
    	'own_name',
    	'coment',
    	'um_type',
    	'um_category_id',
    	'rounding_um',
    	'factor_um',
    	'active',
    ];

    public function category()
    {
        return $this->belongsTo('App\UnitMeasureCategory', 'um_category_id');
    }
}
