<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectType extends Model
{
    //protected $table = 'admin_project_types';

	protected $table = 'project_types';
	
    protected $fillable = [
    	'name'
    ];

    public function Projects()
    {
        return $this->hasMany('App\Project');
    }
}
