<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Period extends Model
{	
	use SoftDeletes;
    //
    protected $table = 'admin_periods';
    protected $dates = ['deleted_at'];

    protected $fillable = ['id',
    					   'company_id',
    					   'fiscalyear_id',
						   'name',
						   'code',
						   'date_from',
						   'date_to',
							];
}
