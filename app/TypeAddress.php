<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeAddress extends Model
{
    //protected $table = 'admin_type_addresses';
    //TODO
	//protected $table = 'admin_address_types';
	protected $table = 'admin_address_type';
    
    protected $fillable = ['name'];
}
