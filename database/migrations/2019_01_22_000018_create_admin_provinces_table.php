<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminProvincesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_provinces';

    /**
     * Run the migrations.
     * @table admin_provinces
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('state_id');
            $table->string('ubigeo', 2);
            $table->string('code', 4)->comment('Codigo 4 digitos Estado + Provincia');
            $table->string('name', 64);

            $table->index(["state_id"], 'fk_admin_provinces_admin_states1_idx');
            $table->nullableTimestamps();


            $table->foreign('state_id', 'fk_admin_provinces_admin_states1_idx')
                ->references('id')->on('admin_states')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
