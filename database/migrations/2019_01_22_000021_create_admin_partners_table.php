<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPartnersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_partners';

    /**
     * Run the migrations.
     * @table admin_partners
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('partner_category_id');
            $table->unsignedInteger('industry_id')->nullable();
            $table->string('name', 164);
            $table->string('comercial_name', 164)->nullable()->default(null);
            $table->string('first_lastname', 64);
            $table->string('second_lastname', 64);
            $table->unsignedInteger('document_type');
            $table->string('document_number', 24)->comment('Nro RUC Nro DNI');
            $table->string('picture', 64)->nullable()->default('no-picture.png');
            $table->string('address')->nullable()->default(null)->comment('Direccion busqueda Sunat');
            $table->unsignedInteger('street_type_id')->nullable()->default(null);
            $table->string('street', 128)->nullable()->default(null);
            $table->string('address_number', 12)->nullable()->default(null)->comment('Numero de la direccion');
            $table->string('address_inside', 12)->nullable()->default(null)->comment('Interior Nro departamento');
            $table->string('street2', 128)->nullable()->default(null);
            $table->unsignedInteger('zone_type_id')->nullable()->default(null);
            $table->string('reference', 64)->nullable()->default(null)->comment('Referencia de zona Normbre Urbanizacio Centro poblado');
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('province_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->string('additional_reference', 64)->nullable()->default(null)->comment('Referencia entre calles');
            $table->string('zip', 8)->nullable()->default(null);
            $table->string('website')->nullable()->default(null);
            $table->tinyInteger('use_address')->nullable()->default(null);
            $table->string('ean_code', 13)->nullable()->default(null);
            $table->string('another_id', 24)->nullable()->default(null);
            $table->decimal('credit_limit', 12, 2)->nullable()->default(null);
            $table->decimal('debit_limit', 12, 2)->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);
            $table->tinyInteger('is_client');
            $table->unsignedInteger('sale_rate_id')->nullable()->default(null);
            $table->unsignedInteger('client_payment_terms')->nullable()->default(null);
            $table->tinyInteger('is_provider');
            $table->unsignedInteger('provider_currency')->nullable()->default(null);
            $table->unsignedInteger('provider_payment_terms')->nullable()->default(null);
            $table->tinyInteger('is_international');
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->tinyInteger('status')->default('0');

            $table->index(["district_id"], 'fk_admin_partners_admin_districts1_idx');

            $table->index(["state_id"], 'fk_admin_partners_admin_states1_idx');

            $table->index(["province_id"], 'fk_admin_partners_admin_provinces1_idx');

            $table->index(["client_payment_terms"], 'fk_admin_partners_basic_payment_terms1_idx');

            $table->index(["partner_category_id"], 'fk_admin_partners_admin_partner_categories1_idx');

            $table->index(["provider_payment_terms"], 'fk_admin_partners_basic_payment_terms2_idx');

            $table->index(["industry_id"], 'fk_admin_partners_admin_industries1_idx');

            $table->index(["document_type"], 'fk_admin_partners_admin_document_types1_idx');

            $table->index(["company_id"], 'fk_admin_partners_admin_companies1_idx');

            $table->index(["street_type_id"], 'fk_admin_partners_admin_address_street_type1_idx');

            $table->unique(["document_number"], 'document_number_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('street_type_id', 'fk_admin_partners_admin_address_street_type1_idx')
                ->references('id')->on('admin_address_street_type')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('company_id', 'fk_admin_partners_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('district_id', 'fk_admin_partners_admin_districts1_idx')
                ->references('id')->on('admin_districts')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('document_type', 'fk_admin_partners_admin_document_types1_idx')
                ->references('id')->on('admin_document_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('industry_id', 'fk_admin_partners_admin_industries1_idx')
                ->references('id')->on('admin_industries')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('partner_category_id', 'fk_admin_partners_admin_partner_categories1_idx')
                ->references('id')->on('admin_partner_categories')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('province_id', 'fk_admin_partners_admin_provinces1_idx')
                ->references('id')->on('admin_provinces')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('state_id', 'fk_admin_partners_admin_states1_idx')
                ->references('id')->on('admin_states')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('client_payment_terms', 'fk_admin_partners_basic_payment_terms1_idx')
                ->references('id')->on('basic_payment_terms')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('provider_payment_terms', 'fk_admin_partners_basic_payment_terms2_idx')
                ->references('id')->on('basic_payment_terms')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
