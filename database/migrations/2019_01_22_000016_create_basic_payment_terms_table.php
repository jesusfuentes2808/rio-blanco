<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicPaymentTermsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'basic_payment_terms';

    /**
     * Run the migrations.
     * @table basic_payment_terms
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 64)->comment('Contado Factura a 7 días Factura a 15 días');
            $table->integer('days_number')->comment('Numero de dias para el calculo de vencimientos');
            $table->enum('value', ['balance', 'porcentaje'])->nullable()->default('balance')->comment('Este campo es para indentificar el tipo de valor de calculo si es equilibrar el valor es 0 si es porcentaje se ingresa la cantidad');
            $table->decimal('value_amount', 12, 2)->nullable()->default(null)->comment('si es equilibrar el valor es cero 0 si es porcentaje ingresar el valor del porcentaje');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
