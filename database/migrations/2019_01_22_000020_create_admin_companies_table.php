<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCompaniesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_companies';

    /**
     * Run the migrations.
     * @table admin_companies
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable()->default(null)->comment('En caso de que se cree una compañia dependiente de la matriz');
            $table->string('name', 164);
            $table->string('ruc', 24);
            $table->string('slogan')->nullable()->default(null);
            $table->unsignedInteger('street_type_id')->nullable();
            $table->string('street', 128);
            $table->string('address_number', 12)->nullable()->default(null);
            $table->string('address_inside', 12)->nullable()->default(null);
            $table->string('street2', 128)->nullable()->default(null);
            $table->unsignedInteger('zone_type_id')->nullable();
            $table->string('zone_reference', 64)->nullable()->default(null)->comment('Referencia Urbanizacion Centro Poblado');
            $table->unsignedInteger('state_id')->nullable();
            $table->unsignedInteger('province_id')->nullable();
            $table->unsignedInteger('district_id')->nullable();
            $table->string('additional_reference', 64)->nullable()->default(null);
            $table->string('zip', 8)->nullable()->default(null)->comment('Codigo postal');
            $table->string('website')->nullable()->default(null);
            $table->string('contact', 64);
            $table->string('position', 64)->nullable()->default(null);
            $table->string('phone', 32);
            $table->string('cellphone', 32);
            $table->string('fax', 24)->nullable()->default(null);
            $table->string('email', 128);
            $table->string('register', 64)->nullable()->default(null)->comment('Partida Electronica ');
            $table->unsignedInteger('currency_id');
            $table->string('picture', 64)->default('no-picture.png');
            $table->tinyInteger('status')->default('0');
            $table->string('header_report')->nullable()->default(null);
            $table->text('header_report1')->nullable()->default(null);
            $table->text('header_report2')->nullable()->default(null);
            $table->text('signature')->nullable()->default(null);
            $table->text('notes')->nullable()->default(null);
            $table->text('footer_report')->nullable()->default(null);
            $table->text('footer_report1')->nullable()->default(null);
            $table->text('overdue_msg')->nullable()->default(null)->comment('Mensaje para enviar sobre deudas o facturas vencidas');

            $table->index(["parent_id"], 'fk_admin_companies_admin_companies1_idx');

            $table->index(["state_id"], 'fk_admin_companies_admin_states1_idx');

            $table->index(["province_id"], 'fk_admin_companies_admin_provinces1_idx');

            $table->index(["street_type_id"], 'fk_admin_companies_admin_address_street_type1_idx1');

            $table->index(["district_id"], 'fk_admin_companies_admin_districts1_idx');

            $table->index(["zone_type_id"], 'fk_admin_companies_admin_address_zone_type1_idx1');

            $table->unique(["ruc"], 'ruc_UNIQUE');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('parent_id', 'fk_admin_companies_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('district_id', 'fk_admin_companies_admin_districts1_idx')
                ->references('id')->on('admin_districts')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('province_id', 'fk_admin_companies_admin_provinces1_idx')
                ->references('id')->on('admin_provinces')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('state_id', 'fk_admin_companies_admin_states1_idx')
                ->references('id')->on('admin_states')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('street_type_id', 'fk_admin_companies_admin_address_street_type1_idx1')
                ->references('id')->on('admin_address_street_type')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('zone_type_id', 'fk_admin_companies_admin_address_zone_type1_idx1')
                ->references('id')->on('admin_address_zone_type')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
