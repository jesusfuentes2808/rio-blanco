<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminAddressesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_addresses';

    /**
     * Run the migrations.
     * @table admin_addresses
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id')->nullable()->default(null);
            $table->unsignedInteger('partner_id')->nullable()->default(null);
            $table->unsignedInteger('address_type_id');
            $table->unsignedInteger('street_type_id')->nullable()->default(null);
            $table->string('address', 128);
            $table->string('address_additional', 128)->nullable()->default(null);
            $table->unsignedInteger('zone_type_id')->nullable()->default(null);
            $table->string('zone_reference', 64)->nullable()->default(null);
            $table->unsignedInteger('states_id')->nullable();
            $table->unsignedInteger('provinces_id')->nullable();
            $table->unsignedInteger('districts_id')->nullable();
            $table->string('additional_reference', 64)->nullable()->default(null)->comment('Referencia entre calles');
            $table->string('zip', 8)->nullable()->default(null);

            $table->index(["states_id"], 'fk_admin_addresses_admin_states1_idx');

            $table->index(["partner_id"], 'fk_admin_addresses_admin_partners1_idx');

            $table->index(["address_type_id"], 'fk_admin_addresses_admin_address_type1_idx');

            $table->index(["company_id"], 'fk_admin_addresses_admin_companies1_idx');

            $table->index(["districts_id"], 'fk_admin_addresses_admin_districts1_idx');

            $table->index(["provinces_id"], 'fk_admin_addresses_admin_provinces1_idx');

            $table->index(["zone_type_id"], 'fk_admin_addresses_admin_address_zone_type1_idx');
            $table->nullableTimestamps();


            $table->foreign('address_type_id', 'fk_admin_addresses_admin_address_type1_idx')
                ->references('id')->on('admin_address_type')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('zone_type_id', 'fk_admin_addresses_admin_address_zone_type1_idx')
                ->references('id')->on('admin_address_zone_type')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('company_id', 'fk_admin_addresses_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('districts_id', 'fk_admin_addresses_admin_districts1_idx')
                ->references('id')->on('admin_districts')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('partner_id', 'fk_admin_addresses_admin_partners1_idx')
                ->references('id')->on('admin_partners')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('provinces_id', 'fk_admin_addresses_admin_provinces1_idx')
                ->references('id')->on('admin_provinces')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('states_id', 'fk_admin_addresses_admin_states1_idx')
                ->references('id')->on('admin_states')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
