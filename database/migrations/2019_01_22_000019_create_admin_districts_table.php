<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminDistrictsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_districts';

    /**
     * Run the migrations.
     * @table admin_districts
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('province_id');
            $table->string('ubigeo', 2);
            $table->string('code', 6)->comment('Codigo ubicación geografica 6 digitos estado+provincia+distrito');
            $table->string('name', 64);

            $table->index(["province_id"], 'fk_admin_districts_admin_provinces1_idx');
            $table->nullableTimestamps();


            $table->foreign('province_id', 'fk_admin_districts_admin_provinces1_idx')
                ->references('id')->on('admin_provinces')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
