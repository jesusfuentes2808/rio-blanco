<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminClientsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_clients';

    /**
     * Run the migrations.
     * @table admin_clients
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id')->nullable()->default(null);
            $table->unsignedInteger('partner_category_id');
            $table->integer('client_type');
            $table->string('name');
            $table->string('first_lastname');
            $table->string('second_lastname');
            $table->string('document_type');
            $table->string('document_number');
            $table->string('address')->nullable()->default(null);
            $table->string('street')->nullable()->default(null);
            $table->string('street2')->nullable()->default(null);
            $table->string('reference')->nullable()->default(null);
            $table->string('picture')->default('no-picture.png');
            $table->unsignedInteger('state_id');
            $table->unsignedInteger('province_id');
            $table->unsignedInteger('district_id');
            $table->tinyInteger('use_address')->nullable()->default(null);
            $table->string('ean_code')->nullable()->default(null);
            $table->string('another_id')->nullable()->default(null);
            $table->string('comercial_name')->nullable()->default(null);
            $table->decimal('credit_limit', 12, 2)->nullable()->default(null);
            $table->decimal('debit_limit', 12, 2)->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);
            $table->tinyInteger('is_client');
            $table->unsignedInteger('sale_rate_id')->nullable()->default(null);
            $table->unsignedInteger('client_payment_condition')->nullable()->default(null);
            $table->tinyInteger('is_provider');
            $table->unsignedInteger('provider_currency')->nullable()->default(null);
            $table->unsignedInteger('provider_payment_condition')->nullable()->default(null);
            $table->tinyInteger('is_international');
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->tinyInteger('status')->default('0');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
