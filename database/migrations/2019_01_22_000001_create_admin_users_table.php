<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_users';

    /**
     * Run the migrations.
     * @table admin_users
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('username', 64);
            $table->string('password', 128);
            $table->string('name', 64);
            $table->string('email', 128);
            $table->integer('status')->default('1');
            $table->string('signature');
            $table->unsignedInteger('role_id');
            $table->string('picture', 64)->default('add_user.png');
            $table->rememberToken();

            $table->index(["role_id"], 'fk_admin_users_admin_roles1_idx');

            $table->unique(["username"], 'admin_users_username_unique');

            $table->unique(["email"], 'admin_users_email_unique');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('role_id', 'fk_admin_users_admin_roles1_idx')
                ->references('id')->on('admin_roles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
