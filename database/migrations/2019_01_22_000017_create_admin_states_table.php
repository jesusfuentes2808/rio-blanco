<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminStatesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_states';

    /**
     * Run the migrations.
     * @table admin_states
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('country_id');
            $table->string('ubigeo', 2);
            $table->string('name', 64);
            $table->string('iso', 3);

            $table->index(["country_id"], 'fk_admin_states_admin_countries1_idx');
            $table->nullableTimestamps();


            $table->foreign('country_id', 'fk_admin_states_admin_countries1_idx')
                ->references('id')->on('admin_countries')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
