<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminRoleMenusTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_role_menus';

    /**
     * Run the migrations.
     * @table admin_role_menus
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('menu_id');

            $table->index(["role_id"], 'fk_admin_role_menus_admin_roles1_idx');

            $table->index(["menu_id"], 'fk_admin_role_menus_admin_menu_horizontals1_idx');
            $table->nullableTimestamps();


            $table->foreign('menu_id', 'fk_admin_role_menus_admin_menu_horizontals1_idx')
                ->references('id')->on('admin_menu_horizontals')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('role_id', 'fk_admin_role_menus_admin_roles1_idx')
                ->references('id')->on('admin_roles')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
