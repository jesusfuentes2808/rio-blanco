<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicSubSectionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'basic_sub_sections';

    /**
     * Run the migrations.
     * @table basic_sub_sections
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('basic_section_id');
            $table->string('name');

            $table->index(["basic_section_id"], 'fk_basic_sub_sections_basic_sections1_idx');
            $table->nullableTimestamps();


            $table->foreign('basic_section_id', 'fk_basic_sub_sections_basic_sections1_idx')
                ->references('id')->on('basic_sections')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
