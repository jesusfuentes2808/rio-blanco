<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectProjectsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'project_projects';

    /**
     * Run the migrations.
     * @table project_projects
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('partner_id')->nullable()->default(null);
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->string('code', 13)->comment('concatenar con id para completar con ceros a 13 digitos PRO0000000001');
            $table->string('contract_number', 24)->nullable()->default(null)->comment('nro contrato orden de servicio orde de compra etc');
            $table->string('reference')->nullable()->default(null)->comment('Referencia del proyecto');
            $table->string('name', 128)->comment('nombre del proyecto');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('delivery_conditions')->nullable()->default(null)->comment('15 dias 30 dias 90 dias a la firma del contrato etc segun las condiciones comerciales del cliente');
            $table->date('delivery_date')->nullable()->default(null)->comment('fecha de entrega');
            $table->date('stop_date')->nullable()->default(null)->comment('Fecha de Paralizacion');
            $table->date('canceled_date')->nullable()->default(null)->comment('Fecha de Cancelacion');
            $table->text('comment')->nullable()->default(null);
            $table->enum('status', ['Activo', 'Inactivo', 'Paralizado', 'Cancelado', 'Fianlizado']);

            $table->index(["partner_id"], 'fk_project_projects_admin_partners1_idx');

            $table->index(["company_id"], 'fk_project_projects_admin_companies1_idx');

            $table->index(["type_id"], 'fk_project_projects_project_types1_idx');

            $table->unique(["code"], 'codigo_proyecto_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('company_id', 'fk_project_projects_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('partner_id', 'fk_project_projects_admin_partners1_idx')
                ->references('id')->on('admin_partners')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('type_id', 'fk_project_projects_project_types1_idx')
                ->references('id')->on('project_types')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
