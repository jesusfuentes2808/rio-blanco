<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned();
            $table->integer('type_account_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->integer('company_id')->nullable()->default(null);
            $table->integer('client_id')->unsigned();
            $table->string('account_number');
            $table->string('account_number_format');
            $table->string('cci')->nullable()->default(null);
            $table->boolean('disabled')->default(false);
            $table->boolean('show_in_comprobants')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_bank_accounts');
    }
}
