<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_partners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned()->nullable()->default(NULL);
            $table->integer('partner_category_id')->unsigned();
            $table->integer('client_type');
            $table->string('name');
            $table->string('first_lastname');
            $table->string('second_lastname');
            $table->string('document_type');
            $table->string('document_number');
            $table->string('address')->nullable()->default(NULL);
            $table->string('street')->nullable()->default(NULL);
            $table->string('street2')->nullable()->default(NULL);
            $table->string('reference')->nullable()->default(NULL);
            $table->string('picture')->default('no-picture.png');
            $table->integer('state_id')->unsigned();
            $table->integer('province_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->boolean('use_address')->nullable()->default(NULL);
            $table->string('ean_code')->nullable()->default(NULL);
            $table->string('another_id')->nullable()->default(NULL);
            $table->string('comercial_name')->nullable()->default(NULL);
            $table->decimal('credit_limit', 12, 2)->nullable()->default(NULL);
            $table->decimal('debit_limit', 12, 2)->nullable()->default(NULL);
            $table->text('comment')->nullable()->default(NULL);
            $table->boolean('is_client');
            $table->integer('sale_rate_id')->unsigned()->nullable()->default(NULL);
            $table->integer('client_payment_condition')->unsigned()->nullable()->default(NULL);
            $table->boolean('is_provider');
            $table->integer('provider_currency')->unsigned()->nullable()->default(NULL);
            $table->integer('provider_payment_condition')->unsigned()->nullable()->default(NULL);
            $table->boolean('is_international');
            $table->integer('country_id')->unsigned()->nullable()->default(NULL);
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_partners');
    }
}
