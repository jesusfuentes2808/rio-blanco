<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectProjectTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_projects';

    /**
     * Run the migrations.
     * @table project_project
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('partner_id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('code', 13)->comment('concatenar con id para completar con ceros a 13 digitos PRO0000000001');
            $table->string('contract_number', 24)->nullable()->comment('nro contrato orden de servicio orde de compra etc');
            $table->string('reference')->nullable()->comment('Referencia del proyecto');
            $table->string('name', 128)->comment('nombre del proyecto');

            $table->date('start_date');
            $table->date('end_date');
            $table->string('delivery_conditions')->nullable()->comment('15 dias 30 dias 90 dias a la firma del contrato etc segun las condiciones comerciales del cliente');
            $table->date('delivery_date')->nullable()->comment('fecha de entrega');

            $table->text('comment')->nullable();

            $table->enum('status', ['Activo', 'Inactivo', 'Paralizado', 'Cancelado', 'Fianlizado']);
            $table->timestamps();

            $table->index(["type_id"], 'fk_project_project_project_type1_idx');

            $table->index(["company_id"], 'fk_project_project_admin_company1_idx');

            $table->index(["partner_id"], 'fk_project_project_admin_partner1_idx');

            $table->unique(["code"], 'codigo_proyecto_UNIQUE');


            $table->foreign('company_id')
                ->references('id')->on('admin_company')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('type_id')
                ->references('id')->on('project_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('partner_id')
                ->references('id')->on('admin_partner')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
