<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_cost_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->integer('managements_id')->unsigned();
            $table->integer('section_id')->unsigned();
            $table->integer('cost_type_id')->unsigned();
            $table->integer('cost_center_type_id')->unsigned();
            $table->integer('work_group_id')->unsigned();
            $table->integer('parent_id')->unsigned();
            $table->string('code');
            $table->string('name');
            $table->boolean('supply_to_use_future_operations');
            $table->boolean('supply_other_project');
            /*$table->integer('level');
            $table->boolean('active');*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_cost_centers');
    }
}
