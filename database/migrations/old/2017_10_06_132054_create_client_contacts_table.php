<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_client_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->string('name')->nullable()->default(NULL);
            $table->string('phone')->nullable()->default(NULL);
            $table->string('fax')->nullable()->default(NULL);
            $table->string('cellphone')->nullable()->default(NULL);
            $table->string('email')->nullable()->default(NULL);
            $table->string('web')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_client_contacts');
    }
}
