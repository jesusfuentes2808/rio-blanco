<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitMeasuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_unit_measures', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sunat_code')->nullable();
            $table->string('sunat_name')->nullable();
            $table->string('own_code')->nullable();
            $table->string('own_name')->nullable();
            $table->string('coment');
            $table->string('um_type');
            $table->integer('basic_unit_measure_category_id');
            $table->decimal('rounding_um', 12, 2);
            $table->decimal('factor_um', 12, 2);
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_unit_measures');
    }
}
