<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_partner_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('partner_id')->unsigned();
            $table->string('type');
            $table->string('address')->nullable()->default(NULL);
            $table->string('address_aditional')->nullable()->default(NULL);
            $table->string('reference')->nullable()->default(NULL);
            $table->string('city')->nullable()->default(NULL);
            $table->string('zip')->nullable()->default(NULL);
            $table->integer('state')->unsigned();
            $table->integer('province')->unsigned();
            $table->integer('district')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_partner_addresses');
    }
}
