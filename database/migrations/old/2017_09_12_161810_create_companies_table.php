<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ruc');
            $table->string('slogan')->nullable()->default(NULL);
            $table->string('street');
            $table->string('street2')->nullable()->default(NULL);
            $table->string('reference');
            $table->integer('state_id')->unsigned();
            $table->integer('province_id')->unsigned();
            $table->integer('district_id')->unsigned();
            $table->string('zip')->nullable()->default(NULL);
            $table->string('anotherCity')->nullable()->default(NULL);
            $table->string('website')->nullable()->default(NULL);
            $table->string('contact');
            $table->string('phone');
            $table->string('cellphone');
            $table->string('fax')->nullable()->default(NULL);
            $table->string('email');
            $table->string('register')->nullable()->default(NULL);
            $table->integer('currency_id')->unsigned();
            $table->string('picture')->default('no-picture.png');
            $table->boolean('status')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_companies');
    }
}
