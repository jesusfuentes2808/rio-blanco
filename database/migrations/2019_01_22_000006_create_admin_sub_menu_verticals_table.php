<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminSubMenuVerticalsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_sub_menu_verticals';

    /**
     * Run the migrations.
     * @table admin_sub_menu_verticals
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('menu_vertical_id');
            $table->string('name', 64);
            $table->string('route', 128);

            $table->index(["menu_vertical_id"], 'fk_admin_sub_menu_verticals_admin_menu_verticals2_idx');
            $table->nullableTimestamps();


            $table->foreign('menu_vertical_id', 'fk_admin_sub_menu_verticals_admin_menu_verticals2_idx')
                ->references('id')->on('admin_menu_verticals')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
