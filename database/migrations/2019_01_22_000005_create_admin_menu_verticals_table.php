<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminMenuVerticalsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_menu_verticals';

    /**
     * Run the migrations.
     * @table admin_menu_verticals
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('menu_horizontal_id');
            $table->string('name', 64);
            $table->string('icon', 32);

            $table->index(["menu_horizontal_id"], 'fk_admin_menu_verticals_admin_menu_horizontals2_idx');
            $table->nullableTimestamps();


            $table->foreign('menu_horizontal_id', 'fk_admin_menu_verticals_admin_menu_horizontals2_idx')
                ->references('id')->on('admin_menu_horizontals')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
