<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameVariousTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Schema::rename('admin_address_type', 'admin_address_types');
        //Schema::rename('admin_contacts_type', 'admin_contacts_types');
        //Schema::rename('project_type', 'project_types');
        //Schema::rename('project_project', 'project_projects');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::rename('admin_address_types', 'admin_address_type');
        Schema::rename('admin_contacts_types', 'admin_contacts_type');
        Schema::rename('project_types', 'project_type');
        Schema::rename('project_projects', 'project_project');
    }
}
