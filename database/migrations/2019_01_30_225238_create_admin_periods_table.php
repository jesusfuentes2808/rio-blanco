<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPeriodsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_periods';

    /**
     * Run the migrations.
     * @table admin_periods
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('fiscalyear_id');
            $table->string('code', 14)->comment('01/2019');
            $table->string('name', 45)->comment('Enero-2019');
            $table->date('date_from')->comment('Fecha inicio primer dia del mes');
            $table->date('date_to')->comment('Fecha final ultimo dia del mes');
            $table->tinyInteger('open_clouse')->nullable()->comment('abierto o cerrado');
            $table->tinyInteger('status')->nullable()->comment('activo o inactivo');

            $table->index(["company_id"], 'fk_admin_periods_admin_companies1_idx');

            $table->index(["fiscalyear_id"], 'fk_admin_periods_admin_fiscalyear1_idx');
            $table->nullableTimestamps();


            $table->foreign('company_id', 'fk_admin_periods_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('fiscalyear_id', 'fk_admin_periods_admin_fiscalyear1_idx')
                ->references('id')->on('admin_fiscalyear')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
