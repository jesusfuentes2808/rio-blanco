<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicWorkFrontsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'basic_work_fronts';

    /**
     * Run the migrations.
     * @table basic_work_fronts
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('sub_section_id');
            $table->string('name');
            $table->text('notes')->nullable()->default(null);

            $table->index(["sub_section_id"], 'fk_basic_work_fronts_basic_sub_sections1_idx');
            $table->nullableTimestamps();


            $table->foreign('sub_section_id', 'fk_basic_work_fronts_basic_sub_sections1_idx')
                ->references('id')->on('basic_sub_sections')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
