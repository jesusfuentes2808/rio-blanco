<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminFiscalyearTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_fiscalyear';

    /**
     * Run the migrations.
     * @table admin_fiscalyear
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('code', 8)->comment('X2019');
            $table->string('name', 45)->comment('2018');
            $table->date('date_from')->comment('Fecha de inicio 1er dia del año');
            $table->date('date_to')->comment('Fecha final es el ultimo dia del año');
            $table->tinyInteger('status')->nullable()->comment('activo inactivo');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
