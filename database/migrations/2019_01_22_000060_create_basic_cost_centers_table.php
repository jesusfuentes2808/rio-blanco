<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicCostCentersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'basic_cost_centers';

    /**
     * Run the migrations.
     * @table basic_cost_centers
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('parent_id');
            $table->unsignedInteger('project_id');
            $table->unsignedInteger('company_id');
            $table->unsignedInteger('cost_type_id');
            $table->unsignedInteger('cost_center_type_id');
            $table->string('code', 24);
            $table->string('name');
            $table->unsignedInteger('unit_id');
            $table->unsignedInteger('section_id');
            $table->unsignedInteger('managements_id');
            $table->unsignedInteger('work_group_id');
            $table->tinyInteger('status');
            $table->tinyInteger('supply_other_project');

            $table->index(["cost_type_id"], 'fk_basic_cost_centers_basic_cost_types1_idx');

            $table->index(["cost_center_type_id"], 'fk_basic_cost_centers_basic_cost_center_types1_idx');

            $table->index(["parent_id"], 'fk_basic_cost_centers_basic_cost_centers1_idx');

            $table->index(["work_group_id"], 'fk_basic_cost_centers_basic_work_groups1_idx');

            $table->index(["managements_id"], 'fk_basic_cost_centers_basic_managements1_idx');

            $table->index(["section_id"], 'fk_basic_cost_centers_basic_sections1_idx');

            $table->index(["project_id", "company_id"], 'fk_basic_cost_centers_project_projects1_idx');

            $table->index(["unit_id"], 'fk_basic_cost_centers_basic_unit_measures1_idx');

            $table->unique(["code"], 'code_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('cost_center_type_id', 'fk_basic_cost_centers_basic_cost_center_types1_idx')
                ->references('id')->on('basic_cost_center_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('parent_id', 'fk_basic_cost_centers_basic_cost_centers1_idx')
                ->references('id')->on('basic_cost_centers')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cost_type_id', 'fk_basic_cost_centers_basic_cost_types1_idx')
                ->references('id')->on('basic_cost_types')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('managements_id', 'fk_basic_cost_centers_basic_managements1_idx')
                ->references('id')->on('basic_managements')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('section_id', 'fk_basic_cost_centers_basic_sections1_idx')
                ->references('id')->on('basic_sections')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('unit_id', 'fk_basic_cost_centers_basic_unit_measures1_idx')
                ->references('id')->on('basic_unit_measures')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('work_group_id', 'fk_basic_cost_centers_basic_work_groups1_idx')
                ->references('id')->on('basic_work_groups')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('project_id', 'fk_basic_cost_centers_project_projects1_idx')
                ->references('id')->on('project_projects')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
