<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminBanksTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_banks';

    /**
     * Run the migrations.
     * @table admin_banks
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('country_id');
            $table->string('swift_code', 8);
            $table->string('short_name', 45)->nullable()->default(null);
            $table->string('name', 128);
            $table->string('city', 64)->nullable()->default(null);
            $table->string('branch_code', 3)->nullable()->default(null)->comment('Branch Code o Office Code');
            $table->string('address', 128)->nullable()->default(null);
            $table->string('zip', 5)->nullable()->default(null)->comment('Codigo Ciudad');
            $table->tinyInteger('active');

            $table->index(["country_id"], 'fk_admin_banks_admin_countries1_idx');

            $table->unique(["swift_code"], 'swift_code_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('country_id', 'fk_admin_banks_admin_countries1_idx')
                ->references('id')->on('admin_countries')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
