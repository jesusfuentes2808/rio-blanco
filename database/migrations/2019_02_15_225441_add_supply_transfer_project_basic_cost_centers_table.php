<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSupplyTransferProjectBasicCostCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('basic_cost_centers', function (Blueprint $table) {
            //
            $table->tinyInteger('supply_transfer_project')->default(0)->after('supply_other_project');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('basic_cost_centers', function (Blueprint $table) {
            //
            $table->dropColumn('supply_transfer_project');
        });
    }
}
