<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicResponsiblesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'basic_responsibles';

    /**
     * Run the migrations.
     * @table basic_responsibles
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('responsible_type_id');
            $table->string('name', 64);
            $table->string('position', 64);

            $table->index(["responsible_type_id"], 'fk_basic_responsibles_basic_responsible_types1_idx');
            $table->nullableTimestamps();


            $table->foreign('responsible_type_id', 'fk_basic_responsibles_basic_responsible_types1_idx')
                ->references('id')->on('basic_responsible_types')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
