<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminBankAccountsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_bank_accounts';

    /**
     * Run the migrations.
     * @table admin_bank_accounts
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('bank_id');
            $table->unsignedInteger('type_account_id');
            $table->unsignedInteger('currency_id');
            $table->unsignedInteger('company_id')->nullable()->default(null);
            $table->unsignedInteger('partner_id')->nullable()->default(null);
            $table->string('account_number', 32);
            $table->string('account_number_format', 32)->nullable()->default(null);
            $table->string('cci', 32)->nullable()->default(null);
            $table->tinyInteger('disabled')->default('0');
            $table->tinyInteger('show_in_vouchers')->nullable()->default('0')->comment('Solo mostrar en el registro de empresas');

            $table->index(["partner_id"], 'fk_admin_bank_accounts_admin_partners1_idx');

            $table->index(["bank_id"], 'fk_admin_bank_accounts_admin_banks1_idx');

            $table->index(["company_id"], 'fk_admin_bank_accounts_admin_companies1_idx');

            $table->index(["currency_id"], 'fk_admin_bank_accounts_admin_currencies1_idx');

            $table->index(["type_account_id"], 'fk_admin_bank_accounts_admin_bank_type_accounts1_idx');
            $table->nullableTimestamps();


            $table->foreign('type_account_id', 'fk_admin_bank_accounts_admin_bank_type_accounts1_idx')
                ->references('id')->on('admin_bank_type_accounts')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('bank_id', 'fk_admin_bank_accounts_admin_banks1_idx')
                ->references('id')->on('admin_banks')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('company_id', 'fk_admin_bank_accounts_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('currency_id', 'fk_admin_bank_accounts_admin_currencies1_idx')
                ->references('id')->on('admin_currencies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('partner_id', 'fk_admin_bank_accounts_admin_partners1_idx')
                ->references('id')->on('admin_partners')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
