<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCountriesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_countries';

    /**
     * Run the migrations.
     * @table admin_countries
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('currency_id');
            $table->string('iso2', 2);
            $table->string('iso3', 3);
            $table->string('iso_numeric', 3);
            $table->string('name', 80);
            $table->string('phone_code', 3);

            $table->index(["currency_id"], 'fk_admin_countries_admin_currencies1_idx');

            $table->unique(["iso2"], 'iso2_UNIQUE');

            $table->unique(["iso3"], 'iso3_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('currency_id', 'fk_admin_countries_admin_currencies1_idx')
                ->references('id')->on('admin_currencies')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
