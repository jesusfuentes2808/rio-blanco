<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminContactsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_contacts';

    /**
     * Run the migrations.
     * @table admin_contacts
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('company_id')->nullable()->default(null);
            $table->unsignedInteger('partner_id')->nullable()->default(null);
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('type');
            $table->string('name', 64)->nullable()->default(null);
            $table->string('position', 64)->nullable()->default(null);
            $table->string('phone', 32)->nullable()->default(null);
            $table->string('cellphone', 32)->nullable()->default(null);
            $table->string('fax', 24)->nullable()->default(null);
            $table->string('email', 128)->nullable()->default(null);
            $table->string('website')->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);

            $table->index(["partner_id"], 'fk_admin_contacts_admin_partners1_idx');

            $table->index(["company_id"], 'fk_admin_contacts_admin_companies1_idx');

            $table->index(["type_id"], 'fk_admin_contacts_admin_contacts_type1_idx');
            $table->nullableTimestamps();


            $table->foreign('company_id', 'fk_admin_contacts_admin_companies1_idx')
                ->references('id')->on('admin_companies')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('type_id', 'fk_admin_contacts_admin_contacts_type1_idx')
                ->references('id')->on('admin_contacts_type')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('partner_id', 'fk_admin_contacts_admin_partners1_idx')
                ->references('id')->on('admin_partners')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
