<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCurrenciesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'admin_currencies';

    /**
     * Run the migrations.
     * @table admin_currencies
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('iso', 3);
            $table->string('iso_numeric', 3)->nullable()->default(null);
            $table->string('name', 80)->nullable()->default(null);
            $table->string('symbol', 5)->nullable()->default(null);
            $table->enum('position', ['Antes', 'Despues'])->nullable()->default(null)->comment('Es para posicionar el simbolo de la moneda');
            $table->decimal('round', 12, 4)->nullable()->default(null);
            $table->tinyInteger('base');
            $table->tinyInteger('active');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
