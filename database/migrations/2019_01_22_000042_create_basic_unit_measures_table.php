<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicUnitMeasuresTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'basic_unit_measures';

    /**
     * Run the migrations.
     * @table basic_unit_measures
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('sunat_code', 3);
            $table->string('sunat_name', 128);
            $table->string('own_code', 3)->nullable()->default(null);
            $table->string('own_name', 128)->nullable()->default(null);
            $table->string('coment')->nullable()->default(null);
            $table->enum('um_type', ['Mediano', 'Grande'])->default('Mediano');
            $table->unsignedInteger('um_category_id');
            $table->decimal('rounding_um', 12, 2)->nullable()->default(null);
            $table->decimal('factor_um', 12, 2)->nullable()->default(null);
            $table->tinyInteger('active');

            $table->index(["um_category_id"], 'fk_basic_unit_measures_basic_unit_measure_categories1_idx');

            $table->unique(["own_code"], 'own_code_UNIQUE');

            $table->unique(["sunat_code"], 'sunat_code_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('um_category_id', 'fk_basic_unit_measures_basic_unit_measure_categories1_idx')
                ->references('id')->on('basic_unit_measure_categories')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
