<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PaymentTypes = '
      [
        {
            "id": "1",
            "name": "EFECTIVO",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "name": "TRANSFERENCIA BANCARIA",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "name": "CHEQUE",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "name": "APLICAR CREDITO",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "name": "DEBITO",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "name": "TARJETA VISA",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "name": "MASTERCARD",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "name": "AMERICAN EXPRESS",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "9",
            "name": "DISCOVER",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "10",
            "name": "DINERS",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "11",
            "name": "UNIONPAY",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "12",
            "name": "JCB",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "13",
            "name": "MAESTRO",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "14",
            "name": "EUROCARD",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "15",
            "name": "PAYPAL",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "16",
            "name": "GOOGLE WALLET",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "17",
            "name": "OTRA TARJETA DE CREDITO",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "18",
            "name": "GIRO POSTAL",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "19",
            "name": "BITCOIN",
            "icon": "",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $PaymentTypes = json_decode($PaymentTypes);

        foreach ($PaymentTypes as $ptypes) {
          DB::table('basic_payment_ptypes')->insert([
            'name'          => $ptypes->name,
            'icon'          => $ptypes->icon,
            'created_at'    => $ptypes->created_at,
            'updated_at'    => $ptypes->updated_at
          ]);
        }
    }
}
