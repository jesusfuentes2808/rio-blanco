<?php

use Illuminate\Database\Seeder;

class MenuVerticalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $MenuVertical = '
      [
        {
            "id": "1",
            "menu_horizontal_id": "1",
            "name": "Administración",
            "icon": "fa-cog",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "menu_horizontal_id": "1",
            "name": "Datos Básicos",
            "icon": "fa-edit",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "menu_horizontal_id": "1",
            "name": "Configuración",
            "icon": "fa-tasks",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "menu_horizontal_id": "3",
            "name": "Gestión de Ventas",
            "icon": "fa-money",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "menu_horizontal_id": "3",
            "name": "Datos Básicos",
            "icon": "fa-edit",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "menu_horizontal_id": "3",
            "name": "Configuración",
            "icon": "fa-tasks",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $MenuVertical = json_decode($MenuVertical);

        foreach ($MenuVertical as $mvertical) {
          DB::table('admin_menu_verticals')->insert([
            'menu_horizontal_id'  => $mvertical->menu_horizontal_id,
            'name'                => $mvertical->name,
            'icon'                => $mvertical->icon,
            'created_at'          => $mvertical->created_at,
            'updated_at'          => $mvertical->updated_at
          ]);
        }
    }
}
