<?php

use Illuminate\Database\Seeder;

class SubMenuVerticalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $SubMenuVertical = '
      [
        {
            "id": "1",
            "menu_vertical_id": "1",
            "name": "Usuarios",
            "route": "/users",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "menu_vertical_id": "1",
            "name": "Compañías",
            "route": "/companies",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "menu_vertical_id": "1",
            "name": "Año Fiscal",
            "route": "/fiscal-year",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "menu_vertical_id": "1",
            "name": "Periodos",
            "route": "/periods",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "menu_vertical_id": "3",
            "name": "Bancos",
            "route": "/banks",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "menu_vertical_id": "3",
            "name": "Tipo de Cuentas",
            "route": "/type-accounts",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "menu_vertical_id": "3",
            "name": "Categoría Socio",
            "route": "/partner-category",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "menu_vertical_id": "3",
            "name": "Tipo Documento",
            "route": "/documents-type",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "9",
            "menu_vertical_id": "3",
            "name": "Tarifas de Venta",
            "route": "/sales-rate",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "10",
            "menu_vertical_id": "3",
            "name": "Condiciones de Pago",
            "route": "/payment-conditions",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "11",
            "menu_vertical_id": "3",
            "name": "Tipos de datos contactos",
            "route": "/type-contacts",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "12",
            "menu_vertical_id": "3",
            "name": "Tipo/Categoría Dirección",
            "route": "/type-addresses",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "13",
            "menu_vertical_id": "2",
            "name": "Centro de costos",
            "route": "/cost-center",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "14",
            "menu_vertical_id": "2",
            "name": "Tipo Centro de Costos",
            "route": "/type-cost-center",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "15",
            "menu_vertical_id": "2",
            "name": "Tipo de Costo",
            "route": "/cost-types",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "16",
            "menu_vertical_id": "2",
            "name": "Gerencias",
            "route": "/managements",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "17",
            "menu_vertical_id": "2",
            "name": "Responsables",
            "route": "/responsibles",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "18",
            "menu_vertical_id": "2",
            "name": "Tipo de Responsables",
            "route": "/responsible-types",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "19",
            "menu_vertical_id": "2",
            "name": "Unidades de Medida",
            "route": "/unit-measures",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "20",
            "menu_vertical_id": "2",
            "name": "Categorias Unidades de Medida",
            "route": "/unit-measure-categories",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "21",
            "menu_vertical_id": "2",
            "name": "Secciones",
            "route": "/sections",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "22",
            "menu_vertical_id": "2",
            "name": "Sub Secciones",
            "route": "/sub-sections",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "23",
            "menu_vertical_id": "3",
            "name": "Tipo de Proyectos",
            "route": "/project-types",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "24",
            "menu_vertical_id": "3",
            "name": "Proyectos",
            "route": "/projects",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "25",
            "menu_vertical_id": "4",
            "name": "Propuesta (Cotización)",
            "route": "/quotation",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "26",
            "menu_vertical_id": "4",
            "name": "Pedido de Venta",
            "route": "/sale-order",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "27",
            "menu_vertical_id": "4",
            "name": "Facturación",
            "route": "/billing",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "28",
            "menu_vertical_id": "4",
            "name": "Products/Servicios",
            "route": "/products-services",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "29",
            "menu_vertical_id": "4",
            "name": "Clientes",
            "route": "/clients",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $SubMenuVertical = json_decode($SubMenuVertical);

        foreach ($SubMenuVertical as $submenu) {
          DB::table('admin_sub_menu_verticals')->insert([
            'menu_vertical_id'=> $submenu->menu_vertical_id,
            'name'            => $submenu->name,
            'route'           => $submenu->route,
            'created_at'      => $submenu->created_at,
            'updated_at'      => $submenu->updated_at
          ]);
        }
    }
}
