<?php

use Illuminate\Database\Seeder;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = '
			[
        {
            "id": "1",
            "province_id": "1",
            "ubigeo": "01",
            "code": "010101",
            "name": "CHACHAPOYAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "province_id": "1",
            "ubigeo": "02",
            "code": "010102",
            "name": "ASUNCIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "province_id": "1",
            "ubigeo": "03",
            "code": "010103",
            "name": "BALSAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "province_id": "1",
            "ubigeo": "04",
            "code": "010104",
            "name": "CHETO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "province_id": "1",
            "ubigeo": "05",
            "code": "010105",
            "name": "CHILIQUIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "province_id": "1",
            "ubigeo": "06",
            "code": "010106",
            "name": "CHUQUIBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "province_id": "1",
            "ubigeo": "07",
            "code": "010107",
            "name": "GRANADA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "province_id": "1",
            "ubigeo": "08",
            "code": "010108",
            "name": "HUANCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "9",
            "province_id": "1",
            "ubigeo": "09",
            "code": "010109",
            "name": "LA JALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "10",
            "province_id": "1",
            "ubigeo": "10",
            "code": "010110",
            "name": "LEIMEBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "11",
            "province_id": "1",
            "ubigeo": "11",
            "code": "010111",
            "name": "LEVANTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "12",
            "province_id": "1",
            "ubigeo": "12",
            "code": "010112",
            "name": "MAGDALENA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "13",
            "province_id": "1",
            "ubigeo": "13",
            "code": "010113",
            "name": "MARISCAL CASTILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "14",
            "province_id": "1",
            "ubigeo": "14",
            "code": "010114",
            "name": "MOLINOPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "15",
            "province_id": "1",
            "ubigeo": "15",
            "code": "010115",
            "name": "MONTEVIDEO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "16",
            "province_id": "1",
            "ubigeo": "16",
            "code": "010116",
            "name": "OLLEROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "17",
            "province_id": "1",
            "ubigeo": "17",
            "code": "010117",
            "name": "QUINJALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "18",
            "province_id": "1",
            "ubigeo": "18",
            "code": "010118",
            "name": "SAN FRANCISCO DE DAGUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "19",
            "province_id": "1",
            "ubigeo": "19",
            "code": "010119",
            "name": "SAN ISIDRO DE MAINO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "20",
            "province_id": "1",
            "ubigeo": "20",
            "code": "010120",
            "name": "SOLOCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "21",
            "province_id": "1",
            "ubigeo": "21",
            "code": "010121",
            "name": "SONCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "22",
            "province_id": "2",
            "ubigeo": "01",
            "code": "010201",
            "name": "BAGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "23",
            "province_id": "2",
            "ubigeo": "02",
            "code": "010202",
            "name": "ARAMANGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "24",
            "province_id": "2",
            "ubigeo": "03",
            "code": "010203",
            "name": "COPALLIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "25",
            "province_id": "2",
            "ubigeo": "04",
            "code": "010204",
            "name": "EL PARCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "26",
            "province_id": "2",
            "ubigeo": "05",
            "code": "010205",
            "name": "IMAZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "27",
            "province_id": "2",
            "ubigeo": "06",
            "code": "010206",
            "name": "LA PECA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "28",
            "province_id": "3",
            "ubigeo": "01",
            "code": "010301",
            "name": "JUMBILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "29",
            "province_id": "3",
            "ubigeo": "02",
            "code": "010302",
            "name": "CHISQUILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "30",
            "province_id": "3",
            "ubigeo": "03",
            "code": "010303",
            "name": "CHURUJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "31",
            "province_id": "3",
            "ubigeo": "04",
            "code": "010304",
            "name": "COROSHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "32",
            "province_id": "3",
            "ubigeo": "05",
            "code": "010305",
            "name": "CUISPES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "33",
            "province_id": "3",
            "ubigeo": "06",
            "code": "010306",
            "name": "FLORIDA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "34",
            "province_id": "3",
            "ubigeo": "07",
            "code": "010307",
            "name": "JAZAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "35",
            "province_id": "3",
            "ubigeo": "08",
            "code": "010308",
            "name": "RECTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "36",
            "province_id": "3",
            "ubigeo": "09",
            "code": "010309",
            "name": "SAN CARLOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "37",
            "province_id": "3",
            "ubigeo": "10",
            "code": "010310",
            "name": "SHIPASBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "38",
            "province_id": "3",
            "ubigeo": "11",
            "code": "010311",
            "name": "VALERA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "39",
            "province_id": "3",
            "ubigeo": "12",
            "code": "010312",
            "name": "YAMBRASBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "40",
            "province_id": "4",
            "ubigeo": "01",
            "code": "010401",
            "name": "NIEVA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "41",
            "province_id": "4",
            "ubigeo": "02",
            "code": "010402",
            "name": "EL CENEPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "42",
            "province_id": "4",
            "ubigeo": "03",
            "code": "010403",
            "name": "RÍO SANTIAGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "43",
            "province_id": "5",
            "ubigeo": "01",
            "code": "010501",
            "name": "LAMUD",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "44",
            "province_id": "5",
            "ubigeo": "02",
            "code": "010502",
            "name": "CAMPORREDONDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "45",
            "province_id": "5",
            "ubigeo": "03",
            "code": "010503",
            "name": "COCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "46",
            "province_id": "5",
            "ubigeo": "04",
            "code": "010504",
            "name": "COLCAMAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "47",
            "province_id": "5",
            "ubigeo": "05",
            "code": "010505",
            "name": "CONILA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "48",
            "province_id": "5",
            "ubigeo": "06",
            "code": "010506",
            "name": "INGUILPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "49",
            "province_id": "5",
            "ubigeo": "07",
            "code": "010507",
            "name": "LONGUITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "50",
            "province_id": "5",
            "ubigeo": "08",
            "code": "010508",
            "name": "LONYA CHICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "51",
            "province_id": "5",
            "ubigeo": "09",
            "code": "010509",
            "name": "LUYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "52",
            "province_id": "5",
            "ubigeo": "10",
            "code": "010510",
            "name": "LUYA VIEJO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "53",
            "province_id": "5",
            "ubigeo": "11",
            "code": "010511",
            "name": "MARÍA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "54",
            "province_id": "5",
            "ubigeo": "12",
            "code": "010512",
            "name": "OCALLI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "55",
            "province_id": "5",
            "ubigeo": "13",
            "code": "010513",
            "name": "OCUMAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "56",
            "province_id": "5",
            "ubigeo": "14",
            "code": "010514",
            "name": "PISUQUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "57",
            "province_id": "5",
            "ubigeo": "15",
            "code": "010515",
            "name": "PROVIDENCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "58",
            "province_id": "5",
            "ubigeo": "16",
            "code": "010516",
            "name": "SAN CRISTÓBAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "59",
            "province_id": "5",
            "ubigeo": "17",
            "code": "010517",
            "name": "SAN FRANCISCO DE YESO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "60",
            "province_id": "5",
            "ubigeo": "18",
            "code": "010518",
            "name": "SAN JERÓNIMO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "61",
            "province_id": "5",
            "ubigeo": "19",
            "code": "010519",
            "name": "SAN JUAN DE LOPECANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "62",
            "province_id": "5",
            "ubigeo": "20",
            "code": "010520",
            "name": "SANTA CATALINA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "63",
            "province_id": "5",
            "ubigeo": "21",
            "code": "010521",
            "name": "SANTO TOMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "64",
            "province_id": "5",
            "ubigeo": "22",
            "code": "010522",
            "name": "TINGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "65",
            "province_id": "5",
            "ubigeo": "23",
            "code": "010523",
            "name": "TRITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "66",
            "province_id": "6",
            "ubigeo": "01",
            "code": "010601",
            "name": "SAN NICOLÁS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "67",
            "province_id": "6",
            "ubigeo": "02",
            "code": "010602",
            "name": "CHIRIMOTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "68",
            "province_id": "6",
            "ubigeo": "03",
            "code": "010603",
            "name": "COCHAMAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "69",
            "province_id": "6",
            "ubigeo": "04",
            "code": "010604",
            "name": "HUAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "70",
            "province_id": "6",
            "ubigeo": "05",
            "code": "010605",
            "name": "LIMABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "71",
            "province_id": "6",
            "ubigeo": "06",
            "code": "010606",
            "name": "LONGAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "72",
            "province_id": "6",
            "ubigeo": "07",
            "code": "010607",
            "name": "MARISCAL BENAVIDES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "73",
            "province_id": "6",
            "ubigeo": "08",
            "code": "010608",
            "name": "MILPUC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "74",
            "province_id": "6",
            "ubigeo": "09",
            "code": "010609",
            "name": "OMIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "75",
            "province_id": "6",
            "ubigeo": "10",
            "code": "010610",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "76",
            "province_id": "6",
            "ubigeo": "11",
            "code": "010611",
            "name": "TOTORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "77",
            "province_id": "6",
            "ubigeo": "12",
            "code": "010612",
            "name": "VISTA ALEGRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "78",
            "province_id": "7",
            "ubigeo": "01",
            "code": "010701",
            "name": "BAGUA GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "79",
            "province_id": "7",
            "ubigeo": "02",
            "code": "010702",
            "name": "CAJARURO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "80",
            "province_id": "7",
            "ubigeo": "03",
            "code": "010703",
            "name": "CUMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "81",
            "province_id": "7",
            "ubigeo": "04",
            "code": "010704",
            "name": "EL MILAGRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "82",
            "province_id": "7",
            "ubigeo": "05",
            "code": "010705",
            "name": "JAMALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "83",
            "province_id": "7",
            "ubigeo": "06",
            "code": "010706",
            "name": "LONYA GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "84",
            "province_id": "7",
            "ubigeo": "07",
            "code": "010707",
            "name": "YAMON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "85",
            "province_id": "8",
            "ubigeo": "01",
            "code": "020101",
            "name": "HUARAZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "86",
            "province_id": "8",
            "ubigeo": "02",
            "code": "020102",
            "name": "COCHABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "87",
            "province_id": "8",
            "ubigeo": "03",
            "code": "020103",
            "name": "COLCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "88",
            "province_id": "8",
            "ubigeo": "04",
            "code": "020104",
            "name": "HUANCHAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "89",
            "province_id": "8",
            "ubigeo": "05",
            "code": "020105",
            "name": "INDEPENDENCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "90",
            "province_id": "8",
            "ubigeo": "06",
            "code": "020106",
            "name": "JANGAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "91",
            "province_id": "8",
            "ubigeo": "07",
            "code": "020107",
            "name": "LA LIBERTAD",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "92",
            "province_id": "8",
            "ubigeo": "08",
            "code": "020108",
            "name": "OLLEROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "93",
            "province_id": "8",
            "ubigeo": "09",
            "code": "020109",
            "name": "PAMPAS GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "94",
            "province_id": "8",
            "ubigeo": "10",
            "code": "020110",
            "name": "PARIACOTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "95",
            "province_id": "8",
            "ubigeo": "11",
            "code": "020111",
            "name": "PIRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "96",
            "province_id": "8",
            "ubigeo": "12",
            "code": "020112",
            "name": "TARICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "97",
            "province_id": "9",
            "ubigeo": "01",
            "code": "020201",
            "name": "AIJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "98",
            "province_id": "9",
            "ubigeo": "02",
            "code": "020202",
            "name": "CORIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "99",
            "province_id": "9",
            "ubigeo": "03",
            "code": "020203",
            "name": "HUACLLAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "100",
            "province_id": "9",
            "ubigeo": "04",
            "code": "020204",
            "name": "LA MERCED",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "101",
            "province_id": "9",
            "ubigeo": "05",
            "code": "020205",
            "name": "SUCCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "102",
            "province_id": "10",
            "ubigeo": "01",
            "code": "020301",
            "name": "LLAMELLIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "103",
            "province_id": "10",
            "ubigeo": "02",
            "code": "020302",
            "name": "ACZO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "104",
            "province_id": "10",
            "ubigeo": "03",
            "code": "020303",
            "name": "CHACCHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "105",
            "province_id": "10",
            "ubigeo": "04",
            "code": "020304",
            "name": "CHINGAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "106",
            "province_id": "10",
            "ubigeo": "05",
            "code": "020305",
            "name": "MIRGAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "107",
            "province_id": "10",
            "ubigeo": "06",
            "code": "020306",
            "name": "SAN JUAN DE RONTOY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "108",
            "province_id": "11",
            "ubigeo": "01",
            "code": "020401",
            "name": "CHACAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "109",
            "province_id": "11",
            "ubigeo": "02",
            "code": "020402",
            "name": "ACOCHACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "110",
            "province_id": "12",
            "ubigeo": "01",
            "code": "020501",
            "name": "CHIQUIAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "111",
            "province_id": "12",
            "ubigeo": "02",
            "code": "020502",
            "name": "ABELARDO PARDO LEZAMETA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "112",
            "province_id": "12",
            "ubigeo": "03",
            "code": "020503",
            "name": "ANTONIO RAYMONDI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "113",
            "province_id": "12",
            "ubigeo": "04",
            "code": "020504",
            "name": "AQUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "114",
            "province_id": "12",
            "ubigeo": "05",
            "code": "020505",
            "name": "CAJACAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "115",
            "province_id": "12",
            "ubigeo": "06",
            "code": "020506",
            "name": "CANIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "116",
            "province_id": "12",
            "ubigeo": "07",
            "code": "020507",
            "name": "COLQUIOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "117",
            "province_id": "12",
            "ubigeo": "08",
            "code": "020508",
            "name": "HUALLANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "118",
            "province_id": "12",
            "ubigeo": "09",
            "code": "020509",
            "name": "HUASTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "119",
            "province_id": "12",
            "ubigeo": "10",
            "code": "020510",
            "name": "HUAYLLACAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "120",
            "province_id": "12",
            "ubigeo": "11",
            "code": "020511",
            "name": "LA PRIMAVERA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "121",
            "province_id": "12",
            "ubigeo": "12",
            "code": "020512",
            "name": "MANGAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "122",
            "province_id": "12",
            "ubigeo": "13",
            "code": "020513",
            "name": "PACLLON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "123",
            "province_id": "12",
            "ubigeo": "14",
            "code": "020514",
            "name": "SAN MIGUEL DE CORPANQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "124",
            "province_id": "12",
            "ubigeo": "15",
            "code": "020515",
            "name": "TICLLOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "125",
            "province_id": "13",
            "ubigeo": "01",
            "code": "020601",
            "name": "CARHUAZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "126",
            "province_id": "13",
            "ubigeo": "02",
            "code": "020602",
            "name": "ACOPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "127",
            "province_id": "13",
            "ubigeo": "03",
            "code": "020603",
            "name": "AMASHCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "128",
            "province_id": "13",
            "ubigeo": "04",
            "code": "020604",
            "name": "ANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "129",
            "province_id": "13",
            "ubigeo": "05",
            "code": "020605",
            "name": "ATAQUERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "130",
            "province_id": "13",
            "ubigeo": "06",
            "code": "020606",
            "name": "MARCARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "131",
            "province_id": "13",
            "ubigeo": "07",
            "code": "020607",
            "name": "PARIAHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "132",
            "province_id": "13",
            "ubigeo": "08",
            "code": "020608",
            "name": "SAN MIGUEL DE ACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "133",
            "province_id": "13",
            "ubigeo": "09",
            "code": "020609",
            "name": "SHILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "134",
            "province_id": "13",
            "ubigeo": "10",
            "code": "020610",
            "name": "TINCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "135",
            "province_id": "13",
            "ubigeo": "11",
            "code": "020611",
            "name": "YUNGAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "136",
            "province_id": "14",
            "ubigeo": "01",
            "code": "020701",
            "name": "SAN LUIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "137",
            "province_id": "14",
            "ubigeo": "02",
            "code": "020702",
            "name": "SAN NICOLÁS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "138",
            "province_id": "14",
            "ubigeo": "03",
            "code": "020703",
            "name": "YAUYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "139",
            "province_id": "15",
            "ubigeo": "01",
            "code": "020801",
            "name": "CASMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "140",
            "province_id": "15",
            "ubigeo": "02",
            "code": "020802",
            "name": "BUENA VISTA ALTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "141",
            "province_id": "15",
            "ubigeo": "03",
            "code": "020803",
            "name": "COMANDANTE NOEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "142",
            "province_id": "15",
            "ubigeo": "04",
            "code": "020804",
            "name": "YAUTAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "143",
            "province_id": "16",
            "ubigeo": "01",
            "code": "020901",
            "name": "CORONGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "144",
            "province_id": "16",
            "ubigeo": "02",
            "code": "020902",
            "name": "ACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "145",
            "province_id": "16",
            "ubigeo": "03",
            "code": "020903",
            "name": "BAMBAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "146",
            "province_id": "16",
            "ubigeo": "04",
            "code": "020904",
            "name": "CUSCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "147",
            "province_id": "16",
            "ubigeo": "05",
            "code": "020905",
            "name": "LA PAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "148",
            "province_id": "16",
            "ubigeo": "06",
            "code": "020906",
            "name": "YANAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "149",
            "province_id": "16",
            "ubigeo": "07",
            "code": "020907",
            "name": "YUPAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "150",
            "province_id": "17",
            "ubigeo": "01",
            "code": "021001",
            "name": "HUARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "151",
            "province_id": "17",
            "ubigeo": "02",
            "code": "021002",
            "name": "ANRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "152",
            "province_id": "17",
            "ubigeo": "03",
            "code": "021003",
            "name": "CAJAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "153",
            "province_id": "17",
            "ubigeo": "04",
            "code": "021004",
            "name": "CHAVIN DE HUANTAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "154",
            "province_id": "17",
            "ubigeo": "05",
            "code": "021005",
            "name": "HUACACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "155",
            "province_id": "17",
            "ubigeo": "06",
            "code": "021006",
            "name": "HUACCHIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "156",
            "province_id": "17",
            "ubigeo": "07",
            "code": "021007",
            "name": "HUACHIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "157",
            "province_id": "17",
            "ubigeo": "08",
            "code": "021008",
            "name": "HUANTAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "158",
            "province_id": "17",
            "ubigeo": "09",
            "code": "021009",
            "name": "MASIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "159",
            "province_id": "17",
            "ubigeo": "10",
            "code": "021010",
            "name": "PAUCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "160",
            "province_id": "17",
            "ubigeo": "11",
            "code": "021011",
            "name": "PONTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "161",
            "province_id": "17",
            "ubigeo": "12",
            "code": "021012",
            "name": "RAHUAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "162",
            "province_id": "17",
            "ubigeo": "13",
            "code": "021013",
            "name": "RAPAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "163",
            "province_id": "17",
            "ubigeo": "14",
            "code": "021014",
            "name": "SAN MARCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "164",
            "province_id": "17",
            "ubigeo": "15",
            "code": "021015",
            "name": "SAN PEDRO DE CHANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "165",
            "province_id": "17",
            "ubigeo": "16",
            "code": "021016",
            "name": "UCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "166",
            "province_id": "18",
            "ubigeo": "01",
            "code": "021101",
            "name": "HUARMEY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "167",
            "province_id": "18",
            "ubigeo": "02",
            "code": "021102",
            "name": "COCHAPETI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "168",
            "province_id": "18",
            "ubigeo": "03",
            "code": "021103",
            "name": "CULEBRAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "169",
            "province_id": "18",
            "ubigeo": "04",
            "code": "021104",
            "name": "HUAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "170",
            "province_id": "18",
            "ubigeo": "05",
            "code": "021105",
            "name": "MALVAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "171",
            "province_id": "19",
            "ubigeo": "01",
            "code": "021201",
            "name": "CARAZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "172",
            "province_id": "19",
            "ubigeo": "02",
            "code": "021202",
            "name": "HUALLANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "173",
            "province_id": "19",
            "ubigeo": "03",
            "code": "021203",
            "name": "HUATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "174",
            "province_id": "19",
            "ubigeo": "04",
            "code": "021204",
            "name": "HUAYLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "175",
            "province_id": "19",
            "ubigeo": "05",
            "code": "021205",
            "name": "MATO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "176",
            "province_id": "19",
            "ubigeo": "06",
            "code": "021206",
            "name": "PAMPAROMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "177",
            "province_id": "19",
            "ubigeo": "07",
            "code": "021207",
            "name": "PUEBLO LIBRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "178",
            "province_id": "19",
            "ubigeo": "08",
            "code": "021208",
            "name": "SANTA CRUZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "179",
            "province_id": "19",
            "ubigeo": "09",
            "code": "021209",
            "name": "SANTO TORIBIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "180",
            "province_id": "19",
            "ubigeo": "10",
            "code": "021210",
            "name": "YURACMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "181",
            "province_id": "20",
            "ubigeo": "01",
            "code": "021301",
            "name": "PISCOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "182",
            "province_id": "20",
            "ubigeo": "02",
            "code": "021302",
            "name": "CASCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "183",
            "province_id": "20",
            "ubigeo": "03",
            "code": "021303",
            "name": "ELEAZAR GUZMÁN BARRON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "184",
            "province_id": "20",
            "ubigeo": "04",
            "code": "021304",
            "name": "FIDEL OLIVAS ESCUDERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "185",
            "province_id": "20",
            "ubigeo": "05",
            "code": "021305",
            "name": "LLAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "186",
            "province_id": "20",
            "ubigeo": "06",
            "code": "021306",
            "name": "LLUMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "187",
            "province_id": "20",
            "ubigeo": "07",
            "code": "021307",
            "name": "LUCMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "188",
            "province_id": "20",
            "ubigeo": "08",
            "code": "021308",
            "name": "MUSGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "189",
            "province_id": "21",
            "ubigeo": "01",
            "code": "021401",
            "name": "OCROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "190",
            "province_id": "21",
            "ubigeo": "02",
            "code": "021402",
            "name": "ACAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "191",
            "province_id": "21",
            "ubigeo": "03",
            "code": "021403",
            "name": "CAJAMARQUILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "192",
            "province_id": "21",
            "ubigeo": "04",
            "code": "021404",
            "name": "CARHUAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "193",
            "province_id": "21",
            "ubigeo": "05",
            "code": "021405",
            "name": "COCHAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "194",
            "province_id": "21",
            "ubigeo": "06",
            "code": "021406",
            "name": "CONGAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "195",
            "province_id": "21",
            "ubigeo": "07",
            "code": "021407",
            "name": "LLIPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "196",
            "province_id": "21",
            "ubigeo": "08",
            "code": "021408",
            "name": "SAN CRISTÓBAL DE RAJAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "197",
            "province_id": "21",
            "ubigeo": "09",
            "code": "021409",
            "name": "SAN PEDRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "198",
            "province_id": "21",
            "ubigeo": "10",
            "code": "021410",
            "name": "SANTIAGO DE CHILCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "199",
            "province_id": "22",
            "ubigeo": "01",
            "code": "021501",
            "name": "CABANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "200",
            "province_id": "22",
            "ubigeo": "02",
            "code": "021502",
            "name": "BOLOGNESI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "201",
            "province_id": "22",
            "ubigeo": "03",
            "code": "021503",
            "name": "CONCHUCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "202",
            "province_id": "22",
            "ubigeo": "04",
            "code": "021504",
            "name": "HUACASCHUQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "203",
            "province_id": "22",
            "ubigeo": "05",
            "code": "021505",
            "name": "HUANDOVAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "204",
            "province_id": "22",
            "ubigeo": "06",
            "code": "021506",
            "name": "LACABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "205",
            "province_id": "22",
            "ubigeo": "07",
            "code": "021507",
            "name": "LLAPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "206",
            "province_id": "22",
            "ubigeo": "08",
            "code": "021508",
            "name": "PALLASCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "207",
            "province_id": "22",
            "ubigeo": "09",
            "code": "021509",
            "name": "PAMPAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "208",
            "province_id": "22",
            "ubigeo": "10",
            "code": "021510",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "209",
            "province_id": "22",
            "ubigeo": "11",
            "code": "021511",
            "name": "TAUCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "210",
            "province_id": "23",
            "ubigeo": "01",
            "code": "021601",
            "name": "POMABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "211",
            "province_id": "23",
            "ubigeo": "02",
            "code": "021602",
            "name": "HUAYLLAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "212",
            "province_id": "23",
            "ubigeo": "03",
            "code": "021603",
            "name": "PAROBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "213",
            "province_id": "23",
            "ubigeo": "04",
            "code": "021604",
            "name": "QUINUABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "214",
            "province_id": "24",
            "ubigeo": "01",
            "code": "021701",
            "name": "RECUAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "215",
            "province_id": "24",
            "ubigeo": "02",
            "code": "021702",
            "name": "CATAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "216",
            "province_id": "24",
            "ubigeo": "03",
            "code": "021703",
            "name": "COTAPARACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "217",
            "province_id": "24",
            "ubigeo": "04",
            "code": "021704",
            "name": "HUAYLLAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "218",
            "province_id": "24",
            "ubigeo": "05",
            "code": "021705",
            "name": "LLACLLIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "219",
            "province_id": "24",
            "ubigeo": "06",
            "code": "021706",
            "name": "MARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "220",
            "province_id": "24",
            "ubigeo": "07",
            "code": "021707",
            "name": "PAMPAS CHICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "221",
            "province_id": "24",
            "ubigeo": "08",
            "code": "021708",
            "name": "PARARIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "222",
            "province_id": "24",
            "ubigeo": "09",
            "code": "021709",
            "name": "TAPACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "223",
            "province_id": "24",
            "ubigeo": "10",
            "code": "021710",
            "name": "TICAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "224",
            "province_id": "25",
            "ubigeo": "01",
            "code": "021801",
            "name": "CHIMBOTE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "225",
            "province_id": "25",
            "ubigeo": "02",
            "code": "021802",
            "name": "CÁCERES DEL PERÚ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "226",
            "province_id": "25",
            "ubigeo": "03",
            "code": "021803",
            "name": "COISHCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "227",
            "province_id": "25",
            "ubigeo": "04",
            "code": "021804",
            "name": "MACATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "228",
            "province_id": "25",
            "ubigeo": "05",
            "code": "021805",
            "name": "MORO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "229",
            "province_id": "25",
            "ubigeo": "06",
            "code": "021806",
            "name": "NEPEÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "230",
            "province_id": "25",
            "ubigeo": "07",
            "code": "021807",
            "name": "SAMANCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "231",
            "province_id": "25",
            "ubigeo": "08",
            "code": "021808",
            "name": "SANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "232",
            "province_id": "25",
            "ubigeo": "09",
            "code": "021809",
            "name": "NUEVO CHIMBOTE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "233",
            "province_id": "26",
            "ubigeo": "01",
            "code": "021901",
            "name": "SIHUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "234",
            "province_id": "26",
            "ubigeo": "02",
            "code": "021902",
            "name": "ACOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "235",
            "province_id": "26",
            "ubigeo": "03",
            "code": "021903",
            "name": "ALFONSO UGARTE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "236",
            "province_id": "26",
            "ubigeo": "04",
            "code": "021904",
            "name": "CASHAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "237",
            "province_id": "26",
            "ubigeo": "05",
            "code": "021905",
            "name": "CHINGALPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "238",
            "province_id": "26",
            "ubigeo": "06",
            "code": "021906",
            "name": "HUAYLLABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "239",
            "province_id": "26",
            "ubigeo": "07",
            "code": "021907",
            "name": "QUICHES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "240",
            "province_id": "26",
            "ubigeo": "08",
            "code": "021908",
            "name": "RAGASH",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "241",
            "province_id": "26",
            "ubigeo": "09",
            "code": "021909",
            "name": "SAN JUAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "242",
            "province_id": "26",
            "ubigeo": "10",
            "code": "021910",
            "name": "SICSIBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "243",
            "province_id": "27",
            "ubigeo": "01",
            "code": "022001",
            "name": "YUNGAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "244",
            "province_id": "27",
            "ubigeo": "02",
            "code": "022002",
            "name": "CASCAPARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "245",
            "province_id": "27",
            "ubigeo": "03",
            "code": "022003",
            "name": "MANCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "246",
            "province_id": "27",
            "ubigeo": "04",
            "code": "022004",
            "name": "MATACOTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "247",
            "province_id": "27",
            "ubigeo": "05",
            "code": "022005",
            "name": "QUILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "248",
            "province_id": "27",
            "ubigeo": "06",
            "code": "022006",
            "name": "RANRAHIRCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "249",
            "province_id": "27",
            "ubigeo": "07",
            "code": "022007",
            "name": "SHUPLUY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "250",
            "province_id": "27",
            "ubigeo": "08",
            "code": "022008",
            "name": "YANAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "251",
            "province_id": "28",
            "ubigeo": "01",
            "code": "030101",
            "name": "ABANCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "252",
            "province_id": "28",
            "ubigeo": "02",
            "code": "030102",
            "name": "CHACOCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "253",
            "province_id": "28",
            "ubigeo": "03",
            "code": "030103",
            "name": "CIRCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "254",
            "province_id": "28",
            "ubigeo": "04",
            "code": "030104",
            "name": "CURAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "255",
            "province_id": "28",
            "ubigeo": "05",
            "code": "030105",
            "name": "HUANIPACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "256",
            "province_id": "28",
            "ubigeo": "06",
            "code": "030106",
            "name": "LAMBRAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "257",
            "province_id": "28",
            "ubigeo": "07",
            "code": "030107",
            "name": "PICHIRHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "258",
            "province_id": "28",
            "ubigeo": "08",
            "code": "030108",
            "name": "SAN PEDRO DE CACHORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "259",
            "province_id": "28",
            "ubigeo": "09",
            "code": "030109",
            "name": "TAMBURCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "260",
            "province_id": "29",
            "ubigeo": "01",
            "code": "030201",
            "name": "ANDAHUAYLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "261",
            "province_id": "29",
            "ubigeo": "02",
            "code": "030202",
            "name": "ANDARAPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "262",
            "province_id": "29",
            "ubigeo": "03",
            "code": "030203",
            "name": "CHIARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "263",
            "province_id": "29",
            "ubigeo": "04",
            "code": "030204",
            "name": "HUANCARAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "264",
            "province_id": "29",
            "ubigeo": "05",
            "code": "030205",
            "name": "HUANCARAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "265",
            "province_id": "29",
            "ubigeo": "06",
            "code": "030206",
            "name": "HUAYANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "266",
            "province_id": "29",
            "ubigeo": "07",
            "code": "030207",
            "name": "KISHUARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "267",
            "province_id": "29",
            "ubigeo": "08",
            "code": "030208",
            "name": "PACOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "268",
            "province_id": "29",
            "ubigeo": "09",
            "code": "030209",
            "name": "PACUCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "269",
            "province_id": "29",
            "ubigeo": "10",
            "code": "030210",
            "name": "PAMPACHIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "270",
            "province_id": "29",
            "ubigeo": "11",
            "code": "030211",
            "name": "POMACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "271",
            "province_id": "29",
            "ubigeo": "12",
            "code": "030212",
            "name": "SAN ANTONIO DE CACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "272",
            "province_id": "29",
            "ubigeo": "13",
            "code": "030213",
            "name": "SAN JERÓNIMO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "273",
            "province_id": "29",
            "ubigeo": "14",
            "code": "030214",
            "name": "SAN MIGUEL DE CHACCRAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "274",
            "province_id": "29",
            "ubigeo": "15",
            "code": "030215",
            "name": "SANTA MARÍA DE CHICMO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "275",
            "province_id": "29",
            "ubigeo": "16",
            "code": "030216",
            "name": "TALAVERA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "276",
            "province_id": "29",
            "ubigeo": "17",
            "code": "030217",
            "name": "TUMAY HUARACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "277",
            "province_id": "29",
            "ubigeo": "18",
            "code": "030218",
            "name": "TURPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "278",
            "province_id": "29",
            "ubigeo": "19",
            "code": "030219",
            "name": "KAQUIABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "279",
            "province_id": "29",
            "ubigeo": "20",
            "code": "030220",
            "name": "JOSÉ MARÍA ARGUEDAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "280",
            "province_id": "30",
            "ubigeo": "01",
            "code": "030301",
            "name": "ANTABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "281",
            "province_id": "30",
            "ubigeo": "02",
            "code": "030302",
            "name": "EL ORO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "282",
            "province_id": "30",
            "ubigeo": "03",
            "code": "030303",
            "name": "HUAQUIRCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "283",
            "province_id": "30",
            "ubigeo": "04",
            "code": "030304",
            "name": "JUAN ESPINOZA MEDRANO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "284",
            "province_id": "30",
            "ubigeo": "05",
            "code": "030305",
            "name": "OROPESA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "285",
            "province_id": "30",
            "ubigeo": "06",
            "code": "030306",
            "name": "PACHACONAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "286",
            "province_id": "30",
            "ubigeo": "07",
            "code": "030307",
            "name": "SABAINO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "287",
            "province_id": "31",
            "ubigeo": "01",
            "code": "030401",
            "name": "CHALHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "288",
            "province_id": "31",
            "ubigeo": "02",
            "code": "030402",
            "name": "CAPAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "289",
            "province_id": "31",
            "ubigeo": "03",
            "code": "030403",
            "name": "CARAYBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "290",
            "province_id": "31",
            "ubigeo": "04",
            "code": "030404",
            "name": "CHAPIMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "291",
            "province_id": "31",
            "ubigeo": "05",
            "code": "030405",
            "name": "COLCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "292",
            "province_id": "31",
            "ubigeo": "06",
            "code": "030406",
            "name": "COTARUSE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "293",
            "province_id": "31",
            "ubigeo": "07",
            "code": "030407",
            "name": "HUAYLLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "294",
            "province_id": "31",
            "ubigeo": "08",
            "code": "030408",
            "name": "JUSTO APU SAHUARAURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "295",
            "province_id": "31",
            "ubigeo": "09",
            "code": "030409",
            "name": "LUCRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "296",
            "province_id": "31",
            "ubigeo": "10",
            "code": "030410",
            "name": "POCOHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "297",
            "province_id": "31",
            "ubigeo": "11",
            "code": "030411",
            "name": "SAN JUAN DE CHACÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "298",
            "province_id": "31",
            "ubigeo": "12",
            "code": "030412",
            "name": "SAÑAYCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "299",
            "province_id": "31",
            "ubigeo": "13",
            "code": "030413",
            "name": "SORAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "300",
            "province_id": "31",
            "ubigeo": "14",
            "code": "030414",
            "name": "TAPAIRIHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "301",
            "province_id": "31",
            "ubigeo": "15",
            "code": "030415",
            "name": "TINTAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "302",
            "province_id": "31",
            "ubigeo": "16",
            "code": "030416",
            "name": "TORAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "303",
            "province_id": "31",
            "ubigeo": "17",
            "code": "030417",
            "name": "YANACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "304",
            "province_id": "32",
            "ubigeo": "01",
            "code": "030501",
            "name": "TAMBOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "305",
            "province_id": "32",
            "ubigeo": "02",
            "code": "030502",
            "name": "COTABAMBAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "306",
            "province_id": "32",
            "ubigeo": "03",
            "code": "030503",
            "name": "COYLLURQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "307",
            "province_id": "32",
            "ubigeo": "04",
            "code": "030504",
            "name": "HAQUIRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "308",
            "province_id": "32",
            "ubigeo": "05",
            "code": "030505",
            "name": "MARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "309",
            "province_id": "32",
            "ubigeo": "06",
            "code": "030506",
            "name": "CHALLHUAHUACHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "310",
            "province_id": "33",
            "ubigeo": "01",
            "code": "030601",
            "name": "CHINCHEROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "311",
            "province_id": "33",
            "ubigeo": "02",
            "code": "030602",
            "name": "ANCO_HUALLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "312",
            "province_id": "33",
            "ubigeo": "03",
            "code": "030603",
            "name": "COCHARCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "313",
            "province_id": "33",
            "ubigeo": "04",
            "code": "030604",
            "name": "HUACCANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "314",
            "province_id": "33",
            "ubigeo": "05",
            "code": "030605",
            "name": "OCOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "315",
            "province_id": "33",
            "ubigeo": "06",
            "code": "030606",
            "name": "ONGOY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "316",
            "province_id": "33",
            "ubigeo": "07",
            "code": "030607",
            "name": "URANMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "317",
            "province_id": "33",
            "ubigeo": "08",
            "code": "030608",
            "name": "RANRACANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "318",
            "province_id": "34",
            "ubigeo": "01",
            "code": "030701",
            "name": "CHUQUIBAMBILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "319",
            "province_id": "34",
            "ubigeo": "02",
            "code": "030702",
            "name": "CURPAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "320",
            "province_id": "34",
            "ubigeo": "03",
            "code": "030703",
            "name": "GAMARRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "321",
            "province_id": "34",
            "ubigeo": "04",
            "code": "030704",
            "name": "HUAYLLATI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "322",
            "province_id": "34",
            "ubigeo": "05",
            "code": "030705",
            "name": "MAMARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "323",
            "province_id": "34",
            "ubigeo": "06",
            "code": "030706",
            "name": "MICAELA BASTIDAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "324",
            "province_id": "34",
            "ubigeo": "07",
            "code": "030707",
            "name": "PATAYPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "325",
            "province_id": "34",
            "ubigeo": "08",
            "code": "030708",
            "name": "PROGRESO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "326",
            "province_id": "34",
            "ubigeo": "09",
            "code": "030709",
            "name": "SAN ANTONIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "327",
            "province_id": "34",
            "ubigeo": "10",
            "code": "030710",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "328",
            "province_id": "34",
            "ubigeo": "11",
            "code": "030711",
            "name": "TURPAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "329",
            "province_id": "34",
            "ubigeo": "12",
            "code": "030712",
            "name": "VILCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "330",
            "province_id": "34",
            "ubigeo": "13",
            "code": "030713",
            "name": "VIRUNDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "331",
            "province_id": "34",
            "ubigeo": "14",
            "code": "030714",
            "name": "CURASCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "332",
            "province_id": "35",
            "ubigeo": "01",
            "code": "040101",
            "name": "AREQUIPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "333",
            "province_id": "35",
            "ubigeo": "02",
            "code": "040102",
            "name": "ALTO SELVA ALEGRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "334",
            "province_id": "35",
            "ubigeo": "03",
            "code": "040103",
            "name": "CAYMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "335",
            "province_id": "35",
            "ubigeo": "04",
            "code": "040104",
            "name": "CERRO COLORADO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "336",
            "province_id": "35",
            "ubigeo": "05",
            "code": "040105",
            "name": "CHARACATO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "337",
            "province_id": "35",
            "ubigeo": "06",
            "code": "040106",
            "name": "CHIGUATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "338",
            "province_id": "35",
            "ubigeo": "07",
            "code": "040107",
            "name": "JACOBO HUNTER",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "339",
            "province_id": "35",
            "ubigeo": "08",
            "code": "040108",
            "name": "LA JOYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "340",
            "province_id": "35",
            "ubigeo": "09",
            "code": "040109",
            "name": "MARIANO MELGAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "341",
            "province_id": "35",
            "ubigeo": "10",
            "code": "040110",
            "name": "MIRAFLORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "342",
            "province_id": "35",
            "ubigeo": "11",
            "code": "040111",
            "name": "MOLLEBAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "343",
            "province_id": "35",
            "ubigeo": "12",
            "code": "040112",
            "name": "PAUCARPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "344",
            "province_id": "35",
            "ubigeo": "13",
            "code": "040113",
            "name": "POCSI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "345",
            "province_id": "35",
            "ubigeo": "14",
            "code": "040114",
            "name": "POLOBAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "346",
            "province_id": "35",
            "ubigeo": "15",
            "code": "040115",
            "name": "QUEQUEÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "347",
            "province_id": "35",
            "ubigeo": "16",
            "code": "040116",
            "name": "SABANDIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "348",
            "province_id": "35",
            "ubigeo": "17",
            "code": "040117",
            "name": "SACHACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "349",
            "province_id": "35",
            "ubigeo": "18",
            "code": "040118",
            "name": "SAN JUAN DE SIGUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "350",
            "province_id": "35",
            "ubigeo": "19",
            "code": "040119",
            "name": "SAN JUAN DE TARUCANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "351",
            "province_id": "35",
            "ubigeo": "20",
            "code": "040120",
            "name": "SANTA ISABEL DE SIGUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "352",
            "province_id": "35",
            "ubigeo": "21",
            "code": "040121",
            "name": "SANTA RITA DE SIGUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "353",
            "province_id": "35",
            "ubigeo": "22",
            "code": "040122",
            "name": "SOCABAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "354",
            "province_id": "35",
            "ubigeo": "23",
            "code": "040123",
            "name": "TIABAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "355",
            "province_id": "35",
            "ubigeo": "24",
            "code": "040124",
            "name": "UCHUMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "356",
            "province_id": "35",
            "ubigeo": "25",
            "code": "040125",
            "name": "VITOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "357",
            "province_id": "35",
            "ubigeo": "26",
            "code": "040126",
            "name": "YANAHUARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "358",
            "province_id": "35",
            "ubigeo": "27",
            "code": "040127",
            "name": "YARABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "359",
            "province_id": "35",
            "ubigeo": "28",
            "code": "040128",
            "name": "YURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "360",
            "province_id": "35",
            "ubigeo": "29",
            "code": "040129",
            "name": "JOSÉ LUIS BUSTAMANTE Y RIVERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "361",
            "province_id": "36",
            "ubigeo": "01",
            "code": "040201",
            "name": "CAMANÁ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "362",
            "province_id": "36",
            "ubigeo": "02",
            "code": "040202",
            "name": "JOSÉ MARÍA QUIMPER",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "363",
            "province_id": "36",
            "ubigeo": "03",
            "code": "040203",
            "name": "MARIANO NICOLÁS VALCÁRCEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "364",
            "province_id": "36",
            "ubigeo": "04",
            "code": "040204",
            "name": "MARISCAL CÁCERES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "365",
            "province_id": "36",
            "ubigeo": "05",
            "code": "040205",
            "name": "NICOLÁS DE PIEROLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "366",
            "province_id": "36",
            "ubigeo": "06",
            "code": "040206",
            "name": "OCOÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "367",
            "province_id": "36",
            "ubigeo": "07",
            "code": "040207",
            "name": "QUILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "368",
            "province_id": "36",
            "ubigeo": "08",
            "code": "040208",
            "name": "SAMUEL PASTOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "369",
            "province_id": "37",
            "ubigeo": "01",
            "code": "040301",
            "name": "CARAVELÍ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "370",
            "province_id": "37",
            "ubigeo": "02",
            "code": "040302",
            "name": "ACARÍ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "371",
            "province_id": "37",
            "ubigeo": "03",
            "code": "040303",
            "name": "ATICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "372",
            "province_id": "37",
            "ubigeo": "04",
            "code": "040304",
            "name": "ATIQUIPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "373",
            "province_id": "37",
            "ubigeo": "05",
            "code": "040305",
            "name": "BELLA UNIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "374",
            "province_id": "37",
            "ubigeo": "06",
            "code": "040306",
            "name": "CAHUACHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "375",
            "province_id": "37",
            "ubigeo": "07",
            "code": "040307",
            "name": "CHALA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "376",
            "province_id": "37",
            "ubigeo": "08",
            "code": "040308",
            "name": "CHAPARRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "377",
            "province_id": "37",
            "ubigeo": "09",
            "code": "040309",
            "name": "HUANUHUANU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "378",
            "province_id": "37",
            "ubigeo": "10",
            "code": "040310",
            "name": "JAQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "379",
            "province_id": "37",
            "ubigeo": "11",
            "code": "040311",
            "name": "LOMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "380",
            "province_id": "37",
            "ubigeo": "12",
            "code": "040312",
            "name": "QUICACHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "381",
            "province_id": "37",
            "ubigeo": "13",
            "code": "040313",
            "name": "YAUCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "382",
            "province_id": "38",
            "ubigeo": "01",
            "code": "040401",
            "name": "APLAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "383",
            "province_id": "38",
            "ubigeo": "02",
            "code": "040402",
            "name": "ANDAGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "384",
            "province_id": "38",
            "ubigeo": "03",
            "code": "040403",
            "name": "AYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "385",
            "province_id": "38",
            "ubigeo": "04",
            "code": "040404",
            "name": "CHACHAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "386",
            "province_id": "38",
            "ubigeo": "05",
            "code": "040405",
            "name": "CHILCAYMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "387",
            "province_id": "38",
            "ubigeo": "06",
            "code": "040406",
            "name": "CHOCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "388",
            "province_id": "38",
            "ubigeo": "07",
            "code": "040407",
            "name": "HUANCARQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "389",
            "province_id": "38",
            "ubigeo": "08",
            "code": "040408",
            "name": "MACHAGUAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "390",
            "province_id": "38",
            "ubigeo": "09",
            "code": "040409",
            "name": "ORCOPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "391",
            "province_id": "38",
            "ubigeo": "10",
            "code": "040410",
            "name": "PAMPACOLCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "392",
            "province_id": "38",
            "ubigeo": "11",
            "code": "040411",
            "name": "TIPAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "393",
            "province_id": "38",
            "ubigeo": "12",
            "code": "040412",
            "name": "UÑON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "394",
            "province_id": "38",
            "ubigeo": "13",
            "code": "040413",
            "name": "URACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "395",
            "province_id": "38",
            "ubigeo": "14",
            "code": "040414",
            "name": "VIRACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "396",
            "province_id": "39",
            "ubigeo": "01",
            "code": "040501",
            "name": "CHIVAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "397",
            "province_id": "39",
            "ubigeo": "02",
            "code": "040502",
            "name": "ACHOMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "398",
            "province_id": "39",
            "ubigeo": "03",
            "code": "040503",
            "name": "CABANACONDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "399",
            "province_id": "39",
            "ubigeo": "04",
            "code": "040504",
            "name": "CALLALLI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "400",
            "province_id": "39",
            "ubigeo": "05",
            "code": "040505",
            "name": "CAYLLOMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "401",
            "province_id": "39",
            "ubigeo": "06",
            "code": "040506",
            "name": "COPORAQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "402",
            "province_id": "39",
            "ubigeo": "07",
            "code": "040507",
            "name": "HUAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "403",
            "province_id": "39",
            "ubigeo": "08",
            "code": "040508",
            "name": "HUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "404",
            "province_id": "39",
            "ubigeo": "09",
            "code": "040509",
            "name": "ICHUPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "405",
            "province_id": "39",
            "ubigeo": "10",
            "code": "040510",
            "name": "LARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "406",
            "province_id": "39",
            "ubigeo": "11",
            "code": "040511",
            "name": "LLUTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "407",
            "province_id": "39",
            "ubigeo": "12",
            "code": "040512",
            "name": "MACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "408",
            "province_id": "39",
            "ubigeo": "13",
            "code": "040513",
            "name": "MADRIGAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "409",
            "province_id": "39",
            "ubigeo": "14",
            "code": "040514",
            "name": "SAN ANTONIO DE CHUCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "410",
            "province_id": "39",
            "ubigeo": "15",
            "code": "040515",
            "name": "SIBAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "411",
            "province_id": "39",
            "ubigeo": "16",
            "code": "040516",
            "name": "TAPAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "412",
            "province_id": "39",
            "ubigeo": "17",
            "code": "040517",
            "name": "TISCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "413",
            "province_id": "39",
            "ubigeo": "18",
            "code": "040518",
            "name": "TUTI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "414",
            "province_id": "39",
            "ubigeo": "19",
            "code": "040519",
            "name": "YANQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "415",
            "province_id": "39",
            "ubigeo": "20",
            "code": "040520",
            "name": "MAJES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "416",
            "province_id": "40",
            "ubigeo": "01",
            "code": "040601",
            "name": "CHUQUIBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "417",
            "province_id": "40",
            "ubigeo": "02",
            "code": "040602",
            "name": "ANDARAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "418",
            "province_id": "40",
            "ubigeo": "03",
            "code": "040603",
            "name": "CAYARANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "419",
            "province_id": "40",
            "ubigeo": "04",
            "code": "040604",
            "name": "CHICHAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "420",
            "province_id": "40",
            "ubigeo": "05",
            "code": "040605",
            "name": "IRAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "421",
            "province_id": "40",
            "ubigeo": "06",
            "code": "040606",
            "name": "RÍO GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "422",
            "province_id": "40",
            "ubigeo": "07",
            "code": "040607",
            "name": "SALAMANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "423",
            "province_id": "40",
            "ubigeo": "08",
            "code": "040608",
            "name": "YANAQUIHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "424",
            "province_id": "41",
            "ubigeo": "01",
            "code": "040701",
            "name": "MOLLENDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "425",
            "province_id": "41",
            "ubigeo": "02",
            "code": "040702",
            "name": "COCACHACRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "426",
            "province_id": "41",
            "ubigeo": "03",
            "code": "040703",
            "name": "DEAN VALDIVIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "427",
            "province_id": "41",
            "ubigeo": "04",
            "code": "040704",
            "name": "ISLAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "428",
            "province_id": "41",
            "ubigeo": "05",
            "code": "040705",
            "name": "MEJIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "429",
            "province_id": "41",
            "ubigeo": "06",
            "code": "040706",
            "name": "PUNTA DE BOMBÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "430",
            "province_id": "42",
            "ubigeo": "01",
            "code": "040801",
            "name": "COTAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "431",
            "province_id": "42",
            "ubigeo": "02",
            "code": "040802",
            "name": "ALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "432",
            "province_id": "42",
            "ubigeo": "03",
            "code": "040803",
            "name": "CHARCANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "433",
            "province_id": "42",
            "ubigeo": "04",
            "code": "040804",
            "name": "HUAYNACOTAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "434",
            "province_id": "42",
            "ubigeo": "05",
            "code": "040805",
            "name": "PAMPAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "435",
            "province_id": "42",
            "ubigeo": "06",
            "code": "040806",
            "name": "PUYCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "436",
            "province_id": "42",
            "ubigeo": "07",
            "code": "040807",
            "name": "QUECHUALLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "437",
            "province_id": "42",
            "ubigeo": "08",
            "code": "040808",
            "name": "SAYLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "438",
            "province_id": "42",
            "ubigeo": "09",
            "code": "040809",
            "name": "TAURIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "439",
            "province_id": "42",
            "ubigeo": "10",
            "code": "040810",
            "name": "TOMEPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "440",
            "province_id": "42",
            "ubigeo": "11",
            "code": "040811",
            "name": "TORO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "441",
            "province_id": "43",
            "ubigeo": "01",
            "code": "050101",
            "name": "AYACUCHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "442",
            "province_id": "43",
            "ubigeo": "02",
            "code": "050102",
            "name": "ACOCRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "443",
            "province_id": "43",
            "ubigeo": "03",
            "code": "050103",
            "name": "ACOS VINCHOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "444",
            "province_id": "43",
            "ubigeo": "04",
            "code": "050104",
            "name": "CARMEN ALTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "445",
            "province_id": "43",
            "ubigeo": "05",
            "code": "050105",
            "name": "CHIARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "446",
            "province_id": "43",
            "ubigeo": "06",
            "code": "050106",
            "name": "OCROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "447",
            "province_id": "43",
            "ubigeo": "07",
            "code": "050107",
            "name": "PACAYCASA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "448",
            "province_id": "43",
            "ubigeo": "08",
            "code": "050108",
            "name": "QUINUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "449",
            "province_id": "43",
            "ubigeo": "09",
            "code": "050109",
            "name": "SAN JOSÉ DE TICLLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "450",
            "province_id": "43",
            "ubigeo": "10",
            "code": "050110",
            "name": "SAN JUAN BAUTISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "451",
            "province_id": "43",
            "ubigeo": "11",
            "code": "050111",
            "name": "SANTIAGO DE PISCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "452",
            "province_id": "43",
            "ubigeo": "12",
            "code": "050112",
            "name": "SOCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "453",
            "province_id": "43",
            "ubigeo": "13",
            "code": "050113",
            "name": "TAMBILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "454",
            "province_id": "43",
            "ubigeo": "14",
            "code": "050114",
            "name": "VINCHOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "455",
            "province_id": "43",
            "ubigeo": "15",
            "code": "050115",
            "name": "JESÚS NAZARENO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "456",
            "province_id": "43",
            "ubigeo": "16",
            "code": "050116",
            "name": "ANDRÉS AVELINO CÁCERES DORREGARAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "457",
            "province_id": "44",
            "ubigeo": "01",
            "code": "050201",
            "name": "CANGALLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "458",
            "province_id": "44",
            "ubigeo": "02",
            "code": "050202",
            "name": "CHUSCHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "459",
            "province_id": "44",
            "ubigeo": "03",
            "code": "050203",
            "name": "LOS MOROCHUCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "460",
            "province_id": "44",
            "ubigeo": "04",
            "code": "050204",
            "name": "MARÍA PARADO DE BELLIDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "461",
            "province_id": "44",
            "ubigeo": "05",
            "code": "050205",
            "name": "PARAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "462",
            "province_id": "44",
            "ubigeo": "06",
            "code": "050206",
            "name": "TOTOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "463",
            "province_id": "45",
            "ubigeo": "01",
            "code": "050301",
            "name": "SANCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "464",
            "province_id": "45",
            "ubigeo": "02",
            "code": "050302",
            "name": "CARAPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "465",
            "province_id": "45",
            "ubigeo": "03",
            "code": "050303",
            "name": "SACSAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "466",
            "province_id": "45",
            "ubigeo": "04",
            "code": "050304",
            "name": "SANTIAGO DE LUCANAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "467",
            "province_id": "46",
            "ubigeo": "01",
            "code": "050401",
            "name": "HUANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "468",
            "province_id": "46",
            "ubigeo": "02",
            "code": "050402",
            "name": "AYAHUANCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "469",
            "province_id": "46",
            "ubigeo": "03",
            "code": "050403",
            "name": "HUAMANGUILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "470",
            "province_id": "46",
            "ubigeo": "04",
            "code": "050404",
            "name": "IGUAIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "471",
            "province_id": "46",
            "ubigeo": "05",
            "code": "050405",
            "name": "LURICOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "472",
            "province_id": "46",
            "ubigeo": "06",
            "code": "050406",
            "name": "SANTILLANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "473",
            "province_id": "46",
            "ubigeo": "07",
            "code": "050407",
            "name": "SIVIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "474",
            "province_id": "46",
            "ubigeo": "08",
            "code": "050408",
            "name": "LLOCHEGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "475",
            "province_id": "46",
            "ubigeo": "09",
            "code": "050409",
            "name": "CANAYRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "476",
            "province_id": "46",
            "ubigeo": "10",
            "code": "050410",
            "name": "UCHURACCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "477",
            "province_id": "46",
            "ubigeo": "11",
            "code": "050411",
            "name": "PUCACOLPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "478",
            "province_id": "47",
            "ubigeo": "01",
            "code": "050501",
            "name": "SAN MIGUEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "479",
            "province_id": "47",
            "ubigeo": "02",
            "code": "050502",
            "name": "ANCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "480",
            "province_id": "47",
            "ubigeo": "03",
            "code": "050503",
            "name": "AYNA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "481",
            "province_id": "47",
            "ubigeo": "04",
            "code": "050504",
            "name": "CHILCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "482",
            "province_id": "47",
            "ubigeo": "05",
            "code": "050505",
            "name": "CHUNGUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "483",
            "province_id": "47",
            "ubigeo": "06",
            "code": "050506",
            "name": "LUIS CARRANZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "484",
            "province_id": "47",
            "ubigeo": "07",
            "code": "050507",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "485",
            "province_id": "47",
            "ubigeo": "08",
            "code": "050508",
            "name": "TAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "486",
            "province_id": "47",
            "ubigeo": "09",
            "code": "050509",
            "name": "SAMUGARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "487",
            "province_id": "47",
            "ubigeo": "10",
            "code": "050510",
            "name": "ANCHIHUAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "488",
            "province_id": "48",
            "ubigeo": "01",
            "code": "050601",
            "name": "PUQUIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "489",
            "province_id": "48",
            "ubigeo": "02",
            "code": "050602",
            "name": "AUCARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "490",
            "province_id": "48",
            "ubigeo": "03",
            "code": "050603",
            "name": "CABANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "491",
            "province_id": "48",
            "ubigeo": "04",
            "code": "050604",
            "name": "CARMEN SALCEDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "492",
            "province_id": "48",
            "ubigeo": "05",
            "code": "050605",
            "name": "CHAVIÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "493",
            "province_id": "48",
            "ubigeo": "06",
            "code": "050606",
            "name": "CHIPAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "494",
            "province_id": "48",
            "ubigeo": "07",
            "code": "050607",
            "name": "HUAC-HUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "495",
            "province_id": "48",
            "ubigeo": "08",
            "code": "050608",
            "name": "LARAMATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "496",
            "province_id": "48",
            "ubigeo": "09",
            "code": "050609",
            "name": "LEONCIO PRADO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "497",
            "province_id": "48",
            "ubigeo": "10",
            "code": "050610",
            "name": "LLAUTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "498",
            "province_id": "48",
            "ubigeo": "11",
            "code": "050611",
            "name": "LUCANAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "499",
            "province_id": "48",
            "ubigeo": "12",
            "code": "050612",
            "name": "OCAÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "500",
            "province_id": "48",
            "ubigeo": "13",
            "code": "050613",
            "name": "OTOCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "501",
            "province_id": "48",
            "ubigeo": "14",
            "code": "050614",
            "name": "SAISA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "502",
            "province_id": "48",
            "ubigeo": "15",
            "code": "050615",
            "name": "SAN CRISTÓBAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "503",
            "province_id": "48",
            "ubigeo": "16",
            "code": "050616",
            "name": "SAN JUAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "504",
            "province_id": "48",
            "ubigeo": "17",
            "code": "050617",
            "name": "SAN PEDRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "505",
            "province_id": "48",
            "ubigeo": "18",
            "code": "050618",
            "name": "SAN PEDRO DE PALCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "506",
            "province_id": "48",
            "ubigeo": "19",
            "code": "050619",
            "name": "SANCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "507",
            "province_id": "48",
            "ubigeo": "20",
            "code": "050620",
            "name": "SANTA ANA DE HUAYCAHUACHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "508",
            "province_id": "48",
            "ubigeo": "21",
            "code": "050621",
            "name": "SANTA LUCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "509",
            "province_id": "49",
            "ubigeo": "01",
            "code": "050701",
            "name": "CORACORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "510",
            "province_id": "49",
            "ubigeo": "02",
            "code": "050702",
            "name": "CHUMPI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "511",
            "province_id": "49",
            "ubigeo": "03",
            "code": "050703",
            "name": "CORONEL CASTAÑEDA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "512",
            "province_id": "49",
            "ubigeo": "04",
            "code": "050704",
            "name": "PACAPAUSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "513",
            "province_id": "49",
            "ubigeo": "05",
            "code": "050705",
            "name": "PULLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "514",
            "province_id": "49",
            "ubigeo": "06",
            "code": "050706",
            "name": "PUYUSCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "515",
            "province_id": "49",
            "ubigeo": "07",
            "code": "050707",
            "name": "SAN FRANCISCO DE RAVACAYCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "516",
            "province_id": "49",
            "ubigeo": "08",
            "code": "050708",
            "name": "UPAHUACHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "517",
            "province_id": "50",
            "ubigeo": "01",
            "code": "050801",
            "name": "PAUSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "518",
            "province_id": "50",
            "ubigeo": "02",
            "code": "050802",
            "name": "COLTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "519",
            "province_id": "50",
            "ubigeo": "03",
            "code": "050803",
            "name": "CORCULLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "520",
            "province_id": "50",
            "ubigeo": "04",
            "code": "050804",
            "name": "LAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "521",
            "province_id": "50",
            "ubigeo": "05",
            "code": "050805",
            "name": "MARCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "522",
            "province_id": "50",
            "ubigeo": "06",
            "code": "050806",
            "name": "OYOLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "523",
            "province_id": "50",
            "ubigeo": "07",
            "code": "050807",
            "name": "PARARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "524",
            "province_id": "50",
            "ubigeo": "08",
            "code": "050808",
            "name": "SAN JAVIER DE ALPABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "525",
            "province_id": "50",
            "ubigeo": "09",
            "code": "050809",
            "name": "SAN JOSÉ DE USHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "526",
            "province_id": "50",
            "ubigeo": "10",
            "code": "050810",
            "name": "SARA SARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "527",
            "province_id": "51",
            "ubigeo": "01",
            "code": "050901",
            "name": "QUEROBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "528",
            "province_id": "51",
            "ubigeo": "02",
            "code": "050902",
            "name": "BELÉN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "529",
            "province_id": "51",
            "ubigeo": "03",
            "code": "050903",
            "name": "CHALCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "530",
            "province_id": "51",
            "ubigeo": "04",
            "code": "050904",
            "name": "CHILCAYOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "531",
            "province_id": "51",
            "ubigeo": "05",
            "code": "050905",
            "name": "HUACAÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "532",
            "province_id": "51",
            "ubigeo": "06",
            "code": "050906",
            "name": "MORCOLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "533",
            "province_id": "51",
            "ubigeo": "07",
            "code": "050907",
            "name": "PAICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "534",
            "province_id": "51",
            "ubigeo": "08",
            "code": "050908",
            "name": "SAN PEDRO DE LARCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "535",
            "province_id": "51",
            "ubigeo": "09",
            "code": "050909",
            "name": "SAN SALVADOR DE QUIJE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "536",
            "province_id": "51",
            "ubigeo": "10",
            "code": "050910",
            "name": "SANTIAGO DE PAUCARAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "537",
            "province_id": "51",
            "ubigeo": "11",
            "code": "050911",
            "name": "SORAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "538",
            "province_id": "52",
            "ubigeo": "01",
            "code": "051001",
            "name": "HUANCAPI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "539",
            "province_id": "52",
            "ubigeo": "02",
            "code": "051002",
            "name": "ALCAMENCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "540",
            "province_id": "52",
            "ubigeo": "03",
            "code": "051003",
            "name": "APONGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "541",
            "province_id": "52",
            "ubigeo": "04",
            "code": "051004",
            "name": "ASQUIPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "542",
            "province_id": "52",
            "ubigeo": "05",
            "code": "051005",
            "name": "CANARIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "543",
            "province_id": "52",
            "ubigeo": "06",
            "code": "051006",
            "name": "CAYARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "544",
            "province_id": "52",
            "ubigeo": "07",
            "code": "051007",
            "name": "COLCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "545",
            "province_id": "52",
            "ubigeo": "08",
            "code": "051008",
            "name": "HUAMANQUIQUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "546",
            "province_id": "52",
            "ubigeo": "09",
            "code": "051009",
            "name": "HUANCARAYLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "547",
            "province_id": "52",
            "ubigeo": "10",
            "code": "051010",
            "name": "HUAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "548",
            "province_id": "52",
            "ubigeo": "11",
            "code": "051011",
            "name": "SARHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "549",
            "province_id": "52",
            "ubigeo": "12",
            "code": "051012",
            "name": "VILCANCHOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "550",
            "province_id": "53",
            "ubigeo": "01",
            "code": "051101",
            "name": "VILCAS HUAMAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "551",
            "province_id": "53",
            "ubigeo": "02",
            "code": "051102",
            "name": "ACCOMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "552",
            "province_id": "53",
            "ubigeo": "03",
            "code": "051103",
            "name": "CARHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "553",
            "province_id": "53",
            "ubigeo": "04",
            "code": "051104",
            "name": "CONCEPCIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "554",
            "province_id": "53",
            "ubigeo": "05",
            "code": "051105",
            "name": "HUAMBALPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "555",
            "province_id": "53",
            "ubigeo": "06",
            "code": "051106",
            "name": "INDEPENDENCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "556",
            "province_id": "53",
            "ubigeo": "07",
            "code": "051107",
            "name": "SAURAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "557",
            "province_id": "53",
            "ubigeo": "08",
            "code": "051108",
            "name": "VISCHONGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "558",
            "province_id": "54",
            "ubigeo": "01",
            "code": "060101",
            "name": "CAJAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "559",
            "province_id": "54",
            "ubigeo": "02",
            "code": "060102",
            "name": "ASUNCIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "560",
            "province_id": "54",
            "ubigeo": "03",
            "code": "060103",
            "name": "CHETILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "561",
            "province_id": "54",
            "ubigeo": "04",
            "code": "060104",
            "name": "COSPAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "562",
            "province_id": "54",
            "ubigeo": "05",
            "code": "060105",
            "name": "ENCAÑADA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "563",
            "province_id": "54",
            "ubigeo": "06",
            "code": "060106",
            "name": "JESÚS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "564",
            "province_id": "54",
            "ubigeo": "07",
            "code": "060107",
            "name": "LLACANORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "565",
            "province_id": "54",
            "ubigeo": "08",
            "code": "060108",
            "name": "LOS BAÑOS DEL INCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "566",
            "province_id": "54",
            "ubigeo": "09",
            "code": "060109",
            "name": "MAGDALENA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "567",
            "province_id": "54",
            "ubigeo": "10",
            "code": "060110",
            "name": "MATARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "568",
            "province_id": "54",
            "ubigeo": "11",
            "code": "060111",
            "name": "NAMORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "569",
            "province_id": "54",
            "ubigeo": "12",
            "code": "060112",
            "name": "SAN JUAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "570",
            "province_id": "55",
            "ubigeo": "01",
            "code": "060201",
            "name": "CAJABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "571",
            "province_id": "55",
            "ubigeo": "02",
            "code": "060202",
            "name": "CACHACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "572",
            "province_id": "55",
            "ubigeo": "03",
            "code": "060203",
            "name": "CONDEBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "573",
            "province_id": "55",
            "ubigeo": "04",
            "code": "060204",
            "name": "SITACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "574",
            "province_id": "56",
            "ubigeo": "01",
            "code": "060301",
            "name": "CELENDÍN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "575",
            "province_id": "56",
            "ubigeo": "02",
            "code": "060302",
            "name": "CHUMUCH",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "576",
            "province_id": "56",
            "ubigeo": "03",
            "code": "060303",
            "name": "CORTEGANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "577",
            "province_id": "56",
            "ubigeo": "04",
            "code": "060304",
            "name": "HUASMIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "578",
            "province_id": "56",
            "ubigeo": "05",
            "code": "060305",
            "name": "JORGE CHÁVEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "579",
            "province_id": "56",
            "ubigeo": "06",
            "code": "060306",
            "name": "JOSÉ GÁLVEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "580",
            "province_id": "56",
            "ubigeo": "07",
            "code": "060307",
            "name": "MIGUEL IGLESIAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "581",
            "province_id": "56",
            "ubigeo": "08",
            "code": "060308",
            "name": "OXAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "582",
            "province_id": "56",
            "ubigeo": "09",
            "code": "060309",
            "name": "SOROCHUCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "583",
            "province_id": "56",
            "ubigeo": "10",
            "code": "060310",
            "name": "SUCRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "584",
            "province_id": "56",
            "ubigeo": "11",
            "code": "060311",
            "name": "UTCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "585",
            "province_id": "56",
            "ubigeo": "12",
            "code": "060312",
            "name": "LA LIBERTAD DE PALLAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "586",
            "province_id": "57",
            "ubigeo": "01",
            "code": "060401",
            "name": "CHOTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "587",
            "province_id": "57",
            "ubigeo": "02",
            "code": "060402",
            "name": "ANGUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "588",
            "province_id": "57",
            "ubigeo": "03",
            "code": "060403",
            "name": "CHADIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "589",
            "province_id": "57",
            "ubigeo": "04",
            "code": "060404",
            "name": "CHIGUIRIP",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "590",
            "province_id": "57",
            "ubigeo": "05",
            "code": "060405",
            "name": "CHIMBAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "591",
            "province_id": "57",
            "ubigeo": "06",
            "code": "060406",
            "name": "CHOROPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "592",
            "province_id": "57",
            "ubigeo": "07",
            "code": "060407",
            "name": "COCHABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "593",
            "province_id": "57",
            "ubigeo": "08",
            "code": "060408",
            "name": "CONCHAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "594",
            "province_id": "57",
            "ubigeo": "09",
            "code": "060409",
            "name": "HUAMBOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "595",
            "province_id": "57",
            "ubigeo": "10",
            "code": "060410",
            "name": "LAJAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "596",
            "province_id": "57",
            "ubigeo": "11",
            "code": "060411",
            "name": "LLAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "597",
            "province_id": "57",
            "ubigeo": "12",
            "code": "060412",
            "name": "MIRACOSTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "598",
            "province_id": "57",
            "ubigeo": "13",
            "code": "060413",
            "name": "PACCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "599",
            "province_id": "57",
            "ubigeo": "14",
            "code": "060414",
            "name": "PION",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "600",
            "province_id": "57",
            "ubigeo": "15",
            "code": "060415",
            "name": "QUEROCOTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "601",
            "province_id": "57",
            "ubigeo": "16",
            "code": "060416",
            "name": "SAN JUAN DE LICUPIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "602",
            "province_id": "57",
            "ubigeo": "17",
            "code": "060417",
            "name": "TACABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "603",
            "province_id": "57",
            "ubigeo": "18",
            "code": "060418",
            "name": "TOCMOCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "604",
            "province_id": "57",
            "ubigeo": "19",
            "code": "060419",
            "name": "CHALAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "605",
            "province_id": "58",
            "ubigeo": "01",
            "code": "060501",
            "name": "CONTUMAZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "606",
            "province_id": "58",
            "ubigeo": "02",
            "code": "060502",
            "name": "CHILETE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "607",
            "province_id": "58",
            "ubigeo": "03",
            "code": "060503",
            "name": "CUPISNIQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "608",
            "province_id": "58",
            "ubigeo": "04",
            "code": "060504",
            "name": "GUZMANGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "609",
            "province_id": "58",
            "ubigeo": "05",
            "code": "060505",
            "name": "SAN BENITO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "610",
            "province_id": "58",
            "ubigeo": "06",
            "code": "060506",
            "name": "SANTA CRUZ DE TOLEDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "611",
            "province_id": "58",
            "ubigeo": "07",
            "code": "060507",
            "name": "TANTARICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "612",
            "province_id": "58",
            "ubigeo": "08",
            "code": "060508",
            "name": "YONAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "613",
            "province_id": "59",
            "ubigeo": "01",
            "code": "060601",
            "name": "CUTERVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "614",
            "province_id": "59",
            "ubigeo": "02",
            "code": "060602",
            "name": "CALLAYUC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "615",
            "province_id": "59",
            "ubigeo": "03",
            "code": "060603",
            "name": "CHOROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "616",
            "province_id": "59",
            "ubigeo": "04",
            "code": "060604",
            "name": "CUJILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "617",
            "province_id": "59",
            "ubigeo": "05",
            "code": "060605",
            "name": "LA RAMADA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "618",
            "province_id": "59",
            "ubigeo": "06",
            "code": "060606",
            "name": "PIMPINGOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "619",
            "province_id": "59",
            "ubigeo": "07",
            "code": "060607",
            "name": "QUEROCOTILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "620",
            "province_id": "59",
            "ubigeo": "08",
            "code": "060608",
            "name": "SAN ANDRÉS DE CUTERVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "621",
            "province_id": "59",
            "ubigeo": "09",
            "code": "060609",
            "name": "SAN JUAN DE CUTERVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "622",
            "province_id": "59",
            "ubigeo": "10",
            "code": "060610",
            "name": "SAN LUIS DE LUCMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "623",
            "province_id": "59",
            "ubigeo": "11",
            "code": "060611",
            "name": "SANTA CRUZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "624",
            "province_id": "59",
            "ubigeo": "12",
            "code": "060612",
            "name": "SANTO DOMINGO DE LA CAPILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "625",
            "province_id": "59",
            "ubigeo": "13",
            "code": "060613",
            "name": "SANTO TOMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "626",
            "province_id": "59",
            "ubigeo": "14",
            "code": "060614",
            "name": "SOCOTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "627",
            "province_id": "59",
            "ubigeo": "15",
            "code": "060615",
            "name": "TORIBIO CASANOVA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "628",
            "province_id": "60",
            "ubigeo": "01",
            "code": "060701",
            "name": "BAMBAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "629",
            "province_id": "60",
            "ubigeo": "02",
            "code": "060702",
            "name": "CHUGUR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "630",
            "province_id": "60",
            "ubigeo": "03",
            "code": "060703",
            "name": "HUALGAYOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "631",
            "province_id": "61",
            "ubigeo": "01",
            "code": "060801",
            "name": "JAÉN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "632",
            "province_id": "61",
            "ubigeo": "02",
            "code": "060802",
            "name": "BELLAVISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "633",
            "province_id": "61",
            "ubigeo": "03",
            "code": "060803",
            "name": "CHONTALI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "634",
            "province_id": "61",
            "ubigeo": "04",
            "code": "060804",
            "name": "COLASAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "635",
            "province_id": "61",
            "ubigeo": "05",
            "code": "060805",
            "name": "HUABAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "636",
            "province_id": "61",
            "ubigeo": "06",
            "code": "060806",
            "name": "LAS PIRIAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "637",
            "province_id": "61",
            "ubigeo": "07",
            "code": "060807",
            "name": "POMAHUACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "638",
            "province_id": "61",
            "ubigeo": "08",
            "code": "060808",
            "name": "PUCARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "639",
            "province_id": "61",
            "ubigeo": "09",
            "code": "060809",
            "name": "SALLIQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "640",
            "province_id": "61",
            "ubigeo": "10",
            "code": "060810",
            "name": "SAN FELIPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "641",
            "province_id": "61",
            "ubigeo": "11",
            "code": "060811",
            "name": "SAN JOSÉ DEL ALTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "642",
            "province_id": "61",
            "ubigeo": "12",
            "code": "060812",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "643",
            "province_id": "62",
            "ubigeo": "01",
            "code": "060901",
            "name": "SAN IGNACIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "644",
            "province_id": "62",
            "ubigeo": "02",
            "code": "060902",
            "name": "CHIRINOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "645",
            "province_id": "62",
            "ubigeo": "03",
            "code": "060903",
            "name": "HUARANGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "646",
            "province_id": "62",
            "ubigeo": "04",
            "code": "060904",
            "name": "LA COIPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "647",
            "province_id": "62",
            "ubigeo": "05",
            "code": "060905",
            "name": "NAMBALLE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "648",
            "province_id": "62",
            "ubigeo": "06",
            "code": "060906",
            "name": "SAN JOSÉ DE LOURDES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "649",
            "province_id": "62",
            "ubigeo": "07",
            "code": "060907",
            "name": "TABACONAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "650",
            "province_id": "63",
            "ubigeo": "01",
            "code": "061001",
            "name": "PEDRO GÁLVEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "651",
            "province_id": "63",
            "ubigeo": "02",
            "code": "061002",
            "name": "CHANCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "652",
            "province_id": "63",
            "ubigeo": "03",
            "code": "061003",
            "name": "EDUARDO VILLANUEVA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "653",
            "province_id": "63",
            "ubigeo": "04",
            "code": "061004",
            "name": "GREGORIO PITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "654",
            "province_id": "63",
            "ubigeo": "05",
            "code": "061005",
            "name": "ICHOCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "655",
            "province_id": "63",
            "ubigeo": "06",
            "code": "061006",
            "name": "JOSÉ MANUEL QUIROZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "656",
            "province_id": "63",
            "ubigeo": "07",
            "code": "061007",
            "name": "JOSÉ SABOGAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "657",
            "province_id": "64",
            "ubigeo": "01",
            "code": "061101",
            "name": "SAN MIGUEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "658",
            "province_id": "64",
            "ubigeo": "02",
            "code": "061102",
            "name": "BOLÍVAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "659",
            "province_id": "64",
            "ubigeo": "03",
            "code": "061103",
            "name": "CALQUIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "660",
            "province_id": "64",
            "ubigeo": "04",
            "code": "061104",
            "name": "CATILLUC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "661",
            "province_id": "64",
            "ubigeo": "05",
            "code": "061105",
            "name": "EL PRADO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "662",
            "province_id": "64",
            "ubigeo": "06",
            "code": "061106",
            "name": "LA FLORIDA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "663",
            "province_id": "64",
            "ubigeo": "07",
            "code": "061107",
            "name": "LLAPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "664",
            "province_id": "64",
            "ubigeo": "08",
            "code": "061108",
            "name": "NANCHOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "665",
            "province_id": "64",
            "ubigeo": "09",
            "code": "061109",
            "name": "NIEPOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "666",
            "province_id": "64",
            "ubigeo": "10",
            "code": "061110",
            "name": "SAN GREGORIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "667",
            "province_id": "64",
            "ubigeo": "11",
            "code": "061111",
            "name": "SAN SILVESTRE DE COCHAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "668",
            "province_id": "64",
            "ubigeo": "12",
            "code": "061112",
            "name": "TONGOD",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "669",
            "province_id": "64",
            "ubigeo": "13",
            "code": "061113",
            "name": "UNIÓN AGUA BLANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "670",
            "province_id": "65",
            "ubigeo": "01",
            "code": "061201",
            "name": "SAN PABLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "671",
            "province_id": "65",
            "ubigeo": "02",
            "code": "061202",
            "name": "SAN BERNARDINO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "672",
            "province_id": "65",
            "ubigeo": "03",
            "code": "061203",
            "name": "SAN LUIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "673",
            "province_id": "65",
            "ubigeo": "04",
            "code": "061204",
            "name": "TUMBADEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "674",
            "province_id": "66",
            "ubigeo": "01",
            "code": "061301",
            "name": "SANTA CRUZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "675",
            "province_id": "66",
            "ubigeo": "02",
            "code": "061302",
            "name": "ANDABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "676",
            "province_id": "66",
            "ubigeo": "03",
            "code": "061303",
            "name": "CATACHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "677",
            "province_id": "66",
            "ubigeo": "04",
            "code": "061304",
            "name": "CHANCAYBAÑOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "678",
            "province_id": "66",
            "ubigeo": "05",
            "code": "061305",
            "name": "LA ESPERANZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "679",
            "province_id": "66",
            "ubigeo": "06",
            "code": "061306",
            "name": "NINABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "680",
            "province_id": "66",
            "ubigeo": "07",
            "code": "061307",
            "name": "PULAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "681",
            "province_id": "66",
            "ubigeo": "08",
            "code": "061308",
            "name": "SAUCEPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "682",
            "province_id": "66",
            "ubigeo": "09",
            "code": "061309",
            "name": "SEXI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "683",
            "province_id": "66",
            "ubigeo": "10",
            "code": "061310",
            "name": "UTICYACU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "684",
            "province_id": "66",
            "ubigeo": "11",
            "code": "061311",
            "name": "YAUYUCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "685",
            "province_id": "67",
            "ubigeo": "01",
            "code": "070101",
            "name": "CALLAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "686",
            "province_id": "67",
            "ubigeo": "02",
            "code": "070102",
            "name": "BELLAVISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "687",
            "province_id": "67",
            "ubigeo": "03",
            "code": "070103",
            "name": "CARMEN DE LA LEGUA REYNOSO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "688",
            "province_id": "67",
            "ubigeo": "04",
            "code": "070104",
            "name": "LA PERLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "689",
            "province_id": "67",
            "ubigeo": "05",
            "code": "070105",
            "name": "LA PUNTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "690",
            "province_id": "67",
            "ubigeo": "06",
            "code": "070106",
            "name": "VENTANILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "691",
            "province_id": "67",
            "ubigeo": "07",
            "code": "070107",
            "name": "MI PERÚ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "692",
            "province_id": "68",
            "ubigeo": "01",
            "code": "080101",
            "name": "CUSCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "693",
            "province_id": "68",
            "ubigeo": "02",
            "code": "080102",
            "name": "CCORCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "694",
            "province_id": "68",
            "ubigeo": "03",
            "code": "080103",
            "name": "POROY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "695",
            "province_id": "68",
            "ubigeo": "04",
            "code": "080104",
            "name": "SAN JERÓNIMO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "696",
            "province_id": "68",
            "ubigeo": "05",
            "code": "080105",
            "name": "SAN SEBASTIAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "697",
            "province_id": "68",
            "ubigeo": "06",
            "code": "080106",
            "name": "SANTIAGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "698",
            "province_id": "68",
            "ubigeo": "07",
            "code": "080107",
            "name": "SAYLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "699",
            "province_id": "68",
            "ubigeo": "08",
            "code": "080108",
            "name": "WANCHAQ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "700",
            "province_id": "69",
            "ubigeo": "01",
            "code": "080201",
            "name": "ACOMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "701",
            "province_id": "69",
            "ubigeo": "02",
            "code": "080202",
            "name": "ACOPIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "702",
            "province_id": "69",
            "ubigeo": "03",
            "code": "080203",
            "name": "ACOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "703",
            "province_id": "69",
            "ubigeo": "04",
            "code": "080204",
            "name": "MOSOC LLACTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "704",
            "province_id": "69",
            "ubigeo": "05",
            "code": "080205",
            "name": "POMACANCHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "705",
            "province_id": "69",
            "ubigeo": "06",
            "code": "080206",
            "name": "RONDOCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "706",
            "province_id": "69",
            "ubigeo": "07",
            "code": "080207",
            "name": "SANGARARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "707",
            "province_id": "70",
            "ubigeo": "01",
            "code": "080301",
            "name": "ANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "708",
            "province_id": "70",
            "ubigeo": "02",
            "code": "080302",
            "name": "ANCAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "709",
            "province_id": "70",
            "ubigeo": "03",
            "code": "080303",
            "name": "CACHIMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "710",
            "province_id": "70",
            "ubigeo": "04",
            "code": "080304",
            "name": "CHINCHAYPUJIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "711",
            "province_id": "70",
            "ubigeo": "05",
            "code": "080305",
            "name": "HUAROCONDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "712",
            "province_id": "70",
            "ubigeo": "06",
            "code": "080306",
            "name": "LIMATAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "713",
            "province_id": "70",
            "ubigeo": "07",
            "code": "080307",
            "name": "MOLLEPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "714",
            "province_id": "70",
            "ubigeo": "08",
            "code": "080308",
            "name": "PUCYURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "715",
            "province_id": "70",
            "ubigeo": "09",
            "code": "080309",
            "name": "ZURITE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "716",
            "province_id": "71",
            "ubigeo": "01",
            "code": "080401",
            "name": "CALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "717",
            "province_id": "71",
            "ubigeo": "02",
            "code": "080402",
            "name": "COYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "718",
            "province_id": "71",
            "ubigeo": "03",
            "code": "080403",
            "name": "LAMAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "719",
            "province_id": "71",
            "ubigeo": "04",
            "code": "080404",
            "name": "LARES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "720",
            "province_id": "71",
            "ubigeo": "05",
            "code": "080405",
            "name": "PISAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "721",
            "province_id": "71",
            "ubigeo": "06",
            "code": "080406",
            "name": "SAN SALVADOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "722",
            "province_id": "71",
            "ubigeo": "07",
            "code": "080407",
            "name": "TARAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "723",
            "province_id": "71",
            "ubigeo": "08",
            "code": "080408",
            "name": "YANATILE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "724",
            "province_id": "72",
            "ubigeo": "01",
            "code": "080501",
            "name": "YANAOCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "725",
            "province_id": "72",
            "ubigeo": "02",
            "code": "080502",
            "name": "CHECCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "726",
            "province_id": "72",
            "ubigeo": "03",
            "code": "080503",
            "name": "KUNTURKANKI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "727",
            "province_id": "72",
            "ubigeo": "04",
            "code": "080504",
            "name": "LANGUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "728",
            "province_id": "72",
            "ubigeo": "05",
            "code": "080505",
            "name": "LAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "729",
            "province_id": "72",
            "ubigeo": "06",
            "code": "080506",
            "name": "PAMPAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "730",
            "province_id": "72",
            "ubigeo": "07",
            "code": "080507",
            "name": "QUEHUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "731",
            "province_id": "72",
            "ubigeo": "08",
            "code": "080508",
            "name": "TUPAC AMARU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "732",
            "province_id": "73",
            "ubigeo": "01",
            "code": "080601",
            "name": "SICUANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "733",
            "province_id": "73",
            "ubigeo": "02",
            "code": "080602",
            "name": "CHECACUPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "734",
            "province_id": "73",
            "ubigeo": "03",
            "code": "080603",
            "name": "COMBAPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "735",
            "province_id": "73",
            "ubigeo": "04",
            "code": "080604",
            "name": "MARANGANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "736",
            "province_id": "73",
            "ubigeo": "05",
            "code": "080605",
            "name": "PITUMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "737",
            "province_id": "73",
            "ubigeo": "06",
            "code": "080606",
            "name": "SAN PABLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "738",
            "province_id": "73",
            "ubigeo": "07",
            "code": "080607",
            "name": "SAN PEDRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "739",
            "province_id": "73",
            "ubigeo": "08",
            "code": "080608",
            "name": "TINTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "740",
            "province_id": "74",
            "ubigeo": "01",
            "code": "080701",
            "name": "SANTO TOMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "741",
            "province_id": "74",
            "ubigeo": "02",
            "code": "080702",
            "name": "CAPACMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "742",
            "province_id": "74",
            "ubigeo": "03",
            "code": "080703",
            "name": "CHAMACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "743",
            "province_id": "74",
            "ubigeo": "04",
            "code": "080704",
            "name": "COLQUEMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "744",
            "province_id": "74",
            "ubigeo": "05",
            "code": "080705",
            "name": "LIVITACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "745",
            "province_id": "74",
            "ubigeo": "06",
            "code": "080706",
            "name": "LLUSCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "746",
            "province_id": "74",
            "ubigeo": "07",
            "code": "080707",
            "name": "QUIÑOTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "747",
            "province_id": "74",
            "ubigeo": "08",
            "code": "080708",
            "name": "VELILLE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "748",
            "province_id": "75",
            "ubigeo": "01",
            "code": "080801",
            "name": "ESPINAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "749",
            "province_id": "75",
            "ubigeo": "02",
            "code": "080802",
            "name": "CONDOROMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "750",
            "province_id": "75",
            "ubigeo": "03",
            "code": "080803",
            "name": "COPORAQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "751",
            "province_id": "75",
            "ubigeo": "04",
            "code": "080804",
            "name": "OCORURO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "752",
            "province_id": "75",
            "ubigeo": "05",
            "code": "080805",
            "name": "PALLPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "753",
            "province_id": "75",
            "ubigeo": "06",
            "code": "080806",
            "name": "PICHIGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "754",
            "province_id": "75",
            "ubigeo": "07",
            "code": "080807",
            "name": "SUYCKUTAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "755",
            "province_id": "75",
            "ubigeo": "08",
            "code": "080808",
            "name": "ALTO PICHIGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "756",
            "province_id": "76",
            "ubigeo": "01",
            "code": "080901",
            "name": "SANTA ANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "757",
            "province_id": "76",
            "ubigeo": "02",
            "code": "080902",
            "name": "ECHARATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "758",
            "province_id": "76",
            "ubigeo": "03",
            "code": "080903",
            "name": "HUAYOPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "759",
            "province_id": "76",
            "ubigeo": "04",
            "code": "080904",
            "name": "MARANURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "760",
            "province_id": "76",
            "ubigeo": "05",
            "code": "080905",
            "name": "OCOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "761",
            "province_id": "76",
            "ubigeo": "06",
            "code": "080906",
            "name": "QUELLOUNO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "762",
            "province_id": "76",
            "ubigeo": "07",
            "code": "080907",
            "name": "KIMBIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "763",
            "province_id": "76",
            "ubigeo": "08",
            "code": "080908",
            "name": "SANTA TERESA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "764",
            "province_id": "76",
            "ubigeo": "09",
            "code": "080909",
            "name": "VILCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "765",
            "province_id": "76",
            "ubigeo": "10",
            "code": "080910",
            "name": "PICHARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "766",
            "province_id": "76",
            "ubigeo": "11",
            "code": "080911",
            "name": "INKAWASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "767",
            "province_id": "76",
            "ubigeo": "12",
            "code": "080912",
            "name": "VILLA VIRGEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "768",
            "province_id": "76",
            "ubigeo": "13",
            "code": "080913",
            "name": "VILLA KINTIARINA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "769",
            "province_id": "77",
            "ubigeo": "01",
            "code": "081001",
            "name": "PARURO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "770",
            "province_id": "77",
            "ubigeo": "02",
            "code": "081002",
            "name": "ACCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "771",
            "province_id": "77",
            "ubigeo": "03",
            "code": "081003",
            "name": "CCAPI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "772",
            "province_id": "77",
            "ubigeo": "04",
            "code": "081004",
            "name": "COLCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "773",
            "province_id": "77",
            "ubigeo": "05",
            "code": "081005",
            "name": "HUANOQUITE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "774",
            "province_id": "77",
            "ubigeo": "06",
            "code": "081006",
            "name": "OMACHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "775",
            "province_id": "77",
            "ubigeo": "07",
            "code": "081007",
            "name": "PACCARITAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "776",
            "province_id": "77",
            "ubigeo": "08",
            "code": "081008",
            "name": "PILLPINTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "777",
            "province_id": "77",
            "ubigeo": "09",
            "code": "081009",
            "name": "YAURISQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "778",
            "province_id": "78",
            "ubigeo": "01",
            "code": "081101",
            "name": "PAUCARTAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "779",
            "province_id": "78",
            "ubigeo": "02",
            "code": "081102",
            "name": "CAICAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "780",
            "province_id": "78",
            "ubigeo": "03",
            "code": "081103",
            "name": "CHALLABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "781",
            "province_id": "78",
            "ubigeo": "04",
            "code": "081104",
            "name": "COLQUEPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "782",
            "province_id": "78",
            "ubigeo": "05",
            "code": "081105",
            "name": "HUANCARANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "783",
            "province_id": "78",
            "ubigeo": "06",
            "code": "081106",
            "name": "KOSÑIPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "784",
            "province_id": "79",
            "ubigeo": "01",
            "code": "081201",
            "name": "URCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "785",
            "province_id": "79",
            "ubigeo": "02",
            "code": "081202",
            "name": "ANDAHUAYLILLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "786",
            "province_id": "79",
            "ubigeo": "03",
            "code": "081203",
            "name": "CAMANTI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "787",
            "province_id": "79",
            "ubigeo": "04",
            "code": "081204",
            "name": "CCARHUAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "788",
            "province_id": "79",
            "ubigeo": "05",
            "code": "081205",
            "name": "CCATCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "789",
            "province_id": "79",
            "ubigeo": "06",
            "code": "081206",
            "name": "CUSIPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "790",
            "province_id": "79",
            "ubigeo": "07",
            "code": "081207",
            "name": "HUARO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "791",
            "province_id": "79",
            "ubigeo": "08",
            "code": "081208",
            "name": "LUCRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "792",
            "province_id": "79",
            "ubigeo": "09",
            "code": "081209",
            "name": "MARCAPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "793",
            "province_id": "79",
            "ubigeo": "10",
            "code": "081210",
            "name": "OCONGATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "794",
            "province_id": "79",
            "ubigeo": "11",
            "code": "081211",
            "name": "OROPESA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "795",
            "province_id": "79",
            "ubigeo": "12",
            "code": "081212",
            "name": "QUIQUIJANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "796",
            "province_id": "80",
            "ubigeo": "01",
            "code": "081301",
            "name": "URUBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "797",
            "province_id": "80",
            "ubigeo": "02",
            "code": "081302",
            "name": "CHINCHERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "798",
            "province_id": "80",
            "ubigeo": "03",
            "code": "081303",
            "name": "HUAYLLABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "799",
            "province_id": "80",
            "ubigeo": "04",
            "code": "081304",
            "name": "MACHUPICCHU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "800",
            "province_id": "80",
            "ubigeo": "05",
            "code": "081305",
            "name": "MARAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "801",
            "province_id": "80",
            "ubigeo": "06",
            "code": "081306",
            "name": "OLLANTAYTAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "802",
            "province_id": "80",
            "ubigeo": "07",
            "code": "081307",
            "name": "YUCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "803",
            "province_id": "81",
            "ubigeo": "01",
            "code": "090101",
            "name": "HUANCAVELICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "804",
            "province_id": "81",
            "ubigeo": "02",
            "code": "090102",
            "name": "ACOBAMBILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "805",
            "province_id": "81",
            "ubigeo": "03",
            "code": "090103",
            "name": "ACORIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "806",
            "province_id": "81",
            "ubigeo": "04",
            "code": "090104",
            "name": "CONAYCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "807",
            "province_id": "81",
            "ubigeo": "05",
            "code": "090105",
            "name": "CUENCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "808",
            "province_id": "81",
            "ubigeo": "06",
            "code": "090106",
            "name": "HUACHOCOLPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "809",
            "province_id": "81",
            "ubigeo": "07",
            "code": "090107",
            "name": "HUAYLLAHUARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "810",
            "province_id": "81",
            "ubigeo": "08",
            "code": "090108",
            "name": "IZCUCHACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "811",
            "province_id": "81",
            "ubigeo": "09",
            "code": "090109",
            "name": "LARIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "812",
            "province_id": "81",
            "ubigeo": "10",
            "code": "090110",
            "name": "MANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "813",
            "province_id": "81",
            "ubigeo": "11",
            "code": "090111",
            "name": "MARISCAL CÁCERES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "814",
            "province_id": "81",
            "ubigeo": "12",
            "code": "090112",
            "name": "MOYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "815",
            "province_id": "81",
            "ubigeo": "13",
            "code": "090113",
            "name": "NUEVO OCCORO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "816",
            "province_id": "81",
            "ubigeo": "14",
            "code": "090114",
            "name": "PALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "817",
            "province_id": "81",
            "ubigeo": "15",
            "code": "090115",
            "name": "PILCHACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "818",
            "province_id": "81",
            "ubigeo": "16",
            "code": "090116",
            "name": "VILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "819",
            "province_id": "81",
            "ubigeo": "17",
            "code": "090117",
            "name": "YAULI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "820",
            "province_id": "81",
            "ubigeo": "18",
            "code": "090118",
            "name": "ASCENSIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "821",
            "province_id": "81",
            "ubigeo": "19",
            "code": "090119",
            "name": "HUANDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "822",
            "province_id": "82",
            "ubigeo": "01",
            "code": "090201",
            "name": "ACOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "823",
            "province_id": "82",
            "ubigeo": "02",
            "code": "090202",
            "name": "ANDABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "824",
            "province_id": "82",
            "ubigeo": "03",
            "code": "090203",
            "name": "ANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "825",
            "province_id": "82",
            "ubigeo": "04",
            "code": "090204",
            "name": "CAJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "826",
            "province_id": "82",
            "ubigeo": "05",
            "code": "090205",
            "name": "MARCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "827",
            "province_id": "82",
            "ubigeo": "06",
            "code": "090206",
            "name": "PAUCARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "828",
            "province_id": "82",
            "ubigeo": "07",
            "code": "090207",
            "name": "POMACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "829",
            "province_id": "82",
            "ubigeo": "08",
            "code": "090208",
            "name": "ROSARIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "830",
            "province_id": "83",
            "ubigeo": "01",
            "code": "090301",
            "name": "LIRCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "831",
            "province_id": "83",
            "ubigeo": "02",
            "code": "090302",
            "name": "ANCHONGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "832",
            "province_id": "83",
            "ubigeo": "03",
            "code": "090303",
            "name": "CALLANMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "833",
            "province_id": "83",
            "ubigeo": "04",
            "code": "090304",
            "name": "CCOCHACCASA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "834",
            "province_id": "83",
            "ubigeo": "05",
            "code": "090305",
            "name": "CHINCHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "835",
            "province_id": "83",
            "ubigeo": "06",
            "code": "090306",
            "name": "CONGALLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "836",
            "province_id": "83",
            "ubigeo": "07",
            "code": "090307",
            "name": "HUANCA-HUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "837",
            "province_id": "83",
            "ubigeo": "08",
            "code": "090308",
            "name": "HUAYLLAY GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "838",
            "province_id": "83",
            "ubigeo": "09",
            "code": "090309",
            "name": "JULCAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "839",
            "province_id": "83",
            "ubigeo": "10",
            "code": "090310",
            "name": "SAN ANTONIO DE ANTAPARCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "840",
            "province_id": "83",
            "ubigeo": "11",
            "code": "090311",
            "name": "SANTO TOMAS DE PATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "841",
            "province_id": "83",
            "ubigeo": "12",
            "code": "090312",
            "name": "SECCLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "842",
            "province_id": "84",
            "ubigeo": "01",
            "code": "090401",
            "name": "CASTROVIRREYNA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "843",
            "province_id": "84",
            "ubigeo": "02",
            "code": "090402",
            "name": "ARMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "844",
            "province_id": "84",
            "ubigeo": "03",
            "code": "090403",
            "name": "AURAHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "845",
            "province_id": "84",
            "ubigeo": "04",
            "code": "090404",
            "name": "CAPILLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "846",
            "province_id": "84",
            "ubigeo": "05",
            "code": "090405",
            "name": "CHUPAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "847",
            "province_id": "84",
            "ubigeo": "06",
            "code": "090406",
            "name": "COCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "848",
            "province_id": "84",
            "ubigeo": "07",
            "code": "090407",
            "name": "HUACHOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "849",
            "province_id": "84",
            "ubigeo": "08",
            "code": "090408",
            "name": "HUAMATAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "850",
            "province_id": "84",
            "ubigeo": "09",
            "code": "090409",
            "name": "MOLLEPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "851",
            "province_id": "84",
            "ubigeo": "10",
            "code": "090410",
            "name": "SAN JUAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "852",
            "province_id": "84",
            "ubigeo": "11",
            "code": "090411",
            "name": "SANTA ANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "853",
            "province_id": "84",
            "ubigeo": "12",
            "code": "090412",
            "name": "TANTARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "854",
            "province_id": "84",
            "ubigeo": "13",
            "code": "090413",
            "name": "TICRAPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "855",
            "province_id": "85",
            "ubigeo": "01",
            "code": "090501",
            "name": "CHURCAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "856",
            "province_id": "85",
            "ubigeo": "02",
            "code": "090502",
            "name": "ANCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "857",
            "province_id": "85",
            "ubigeo": "03",
            "code": "090503",
            "name": "CHINCHIHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "858",
            "province_id": "85",
            "ubigeo": "04",
            "code": "090504",
            "name": "EL CARMEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "859",
            "province_id": "85",
            "ubigeo": "05",
            "code": "090505",
            "name": "LA MERCED",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "860",
            "province_id": "85",
            "ubigeo": "06",
            "code": "090506",
            "name": "LOCROJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "861",
            "province_id": "85",
            "ubigeo": "07",
            "code": "090507",
            "name": "PAUCARBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "862",
            "province_id": "85",
            "ubigeo": "08",
            "code": "090508",
            "name": "SAN MIGUEL DE MAYOCC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "863",
            "province_id": "85",
            "ubigeo": "09",
            "code": "090509",
            "name": "SAN PEDRO DE CORIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "864",
            "province_id": "85",
            "ubigeo": "10",
            "code": "090510",
            "name": "PACHAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "865",
            "province_id": "85",
            "ubigeo": "11",
            "code": "090511",
            "name": "COSME",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "866",
            "province_id": "86",
            "ubigeo": "01",
            "code": "090601",
            "name": "HUAYTARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "867",
            "province_id": "86",
            "ubigeo": "02",
            "code": "090602",
            "name": "AYAVI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "868",
            "province_id": "86",
            "ubigeo": "03",
            "code": "090603",
            "name": "CÓRDOVA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "869",
            "province_id": "86",
            "ubigeo": "04",
            "code": "090604",
            "name": "HUAYACUNDO ARMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "870",
            "province_id": "86",
            "ubigeo": "05",
            "code": "090605",
            "name": "LARAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "871",
            "province_id": "86",
            "ubigeo": "06",
            "code": "090606",
            "name": "OCOYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "872",
            "province_id": "86",
            "ubigeo": "07",
            "code": "090607",
            "name": "PILPICHACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "873",
            "province_id": "86",
            "ubigeo": "08",
            "code": "090608",
            "name": "QUERCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "874",
            "province_id": "86",
            "ubigeo": "09",
            "code": "090609",
            "name": "QUITO-ARMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "875",
            "province_id": "86",
            "ubigeo": "10",
            "code": "090610",
            "name": "SAN ANTONIO DE CUSICANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "876",
            "province_id": "86",
            "ubigeo": "11",
            "code": "090611",
            "name": "SAN FRANCISCO DE SANGAYAICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "877",
            "province_id": "86",
            "ubigeo": "12",
            "code": "090612",
            "name": "SAN ISIDRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "878",
            "province_id": "86",
            "ubigeo": "13",
            "code": "090613",
            "name": "SANTIAGO DE CHOCORVOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "879",
            "province_id": "86",
            "ubigeo": "14",
            "code": "090614",
            "name": "SANTIAGO DE QUIRAHUARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "880",
            "province_id": "86",
            "ubigeo": "15",
            "code": "090615",
            "name": "SANTO DOMINGO DE CAPILLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "881",
            "province_id": "86",
            "ubigeo": "16",
            "code": "090616",
            "name": "TAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "882",
            "province_id": "87",
            "ubigeo": "01",
            "code": "090701",
            "name": "PAMPAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "883",
            "province_id": "87",
            "ubigeo": "02",
            "code": "090702",
            "name": "ACOSTAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "884",
            "province_id": "87",
            "ubigeo": "03",
            "code": "090703",
            "name": "ACRAQUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "885",
            "province_id": "87",
            "ubigeo": "04",
            "code": "090704",
            "name": "AHUAYCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "886",
            "province_id": "87",
            "ubigeo": "05",
            "code": "090705",
            "name": "COLCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "887",
            "province_id": "87",
            "ubigeo": "06",
            "code": "090706",
            "name": "DANIEL HERNÁNDEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "888",
            "province_id": "87",
            "ubigeo": "07",
            "code": "090707",
            "name": "HUACHOCOLPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "889",
            "province_id": "87",
            "ubigeo": "09",
            "code": "090709",
            "name": "HUARIBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "890",
            "province_id": "87",
            "ubigeo": "10",
            "code": "090710",
            "name": "ÑAHUIMPUQUIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "891",
            "province_id": "87",
            "ubigeo": "11",
            "code": "090711",
            "name": "PAZOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "892",
            "province_id": "87",
            "ubigeo": "13",
            "code": "090713",
            "name": "QUISHUAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "893",
            "province_id": "87",
            "ubigeo": "14",
            "code": "090714",
            "name": "SALCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "894",
            "province_id": "87",
            "ubigeo": "15",
            "code": "090715",
            "name": "SALCAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "895",
            "province_id": "87",
            "ubigeo": "16",
            "code": "090716",
            "name": "SAN MARCOS DE ROCCHAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "896",
            "province_id": "87",
            "ubigeo": "17",
            "code": "090717",
            "name": "SURCUBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "897",
            "province_id": "87",
            "ubigeo": "18",
            "code": "090718",
            "name": "TINTAY PUNCU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "898",
            "province_id": "87",
            "ubigeo": "19",
            "code": "090719",
            "name": "QUICHUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "899",
            "province_id": "87",
            "ubigeo": "20",
            "code": "090720",
            "name": "ANDAYMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:24:50"
        },
        {
            "id": "900",
            "province_id": "88",
            "ubigeo": "01",
            "code": "100101",
            "name": "HUANUCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "901",
            "province_id": "88",
            "ubigeo": "02",
            "code": "100102",
            "name": "AMARILIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "902",
            "province_id": "88",
            "ubigeo": "03",
            "code": "100103",
            "name": "CHINCHAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "903",
            "province_id": "88",
            "ubigeo": "04",
            "code": "100104",
            "name": "CHURUBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "904",
            "province_id": "88",
            "ubigeo": "05",
            "code": "100105",
            "name": "MARGOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "905",
            "province_id": "88",
            "ubigeo": "06",
            "code": "100106",
            "name": "QUISQUI (KICHKI)",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "906",
            "province_id": "88",
            "ubigeo": "07",
            "code": "100107",
            "name": "SAN FRANCISCO DE CAYRAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "907",
            "province_id": "88",
            "ubigeo": "08",
            "code": "100108",
            "name": "SAN PEDRO DE CHAULAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "908",
            "province_id": "88",
            "ubigeo": "09",
            "code": "100109",
            "name": "SANTA MARÍA DEL VALLE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "909",
            "province_id": "88",
            "ubigeo": "10",
            "code": "100110",
            "name": "YARUMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "910",
            "province_id": "88",
            "ubigeo": "11",
            "code": "100111",
            "name": "PILLCO MARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "911",
            "province_id": "88",
            "ubigeo": "12",
            "code": "100112",
            "name": "YACUS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "912",
            "province_id": "89",
            "ubigeo": "01",
            "code": "100201",
            "name": "AMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "913",
            "province_id": "89",
            "ubigeo": "02",
            "code": "100202",
            "name": "CAYNA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "914",
            "province_id": "89",
            "ubigeo": "03",
            "code": "100203",
            "name": "COLPAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "915",
            "province_id": "89",
            "ubigeo": "04",
            "code": "100204",
            "name": "CONCHAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "916",
            "province_id": "89",
            "ubigeo": "05",
            "code": "100205",
            "name": "HUACAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "917",
            "province_id": "89",
            "ubigeo": "06",
            "code": "100206",
            "name": "SAN FRANCISCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "918",
            "province_id": "89",
            "ubigeo": "07",
            "code": "100207",
            "name": "SAN RAFAEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "919",
            "province_id": "89",
            "ubigeo": "08",
            "code": "100208",
            "name": "TOMAY KICHWA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "920",
            "province_id": "90",
            "ubigeo": "01",
            "code": "100301",
            "name": "LA UNIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "921",
            "province_id": "90",
            "ubigeo": "07",
            "code": "100307",
            "name": "CHUQUIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "922",
            "province_id": "90",
            "ubigeo": "11",
            "code": "100311",
            "name": "MARÍAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "923",
            "province_id": "90",
            "ubigeo": "13",
            "code": "100313",
            "name": "PACHAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "924",
            "province_id": "90",
            "ubigeo": "16",
            "code": "100316",
            "name": "QUIVILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "925",
            "province_id": "90",
            "ubigeo": "17",
            "code": "100317",
            "name": "RIPAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "926",
            "province_id": "90",
            "ubigeo": "21",
            "code": "100321",
            "name": "SHUNQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "927",
            "province_id": "90",
            "ubigeo": "22",
            "code": "100322",
            "name": "SILLAPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "928",
            "province_id": "90",
            "ubigeo": "23",
            "code": "100323",
            "name": "YANAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "929",
            "province_id": "91",
            "ubigeo": "01",
            "code": "100401",
            "name": "HUACAYBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "930",
            "province_id": "91",
            "ubigeo": "02",
            "code": "100402",
            "name": "CANCHABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "931",
            "province_id": "91",
            "ubigeo": "03",
            "code": "100403",
            "name": "COCHABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "932",
            "province_id": "91",
            "ubigeo": "04",
            "code": "100404",
            "name": "PINRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "933",
            "province_id": "92",
            "ubigeo": "01",
            "code": "100501",
            "name": "LLATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "934",
            "province_id": "92",
            "ubigeo": "02",
            "code": "100502",
            "name": "ARANCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "935",
            "province_id": "92",
            "ubigeo": "03",
            "code": "100503",
            "name": "CHAVÍN DE PARIARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "936",
            "province_id": "92",
            "ubigeo": "04",
            "code": "100504",
            "name": "JACAS GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "937",
            "province_id": "92",
            "ubigeo": "05",
            "code": "100505",
            "name": "JIRCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "938",
            "province_id": "92",
            "ubigeo": "06",
            "code": "100506",
            "name": "MIRAFLORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "939",
            "province_id": "92",
            "ubigeo": "07",
            "code": "100507",
            "name": "MONZÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "940",
            "province_id": "92",
            "ubigeo": "08",
            "code": "100508",
            "name": "PUNCHAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "941",
            "province_id": "92",
            "ubigeo": "09",
            "code": "100509",
            "name": "PUÑOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "942",
            "province_id": "92",
            "ubigeo": "10",
            "code": "100510",
            "name": "SINGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "943",
            "province_id": "92",
            "ubigeo": "11",
            "code": "100511",
            "name": "TANTAMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "944",
            "province_id": "93",
            "ubigeo": "01",
            "code": "100601",
            "name": "RUPA-RUPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "945",
            "province_id": "93",
            "ubigeo": "02",
            "code": "100602",
            "name": "DANIEL ALOMÍA ROBLES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "946",
            "province_id": "93",
            "ubigeo": "03",
            "code": "100603",
            "name": "HERMÍLIO VALDIZAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "947",
            "province_id": "93",
            "ubigeo": "04",
            "code": "100604",
            "name": "JOSÉ CRESPO Y CASTILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "948",
            "province_id": "93",
            "ubigeo": "05",
            "code": "100605",
            "name": "LUYANDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "949",
            "province_id": "93",
            "ubigeo": "06",
            "code": "100606",
            "name": "MARIANO DAMASO BERAUN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "950",
            "province_id": "94",
            "ubigeo": "01",
            "code": "100701",
            "name": "HUACRACHUCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "951",
            "province_id": "94",
            "ubigeo": "02",
            "code": "100702",
            "name": "CHOLON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "952",
            "province_id": "94",
            "ubigeo": "03",
            "code": "100703",
            "name": "SAN BUENAVENTURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "953",
            "province_id": "95",
            "ubigeo": "01",
            "code": "100801",
            "name": "PANAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "954",
            "province_id": "95",
            "ubigeo": "02",
            "code": "100802",
            "name": "CHAGLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "955",
            "province_id": "95",
            "ubigeo": "03",
            "code": "100803",
            "name": "MOLINO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "956",
            "province_id": "95",
            "ubigeo": "04",
            "code": "100804",
            "name": "UMARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "957",
            "province_id": "96",
            "ubigeo": "01",
            "code": "100901",
            "name": "PUERTO INCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "958",
            "province_id": "96",
            "ubigeo": "02",
            "code": "100902",
            "name": "CODO DEL POZUZO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "959",
            "province_id": "96",
            "ubigeo": "03",
            "code": "100903",
            "name": "HONORIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "960",
            "province_id": "96",
            "ubigeo": "04",
            "code": "100904",
            "name": "TOURNAVISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "961",
            "province_id": "96",
            "ubigeo": "05",
            "code": "100905",
            "name": "YUYAPICHIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "962",
            "province_id": "97",
            "ubigeo": "01",
            "code": "101001",
            "name": "JESÚS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "963",
            "province_id": "97",
            "ubigeo": "02",
            "code": "101002",
            "name": "BAÑOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "964",
            "province_id": "97",
            "ubigeo": "03",
            "code": "101003",
            "name": "JIVIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "965",
            "province_id": "97",
            "ubigeo": "04",
            "code": "101004",
            "name": "QUEROPALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "966",
            "province_id": "97",
            "ubigeo": "05",
            "code": "101005",
            "name": "RONDOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "967",
            "province_id": "97",
            "ubigeo": "06",
            "code": "101006",
            "name": "SAN FRANCISCO DE ASÍS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "968",
            "province_id": "97",
            "ubigeo": "07",
            "code": "101007",
            "name": "SAN MIGUEL DE CAURI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "969",
            "province_id": "98",
            "ubigeo": "01",
            "code": "101101",
            "name": "CHAVINILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "970",
            "province_id": "98",
            "ubigeo": "02",
            "code": "101102",
            "name": "CAHUAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "971",
            "province_id": "98",
            "ubigeo": "03",
            "code": "101103",
            "name": "CHACABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "972",
            "province_id": "98",
            "ubigeo": "04",
            "code": "101104",
            "name": "APARICIO POMARES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "973",
            "province_id": "98",
            "ubigeo": "05",
            "code": "101105",
            "name": "JACAS CHICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "974",
            "province_id": "98",
            "ubigeo": "06",
            "code": "101106",
            "name": "OBAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "975",
            "province_id": "98",
            "ubigeo": "07",
            "code": "101107",
            "name": "PAMPAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "976",
            "province_id": "98",
            "ubigeo": "08",
            "code": "101108",
            "name": "CHORAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "977",
            "province_id": "99",
            "ubigeo": "01",
            "code": "110101",
            "name": "ICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "978",
            "province_id": "99",
            "ubigeo": "02",
            "code": "110102",
            "name": "LA TINGUIÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "979",
            "province_id": "99",
            "ubigeo": "03",
            "code": "110103",
            "name": "LOS AQUIJES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "980",
            "province_id": "99",
            "ubigeo": "04",
            "code": "110104",
            "name": "OCUCAJE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "981",
            "province_id": "99",
            "ubigeo": "05",
            "code": "110105",
            "name": "PACHACUTEC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "982",
            "province_id": "99",
            "ubigeo": "06",
            "code": "110106",
            "name": "PARCONA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "983",
            "province_id": "99",
            "ubigeo": "07",
            "code": "110107",
            "name": "PUEBLO NUEVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "984",
            "province_id": "99",
            "ubigeo": "08",
            "code": "110108",
            "name": "SALAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "985",
            "province_id": "99",
            "ubigeo": "09",
            "code": "110109",
            "name": "SAN JOSÉ DE LOS MOLINOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "986",
            "province_id": "99",
            "ubigeo": "10",
            "code": "110110",
            "name": "SAN JUAN BAUTISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "987",
            "province_id": "99",
            "ubigeo": "11",
            "code": "110111",
            "name": "SANTIAGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "988",
            "province_id": "99",
            "ubigeo": "12",
            "code": "110112",
            "name": "SUBTANJALLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "989",
            "province_id": "99",
            "ubigeo": "13",
            "code": "110113",
            "name": "TATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "990",
            "province_id": "99",
            "ubigeo": "14",
            "code": "110114",
            "name": "YAUCA DEL ROSARIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "991",
            "province_id": "100",
            "ubigeo": "01",
            "code": "110201",
            "name": "CHINCHA ALTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "992",
            "province_id": "100",
            "ubigeo": "02",
            "code": "110202",
            "name": "ALTO LARAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "993",
            "province_id": "100",
            "ubigeo": "03",
            "code": "110203",
            "name": "CHAVIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "994",
            "province_id": "100",
            "ubigeo": "04",
            "code": "110204",
            "name": "CHINCHA BAJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "995",
            "province_id": "100",
            "ubigeo": "05",
            "code": "110205",
            "name": "EL CARMEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "996",
            "province_id": "100",
            "ubigeo": "06",
            "code": "110206",
            "name": "GROCIO PRADO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "997",
            "province_id": "100",
            "ubigeo": "07",
            "code": "110207",
            "name": "PUEBLO NUEVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "998",
            "province_id": "100",
            "ubigeo": "08",
            "code": "110208",
            "name": "SAN JUAN DE YANAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "999",
            "province_id": "100",
            "ubigeo": "09",
            "code": "110209",
            "name": "SAN PEDRO DE HUACARPANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "1000",
            "province_id": "100",
            "ubigeo": "10",
            "code": "110210",
            "name": "SUNAMPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:22:38"
        },
        {
            "id": "1001",
            "province_id": "100",
            "ubigeo": "11",
            "code": "110211",
            "name": "TAMBO DE MORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1002",
            "province_id": "101",
            "ubigeo": "01",
            "code": "110301",
            "name": "NASCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1003",
            "province_id": "101",
            "ubigeo": "02",
            "code": "110302",
            "name": "CHANGUILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1004",
            "province_id": "101",
            "ubigeo": "03",
            "code": "110303",
            "name": "EL INGENIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1005",
            "province_id": "101",
            "ubigeo": "04",
            "code": "110304",
            "name": "MARCONA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1006",
            "province_id": "101",
            "ubigeo": "05",
            "code": "110305",
            "name": "VISTA ALEGRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1007",
            "province_id": "102",
            "ubigeo": "01",
            "code": "110401",
            "name": "PALPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1008",
            "province_id": "102",
            "ubigeo": "02",
            "code": "110402",
            "name": "LLIPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1009",
            "province_id": "102",
            "ubigeo": "03",
            "code": "110403",
            "name": "RÍO GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1010",
            "province_id": "102",
            "ubigeo": "04",
            "code": "110404",
            "name": "SANTA CRUZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1011",
            "province_id": "102",
            "ubigeo": "05",
            "code": "110405",
            "name": "TIBILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1012",
            "province_id": "103",
            "ubigeo": "01",
            "code": "110501",
            "name": "PISCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1013",
            "province_id": "103",
            "ubigeo": "02",
            "code": "110502",
            "name": "HUANCANO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1014",
            "province_id": "103",
            "ubigeo": "03",
            "code": "110503",
            "name": "HUMAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1015",
            "province_id": "103",
            "ubigeo": "04",
            "code": "110504",
            "name": "INDEPENDENCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1016",
            "province_id": "103",
            "ubigeo": "05",
            "code": "110505",
            "name": "PARACAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1017",
            "province_id": "103",
            "ubigeo": "06",
            "code": "110506",
            "name": "SAN ANDRÉS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1018",
            "province_id": "103",
            "ubigeo": "07",
            "code": "110507",
            "name": "SAN CLEMENTE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1019",
            "province_id": "103",
            "ubigeo": "08",
            "code": "110508",
            "name": "TUPAC AMARU INCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1020",
            "province_id": "104",
            "ubigeo": "01",
            "code": "120101",
            "name": "HUANCAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1021",
            "province_id": "104",
            "ubigeo": "04",
            "code": "120104",
            "name": "CARHUACALLANGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1022",
            "province_id": "104",
            "ubigeo": "05",
            "code": "120105",
            "name": "CHACAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1023",
            "province_id": "104",
            "ubigeo": "06",
            "code": "120106",
            "name": "CHICCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1024",
            "province_id": "104",
            "ubigeo": "07",
            "code": "120107",
            "name": "CHILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1025",
            "province_id": "104",
            "ubigeo": "08",
            "code": "120108",
            "name": "CHONGOS ALTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1026",
            "province_id": "104",
            "ubigeo": "11",
            "code": "120111",
            "name": "CHUPURO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1027",
            "province_id": "104",
            "ubigeo": "12",
            "code": "120112",
            "name": "COLCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1028",
            "province_id": "104",
            "ubigeo": "13",
            "code": "120113",
            "name": "CULLHUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1029",
            "province_id": "104",
            "ubigeo": "14",
            "code": "120114",
            "name": "EL TAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1030",
            "province_id": "104",
            "ubigeo": "16",
            "code": "120116",
            "name": "HUACRAPUQUIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1031",
            "province_id": "104",
            "ubigeo": "17",
            "code": "120117",
            "name": "HUALHUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1032",
            "province_id": "104",
            "ubigeo": "19",
            "code": "120119",
            "name": "HUANCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1033",
            "province_id": "104",
            "ubigeo": "20",
            "code": "120120",
            "name": "HUASICANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1034",
            "province_id": "104",
            "ubigeo": "21",
            "code": "120121",
            "name": "HUAYUCACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1035",
            "province_id": "104",
            "ubigeo": "22",
            "code": "120122",
            "name": "INGENIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1036",
            "province_id": "104",
            "ubigeo": "24",
            "code": "120124",
            "name": "PARIAHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1037",
            "province_id": "104",
            "ubigeo": "25",
            "code": "120125",
            "name": "PILCOMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1038",
            "province_id": "104",
            "ubigeo": "26",
            "code": "120126",
            "name": "PUCARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1039",
            "province_id": "104",
            "ubigeo": "27",
            "code": "120127",
            "name": "QUICHUAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1040",
            "province_id": "104",
            "ubigeo": "28",
            "code": "120128",
            "name": "QUILCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1041",
            "province_id": "104",
            "ubigeo": "29",
            "code": "120129",
            "name": "SAN AGUSTÍN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1042",
            "province_id": "104",
            "ubigeo": "30",
            "code": "120130",
            "name": "SAN JERÓNIMO DE TUNAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1043",
            "province_id": "104",
            "ubigeo": "32",
            "code": "120132",
            "name": "SAÑO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1044",
            "province_id": "104",
            "ubigeo": "33",
            "code": "120133",
            "name": "SAPALLANGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1045",
            "province_id": "104",
            "ubigeo": "34",
            "code": "120134",
            "name": "SICAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1046",
            "province_id": "104",
            "ubigeo": "35",
            "code": "120135",
            "name": "SANTO DOMINGO DE ACOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1047",
            "province_id": "104",
            "ubigeo": "36",
            "code": "120136",
            "name": "VIQUES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1048",
            "province_id": "105",
            "ubigeo": "01",
            "code": "120201",
            "name": "CONCEPCIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1049",
            "province_id": "105",
            "ubigeo": "02",
            "code": "120202",
            "name": "ACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1050",
            "province_id": "105",
            "ubigeo": "03",
            "code": "120203",
            "name": "ANDAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1051",
            "province_id": "105",
            "ubigeo": "04",
            "code": "120204",
            "name": "CHAMBARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1052",
            "province_id": "105",
            "ubigeo": "05",
            "code": "120205",
            "name": "COCHAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1053",
            "province_id": "105",
            "ubigeo": "06",
            "code": "120206",
            "name": "COMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1054",
            "province_id": "105",
            "ubigeo": "07",
            "code": "120207",
            "name": "HEROÍNAS TOLEDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1055",
            "province_id": "105",
            "ubigeo": "08",
            "code": "120208",
            "name": "MANZANARES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1056",
            "province_id": "105",
            "ubigeo": "09",
            "code": "120209",
            "name": "MARISCAL CASTILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1057",
            "province_id": "105",
            "ubigeo": "10",
            "code": "120210",
            "name": "MATAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1058",
            "province_id": "105",
            "ubigeo": "11",
            "code": "120211",
            "name": "MITO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1059",
            "province_id": "105",
            "ubigeo": "12",
            "code": "120212",
            "name": "NUEVE DE JULIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1060",
            "province_id": "105",
            "ubigeo": "13",
            "code": "120213",
            "name": "ORCOTUNA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1061",
            "province_id": "105",
            "ubigeo": "14",
            "code": "120214",
            "name": "SAN JOSÉ DE QUERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1062",
            "province_id": "105",
            "ubigeo": "15",
            "code": "120215",
            "name": "SANTA ROSA DE OCOPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1063",
            "province_id": "106",
            "ubigeo": "01",
            "code": "120301",
            "name": "CHANCHAMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1064",
            "province_id": "106",
            "ubigeo": "02",
            "code": "120302",
            "name": "PERENE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1065",
            "province_id": "106",
            "ubigeo": "03",
            "code": "120303",
            "name": "PICHANAQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1066",
            "province_id": "106",
            "ubigeo": "04",
            "code": "120304",
            "name": "SAN LUIS DE SHUARO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1067",
            "province_id": "106",
            "ubigeo": "05",
            "code": "120305",
            "name": "SAN RAMÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1068",
            "province_id": "106",
            "ubigeo": "06",
            "code": "120306",
            "name": "VITOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1069",
            "province_id": "107",
            "ubigeo": "01",
            "code": "120401",
            "name": "JAUJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1070",
            "province_id": "107",
            "ubigeo": "02",
            "code": "120402",
            "name": "ACOLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1071",
            "province_id": "107",
            "ubigeo": "03",
            "code": "120403",
            "name": "APATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1072",
            "province_id": "107",
            "ubigeo": "04",
            "code": "120404",
            "name": "ATAURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1073",
            "province_id": "107",
            "ubigeo": "05",
            "code": "120405",
            "name": "CANCHAYLLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1074",
            "province_id": "107",
            "ubigeo": "06",
            "code": "120406",
            "name": "CURICACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1075",
            "province_id": "107",
            "ubigeo": "07",
            "code": "120407",
            "name": "EL MANTARO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1076",
            "province_id": "107",
            "ubigeo": "08",
            "code": "120408",
            "name": "HUAMALI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1077",
            "province_id": "107",
            "ubigeo": "09",
            "code": "120409",
            "name": "HUARIPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1078",
            "province_id": "107",
            "ubigeo": "10",
            "code": "120410",
            "name": "HUERTAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1079",
            "province_id": "107",
            "ubigeo": "11",
            "code": "120411",
            "name": "JANJAILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1080",
            "province_id": "107",
            "ubigeo": "12",
            "code": "120412",
            "name": "JULCÁN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1081",
            "province_id": "107",
            "ubigeo": "13",
            "code": "120413",
            "name": "LEONOR ORDÓÑEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1082",
            "province_id": "107",
            "ubigeo": "14",
            "code": "120414",
            "name": "LLOCLLAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1083",
            "province_id": "107",
            "ubigeo": "15",
            "code": "120415",
            "name": "MARCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1084",
            "province_id": "107",
            "ubigeo": "16",
            "code": "120416",
            "name": "MASMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1085",
            "province_id": "107",
            "ubigeo": "17",
            "code": "120417",
            "name": "MASMA CHICCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1086",
            "province_id": "107",
            "ubigeo": "18",
            "code": "120418",
            "name": "MOLINOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1087",
            "province_id": "107",
            "ubigeo": "19",
            "code": "120419",
            "name": "MONOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1088",
            "province_id": "107",
            "ubigeo": "20",
            "code": "120420",
            "name": "MUQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1089",
            "province_id": "107",
            "ubigeo": "21",
            "code": "120421",
            "name": "MUQUIYAUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1090",
            "province_id": "107",
            "ubigeo": "22",
            "code": "120422",
            "name": "PACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1091",
            "province_id": "107",
            "ubigeo": "23",
            "code": "120423",
            "name": "PACCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1092",
            "province_id": "107",
            "ubigeo": "24",
            "code": "120424",
            "name": "PANCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1093",
            "province_id": "107",
            "ubigeo": "25",
            "code": "120425",
            "name": "PARCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1094",
            "province_id": "107",
            "ubigeo": "26",
            "code": "120426",
            "name": "POMACANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1095",
            "province_id": "107",
            "ubigeo": "27",
            "code": "120427",
            "name": "RICRAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1096",
            "province_id": "107",
            "ubigeo": "28",
            "code": "120428",
            "name": "SAN LORENZO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1097",
            "province_id": "107",
            "ubigeo": "29",
            "code": "120429",
            "name": "SAN PEDRO DE CHUNAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1098",
            "province_id": "107",
            "ubigeo": "30",
            "code": "120430",
            "name": "SAUSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1099",
            "province_id": "107",
            "ubigeo": "31",
            "code": "120431",
            "name": "SINCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1100",
            "province_id": "107",
            "ubigeo": "32",
            "code": "120432",
            "name": "TUNAN MARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1101",
            "province_id": "107",
            "ubigeo": "33",
            "code": "120433",
            "name": "YAULI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1102",
            "province_id": "107",
            "ubigeo": "34",
            "code": "120434",
            "name": "YAUYOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1103",
            "province_id": "108",
            "ubigeo": "01",
            "code": "120501",
            "name": "JUNIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1104",
            "province_id": "108",
            "ubigeo": "02",
            "code": "120502",
            "name": "CARHUAMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:27"
        },
        {
            "id": "1105",
            "province_id": "108",
            "ubigeo": "03",
            "code": "120503",
            "name": "ONDORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1106",
            "province_id": "108",
            "ubigeo": "04",
            "code": "120504",
            "name": "ULCUMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1107",
            "province_id": "109",
            "ubigeo": "01",
            "code": "120601",
            "name": "SATIPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1108",
            "province_id": "109",
            "ubigeo": "02",
            "code": "120602",
            "name": "COVIRIALI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1109",
            "province_id": "109",
            "ubigeo": "03",
            "code": "120603",
            "name": "LLAYLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1110",
            "province_id": "109",
            "ubigeo": "04",
            "code": "120604",
            "name": "MAZAMARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1111",
            "province_id": "109",
            "ubigeo": "05",
            "code": "120605",
            "name": "PAMPA HERMOSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1112",
            "province_id": "109",
            "ubigeo": "06",
            "code": "120606",
            "name": "PANGOA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1113",
            "province_id": "109",
            "ubigeo": "07",
            "code": "120607",
            "name": "RÍO NEGRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1114",
            "province_id": "109",
            "ubigeo": "08",
            "code": "120608",
            "name": "RÍO TAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1115",
            "province_id": "109",
            "ubigeo": "09",
            "code": "120609",
            "name": "VIZCATAN DEL ENE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1116",
            "province_id": "110",
            "ubigeo": "01",
            "code": "120701",
            "name": "TARMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1117",
            "province_id": "110",
            "ubigeo": "02",
            "code": "120702",
            "name": "ACOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1118",
            "province_id": "110",
            "ubigeo": "03",
            "code": "120703",
            "name": "HUARICOLCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1119",
            "province_id": "110",
            "ubigeo": "04",
            "code": "120704",
            "name": "HUASAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1120",
            "province_id": "110",
            "ubigeo": "05",
            "code": "120705",
            "name": "LA UNIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1121",
            "province_id": "110",
            "ubigeo": "06",
            "code": "120706",
            "name": "PALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1122",
            "province_id": "110",
            "ubigeo": "07",
            "code": "120707",
            "name": "PALCAMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1123",
            "province_id": "110",
            "ubigeo": "08",
            "code": "120708",
            "name": "SAN PEDRO DE CAJAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1124",
            "province_id": "110",
            "ubigeo": "09",
            "code": "120709",
            "name": "TAPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1125",
            "province_id": "111",
            "ubigeo": "01",
            "code": "120801",
            "name": "LA OROYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1126",
            "province_id": "111",
            "ubigeo": "02",
            "code": "120802",
            "name": "CHACAPALPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1127",
            "province_id": "111",
            "ubigeo": "03",
            "code": "120803",
            "name": "HUAY-HUAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1128",
            "province_id": "111",
            "ubigeo": "04",
            "code": "120804",
            "name": "MARCAPOMACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1129",
            "province_id": "111",
            "ubigeo": "05",
            "code": "120805",
            "name": "MOROCOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1130",
            "province_id": "111",
            "ubigeo": "06",
            "code": "120806",
            "name": "PACCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1131",
            "province_id": "111",
            "ubigeo": "07",
            "code": "120807",
            "name": "SANTA BÁRBARA DE CARHUACAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1132",
            "province_id": "111",
            "ubigeo": "08",
            "code": "120808",
            "name": "SANTA ROSA DE SACCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1133",
            "province_id": "111",
            "ubigeo": "09",
            "code": "120809",
            "name": "SUITUCANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1134",
            "province_id": "111",
            "ubigeo": "10",
            "code": "120810",
            "name": "YAULI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1135",
            "province_id": "112",
            "ubigeo": "01",
            "code": "120901",
            "name": "CHUPACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1136",
            "province_id": "112",
            "ubigeo": "02",
            "code": "120902",
            "name": "AHUAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1137",
            "province_id": "112",
            "ubigeo": "03",
            "code": "120903",
            "name": "CHONGOS BAJO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1138",
            "province_id": "112",
            "ubigeo": "04",
            "code": "120904",
            "name": "HUACHAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1139",
            "province_id": "112",
            "ubigeo": "05",
            "code": "120905",
            "name": "HUAMANCACA CHICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1140",
            "province_id": "112",
            "ubigeo": "06",
            "code": "120906",
            "name": "SAN JUAN DE ISCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1141",
            "province_id": "112",
            "ubigeo": "07",
            "code": "120907",
            "name": "SAN JUAN DE JARPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1142",
            "province_id": "112",
            "ubigeo": "08",
            "code": "120908",
            "name": "TRES DE DICIEMBRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1143",
            "province_id": "112",
            "ubigeo": "09",
            "code": "120909",
            "name": "YANACANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1144",
            "province_id": "113",
            "ubigeo": "01",
            "code": "130101",
            "name": "TRUJILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1145",
            "province_id": "113",
            "ubigeo": "02",
            "code": "130102",
            "name": "EL PORVENIR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1146",
            "province_id": "113",
            "ubigeo": "03",
            "code": "130103",
            "name": "FLORENCIA DE MORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1147",
            "province_id": "113",
            "ubigeo": "04",
            "code": "130104",
            "name": "HUANCHACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1148",
            "province_id": "113",
            "ubigeo": "05",
            "code": "130105",
            "name": "LA ESPERANZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1149",
            "province_id": "113",
            "ubigeo": "06",
            "code": "130106",
            "name": "LAREDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1150",
            "province_id": "113",
            "ubigeo": "07",
            "code": "130107",
            "name": "MOCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1151",
            "province_id": "113",
            "ubigeo": "08",
            "code": "130108",
            "name": "POROTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1152",
            "province_id": "113",
            "ubigeo": "09",
            "code": "130109",
            "name": "SALAVERRY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1153",
            "province_id": "113",
            "ubigeo": "10",
            "code": "130110",
            "name": "SIMBAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1154",
            "province_id": "113",
            "ubigeo": "11",
            "code": "130111",
            "name": "VICTOR LARCO HERRERA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1155",
            "province_id": "114",
            "ubigeo": "01",
            "code": "130201",
            "name": "ASCOPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1156",
            "province_id": "114",
            "ubigeo": "02",
            "code": "130202",
            "name": "CHICAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1157",
            "province_id": "114",
            "ubigeo": "03",
            "code": "130203",
            "name": "CHOCOPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1158",
            "province_id": "114",
            "ubigeo": "04",
            "code": "130204",
            "name": "MAGDALENA DE CAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1159",
            "province_id": "114",
            "ubigeo": "05",
            "code": "130205",
            "name": "PAIJAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1160",
            "province_id": "114",
            "ubigeo": "06",
            "code": "130206",
            "name": "RÁZURI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1161",
            "province_id": "114",
            "ubigeo": "07",
            "code": "130207",
            "name": "SANTIAGO DE CAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1162",
            "province_id": "114",
            "ubigeo": "08",
            "code": "130208",
            "name": "CASA GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1163",
            "province_id": "115",
            "ubigeo": "01",
            "code": "130301",
            "name": "BOLÍVAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1164",
            "province_id": "115",
            "ubigeo": "02",
            "code": "130302",
            "name": "BAMBAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1165",
            "province_id": "115",
            "ubigeo": "03",
            "code": "130303",
            "name": "CONDORMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1166",
            "province_id": "115",
            "ubigeo": "04",
            "code": "130304",
            "name": "LONGOTEA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1167",
            "province_id": "115",
            "ubigeo": "05",
            "code": "130305",
            "name": "UCHUMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1168",
            "province_id": "115",
            "ubigeo": "06",
            "code": "130306",
            "name": "UCUNCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1169",
            "province_id": "116",
            "ubigeo": "01",
            "code": "130401",
            "name": "CHEPEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1170",
            "province_id": "116",
            "ubigeo": "02",
            "code": "130402",
            "name": "PACANGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1171",
            "province_id": "116",
            "ubigeo": "03",
            "code": "130403",
            "name": "PUEBLO NUEVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1172",
            "province_id": "117",
            "ubigeo": "01",
            "code": "130501",
            "name": "JULCAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1173",
            "province_id": "117",
            "ubigeo": "02",
            "code": "130502",
            "name": "CALAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1174",
            "province_id": "117",
            "ubigeo": "03",
            "code": "130503",
            "name": "CARABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1175",
            "province_id": "117",
            "ubigeo": "04",
            "code": "130504",
            "name": "HUASO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1176",
            "province_id": "118",
            "ubigeo": "01",
            "code": "130601",
            "name": "OTUZCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1177",
            "province_id": "118",
            "ubigeo": "02",
            "code": "130602",
            "name": "AGALLPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1178",
            "province_id": "118",
            "ubigeo": "04",
            "code": "130604",
            "name": "CHARAT",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1179",
            "province_id": "118",
            "ubigeo": "05",
            "code": "130605",
            "name": "HUARANCHAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1180",
            "province_id": "118",
            "ubigeo": "06",
            "code": "130606",
            "name": "LA CUESTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1181",
            "province_id": "118",
            "ubigeo": "08",
            "code": "130608",
            "name": "MACHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1182",
            "province_id": "118",
            "ubigeo": "10",
            "code": "130610",
            "name": "PARANDAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1183",
            "province_id": "118",
            "ubigeo": "11",
            "code": "130611",
            "name": "SALPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1184",
            "province_id": "118",
            "ubigeo": "13",
            "code": "130613",
            "name": "SINSICAP",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1185",
            "province_id": "118",
            "ubigeo": "14",
            "code": "130614",
            "name": "USQUIL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1186",
            "province_id": "119",
            "ubigeo": "01",
            "code": "130701",
            "name": "SAN PEDRO DE LLOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1187",
            "province_id": "119",
            "ubigeo": "02",
            "code": "130702",
            "name": "GUADALUPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1188",
            "province_id": "119",
            "ubigeo": "03",
            "code": "130703",
            "name": "JEQUETEPEQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1189",
            "province_id": "119",
            "ubigeo": "04",
            "code": "130704",
            "name": "PACASMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1190",
            "province_id": "119",
            "ubigeo": "05",
            "code": "130705",
            "name": "SAN JOSÉ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1191",
            "province_id": "120",
            "ubigeo": "01",
            "code": "130801",
            "name": "TAYABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1192",
            "province_id": "120",
            "ubigeo": "02",
            "code": "130802",
            "name": "BULDIBUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1193",
            "province_id": "120",
            "ubigeo": "03",
            "code": "130803",
            "name": "CHILLIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1194",
            "province_id": "120",
            "ubigeo": "04",
            "code": "130804",
            "name": "HUANCASPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1195",
            "province_id": "120",
            "ubigeo": "05",
            "code": "130805",
            "name": "HUAYLILLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1196",
            "province_id": "120",
            "ubigeo": "06",
            "code": "130806",
            "name": "HUAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1197",
            "province_id": "120",
            "ubigeo": "07",
            "code": "130807",
            "name": "ONGON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1198",
            "province_id": "120",
            "ubigeo": "08",
            "code": "130808",
            "name": "PARCOY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1199",
            "province_id": "120",
            "ubigeo": "09",
            "code": "130809",
            "name": "PATAZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1200",
            "province_id": "120",
            "ubigeo": "10",
            "code": "130810",
            "name": "PIAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1201",
            "province_id": "120",
            "ubigeo": "11",
            "code": "130811",
            "name": "SANTIAGO DE CHALLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1202",
            "province_id": "120",
            "ubigeo": "12",
            "code": "130812",
            "name": "TAURIJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1203",
            "province_id": "120",
            "ubigeo": "13",
            "code": "130813",
            "name": "URPAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1204",
            "province_id": "121",
            "ubigeo": "01",
            "code": "130901",
            "name": "HUAMACHUCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1205",
            "province_id": "121",
            "ubigeo": "02",
            "code": "130902",
            "name": "CHUGAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1206",
            "province_id": "121",
            "ubigeo": "03",
            "code": "130903",
            "name": "COCHORCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1207",
            "province_id": "121",
            "ubigeo": "04",
            "code": "130904",
            "name": "CURGOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1208",
            "province_id": "121",
            "ubigeo": "05",
            "code": "130905",
            "name": "MARCABAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1209",
            "province_id": "121",
            "ubigeo": "06",
            "code": "130906",
            "name": "SANAGORAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1210",
            "province_id": "121",
            "ubigeo": "07",
            "code": "130907",
            "name": "SARIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1211",
            "province_id": "121",
            "ubigeo": "08",
            "code": "130908",
            "name": "SARTIMBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1212",
            "province_id": "122",
            "ubigeo": "01",
            "code": "131001",
            "name": "SANTIAGO DE CHUCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1213",
            "province_id": "122",
            "ubigeo": "02",
            "code": "131002",
            "name": "ANGASMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1214",
            "province_id": "122",
            "ubigeo": "03",
            "code": "131003",
            "name": "CACHICADAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1215",
            "province_id": "122",
            "ubigeo": "04",
            "code": "131004",
            "name": "MOLLEBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1216",
            "province_id": "122",
            "ubigeo": "05",
            "code": "131005",
            "name": "MOLLEPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1217",
            "province_id": "122",
            "ubigeo": "06",
            "code": "131006",
            "name": "QUIRUVILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1218",
            "province_id": "122",
            "ubigeo": "07",
            "code": "131007",
            "name": "SANTA CRUZ DE CHUCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1219",
            "province_id": "122",
            "ubigeo": "08",
            "code": "131008",
            "name": "SITABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1220",
            "province_id": "123",
            "ubigeo": "01",
            "code": "131101",
            "name": "CASCAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1221",
            "province_id": "123",
            "ubigeo": "02",
            "code": "131102",
            "name": "LUCMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1222",
            "province_id": "123",
            "ubigeo": "03",
            "code": "131103",
            "name": "MARMOT",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1223",
            "province_id": "123",
            "ubigeo": "04",
            "code": "131104",
            "name": "SAYAPULLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1224",
            "province_id": "124",
            "ubigeo": "01",
            "code": "131201",
            "name": "VIRU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1225",
            "province_id": "124",
            "ubigeo": "02",
            "code": "131202",
            "name": "CHAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1226",
            "province_id": "124",
            "ubigeo": "03",
            "code": "131203",
            "name": "GUADALUPITO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1227",
            "province_id": "125",
            "ubigeo": "01",
            "code": "140101",
            "name": "CHICLAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1228",
            "province_id": "125",
            "ubigeo": "02",
            "code": "140102",
            "name": "CHONGOYAPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1229",
            "province_id": "125",
            "ubigeo": "03",
            "code": "140103",
            "name": "ETEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1230",
            "province_id": "125",
            "ubigeo": "04",
            "code": "140104",
            "name": "ETEN PUERTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1231",
            "province_id": "125",
            "ubigeo": "05",
            "code": "140105",
            "name": "JOSÉ LEONARDO ORTIZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1232",
            "province_id": "125",
            "ubigeo": "06",
            "code": "140106",
            "name": "LA VICTORIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1233",
            "province_id": "125",
            "ubigeo": "07",
            "code": "140107",
            "name": "LAGUNAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1234",
            "province_id": "125",
            "ubigeo": "08",
            "code": "140108",
            "name": "MONSEFU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1235",
            "province_id": "125",
            "ubigeo": "09",
            "code": "140109",
            "name": "NUEVA ARICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1236",
            "province_id": "125",
            "ubigeo": "10",
            "code": "140110",
            "name": "OYOTUN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1237",
            "province_id": "125",
            "ubigeo": "11",
            "code": "140111",
            "name": "PICSI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1238",
            "province_id": "125",
            "ubigeo": "12",
            "code": "140112",
            "name": "PIMENTEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1239",
            "province_id": "125",
            "ubigeo": "13",
            "code": "140113",
            "name": "REQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1240",
            "province_id": "125",
            "ubigeo": "14",
            "code": "140114",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1241",
            "province_id": "125",
            "ubigeo": "15",
            "code": "140115",
            "name": "SAÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1242",
            "province_id": "125",
            "ubigeo": "16",
            "code": "140116",
            "name": "CAYALTI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1243",
            "province_id": "125",
            "ubigeo": "17",
            "code": "140117",
            "name": "PATAPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1244",
            "province_id": "125",
            "ubigeo": "18",
            "code": "140118",
            "name": "POMALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1245",
            "province_id": "125",
            "ubigeo": "19",
            "code": "140119",
            "name": "PUCALA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1246",
            "province_id": "125",
            "ubigeo": "20",
            "code": "140120",
            "name": "TUMAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1247",
            "province_id": "126",
            "ubigeo": "01",
            "code": "140201",
            "name": "FERREÑAFE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1248",
            "province_id": "126",
            "ubigeo": "02",
            "code": "140202",
            "name": "CAÑARIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1249",
            "province_id": "126",
            "ubigeo": "03",
            "code": "140203",
            "name": "INCAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1250",
            "province_id": "126",
            "ubigeo": "04",
            "code": "140204",
            "name": "MANUEL ANTONIO MESONES MURO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1251",
            "province_id": "126",
            "ubigeo": "05",
            "code": "140205",
            "name": "PITIPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1252",
            "province_id": "126",
            "ubigeo": "06",
            "code": "140206",
            "name": "PUEBLO NUEVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1253",
            "province_id": "127",
            "ubigeo": "01",
            "code": "140301",
            "name": "LAMBAYEQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1254",
            "province_id": "127",
            "ubigeo": "02",
            "code": "140302",
            "name": "CHOCHOPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1255",
            "province_id": "127",
            "ubigeo": "03",
            "code": "140303",
            "name": "ILLIMO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1256",
            "province_id": "127",
            "ubigeo": "04",
            "code": "140304",
            "name": "JAYANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1257",
            "province_id": "127",
            "ubigeo": "05",
            "code": "140305",
            "name": "MOCHUMI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1258",
            "province_id": "127",
            "ubigeo": "06",
            "code": "140306",
            "name": "MORROPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1259",
            "province_id": "127",
            "ubigeo": "07",
            "code": "140307",
            "name": "MOTUPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1260",
            "province_id": "127",
            "ubigeo": "08",
            "code": "140308",
            "name": "OLMOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1261",
            "province_id": "127",
            "ubigeo": "09",
            "code": "140309",
            "name": "PACORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1262",
            "province_id": "127",
            "ubigeo": "10",
            "code": "140310",
            "name": "SALAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1263",
            "province_id": "127",
            "ubigeo": "11",
            "code": "140311",
            "name": "SAN JOSÉ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1264",
            "province_id": "127",
            "ubigeo": "12",
            "code": "140312",
            "name": "TUCUME",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1265",
            "province_id": "128",
            "ubigeo": "01",
            "code": "150101",
            "name": "LIMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1266",
            "province_id": "128",
            "ubigeo": "02",
            "code": "150102",
            "name": "ANCÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1267",
            "province_id": "128",
            "ubigeo": "03",
            "code": "150103",
            "name": "ATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1268",
            "province_id": "128",
            "ubigeo": "04",
            "code": "150104",
            "name": "BARRANCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1269",
            "province_id": "128",
            "ubigeo": "05",
            "code": "150105",
            "name": "BREÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1270",
            "province_id": "128",
            "ubigeo": "06",
            "code": "150106",
            "name": "CARABAYLLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1271",
            "province_id": "128",
            "ubigeo": "07",
            "code": "150107",
            "name": "CHACLACAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1272",
            "province_id": "128",
            "ubigeo": "08",
            "code": "150108",
            "name": "CHORRILLOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1273",
            "province_id": "128",
            "ubigeo": "09",
            "code": "150109",
            "name": "CIENEGUILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1274",
            "province_id": "128",
            "ubigeo": "10",
            "code": "150110",
            "name": "COMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1275",
            "province_id": "128",
            "ubigeo": "11",
            "code": "150111",
            "name": "EL AGUSTINO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1276",
            "province_id": "128",
            "ubigeo": "12",
            "code": "150112",
            "name": "INDEPENDENCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1277",
            "province_id": "128",
            "ubigeo": "13",
            "code": "150113",
            "name": "JESÚS MARÍA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1278",
            "province_id": "128",
            "ubigeo": "14",
            "code": "150114",
            "name": "LA MOLINA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1279",
            "province_id": "128",
            "ubigeo": "15",
            "code": "150115",
            "name": "LA VICTORIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1280",
            "province_id": "128",
            "ubigeo": "16",
            "code": "150116",
            "name": "LINCE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1281",
            "province_id": "128",
            "ubigeo": "17",
            "code": "150117",
            "name": "LOS OLIVOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1282",
            "province_id": "128",
            "ubigeo": "18",
            "code": "150118",
            "name": "LURIGANCHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1283",
            "province_id": "128",
            "ubigeo": "19",
            "code": "150119",
            "name": "LURIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1284",
            "province_id": "128",
            "ubigeo": "20",
            "code": "150120",
            "name": "MAGDALENA DEL MAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1285",
            "province_id": "128",
            "ubigeo": "21",
            "code": "150121",
            "name": "PUEBLO LIBRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1286",
            "province_id": "128",
            "ubigeo": "22",
            "code": "150122",
            "name": "MIRAFLORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1287",
            "province_id": "128",
            "ubigeo": "23",
            "code": "150123",
            "name": "PACHACAMAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1288",
            "province_id": "128",
            "ubigeo": "24",
            "code": "150124",
            "name": "PUCUSANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1289",
            "province_id": "128",
            "ubigeo": "25",
            "code": "150125",
            "name": "PUENTE PIEDRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1290",
            "province_id": "128",
            "ubigeo": "26",
            "code": "150126",
            "name": "PUNTA HERMOSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1291",
            "province_id": "128",
            "ubigeo": "27",
            "code": "150127",
            "name": "PUNTA NEGRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1292",
            "province_id": "128",
            "ubigeo": "28",
            "code": "150128",
            "name": "RÍMAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1293",
            "province_id": "128",
            "ubigeo": "29",
            "code": "150129",
            "name": "SAN BARTOLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1294",
            "province_id": "128",
            "ubigeo": "30",
            "code": "150130",
            "name": "SAN BORJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1295",
            "province_id": "128",
            "ubigeo": "31",
            "code": "150131",
            "name": "SAN ISIDRO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1296",
            "province_id": "128",
            "ubigeo": "32",
            "code": "150132",
            "name": "SAN JUAN DE LURIGANCHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1297",
            "province_id": "128",
            "ubigeo": "33",
            "code": "150133",
            "name": "SAN JUAN DE MIRAFLORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1298",
            "province_id": "128",
            "ubigeo": "34",
            "code": "150134",
            "name": "SAN LUIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1299",
            "province_id": "128",
            "ubigeo": "35",
            "code": "150135",
            "name": "SAN MARTÍN DE PORRES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1300",
            "province_id": "128",
            "ubigeo": "36",
            "code": "150136",
            "name": "SAN MIGUEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1301",
            "province_id": "128",
            "ubigeo": "37",
            "code": "150137",
            "name": "SANTA ANITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1302",
            "province_id": "128",
            "ubigeo": "38",
            "code": "150138",
            "name": "SANTA MARÍA DEL MAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1303",
            "province_id": "128",
            "ubigeo": "39",
            "code": "150139",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1304",
            "province_id": "128",
            "ubigeo": "40",
            "code": "150140",
            "name": "SANTIAGO DE SURCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1305",
            "province_id": "128",
            "ubigeo": "41",
            "code": "150141",
            "name": "SURQUILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1306",
            "province_id": "128",
            "ubigeo": "42",
            "code": "150142",
            "name": "VILLA EL SALVADOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1307",
            "province_id": "128",
            "ubigeo": "43",
            "code": "150143",
            "name": "VILLA MARÍA DEL TRIUNFO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1308",
            "province_id": "129",
            "ubigeo": "01",
            "code": "150201",
            "name": "BARRANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1309",
            "province_id": "129",
            "ubigeo": "02",
            "code": "150202",
            "name": "PARAMONGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1310",
            "province_id": "129",
            "ubigeo": "03",
            "code": "150203",
            "name": "PATIVILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1311",
            "province_id": "129",
            "ubigeo": "04",
            "code": "150204",
            "name": "SUPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1312",
            "province_id": "129",
            "ubigeo": "05",
            "code": "150205",
            "name": "SUPE PUERTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1313",
            "province_id": "130",
            "ubigeo": "01",
            "code": "150301",
            "name": "CAJATAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1314",
            "province_id": "130",
            "ubigeo": "02",
            "code": "150302",
            "name": "COPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1315",
            "province_id": "130",
            "ubigeo": "03",
            "code": "150303",
            "name": "GORGOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1316",
            "province_id": "130",
            "ubigeo": "04",
            "code": "150304",
            "name": "HUANCAPON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1317",
            "province_id": "130",
            "ubigeo": "05",
            "code": "150305",
            "name": "MANAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1318",
            "province_id": "131",
            "ubigeo": "01",
            "code": "150401",
            "name": "CANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1319",
            "province_id": "131",
            "ubigeo": "02",
            "code": "150402",
            "name": "ARAHUAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1320",
            "province_id": "131",
            "ubigeo": "03",
            "code": "150403",
            "name": "HUAMANTANGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1321",
            "province_id": "131",
            "ubigeo": "04",
            "code": "150404",
            "name": "HUAROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1322",
            "province_id": "131",
            "ubigeo": "05",
            "code": "150405",
            "name": "LACHAQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1323",
            "province_id": "131",
            "ubigeo": "06",
            "code": "150406",
            "name": "SAN BUENAVENTURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1324",
            "province_id": "131",
            "ubigeo": "07",
            "code": "150407",
            "name": "SANTA ROSA DE QUIVES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1325",
            "province_id": "132",
            "ubigeo": "01",
            "code": "150501",
            "name": "SAN VICENTE DE CAÑETE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1326",
            "province_id": "132",
            "ubigeo": "02",
            "code": "150502",
            "name": "ASIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1327",
            "province_id": "132",
            "ubigeo": "03",
            "code": "150503",
            "name": "CALANGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1328",
            "province_id": "132",
            "ubigeo": "04",
            "code": "150504",
            "name": "CERRO AZUL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1329",
            "province_id": "132",
            "ubigeo": "05",
            "code": "150505",
            "name": "CHILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1330",
            "province_id": "132",
            "ubigeo": "06",
            "code": "150506",
            "name": "COAYLLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1331",
            "province_id": "132",
            "ubigeo": "07",
            "code": "150507",
            "name": "IMPERIAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1332",
            "province_id": "132",
            "ubigeo": "08",
            "code": "150508",
            "name": "LUNAHUANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1333",
            "province_id": "132",
            "ubigeo": "09",
            "code": "150509",
            "name": "MALA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1334",
            "province_id": "132",
            "ubigeo": "10",
            "code": "150510",
            "name": "NUEVO IMPERIAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1335",
            "province_id": "132",
            "ubigeo": "11",
            "code": "150511",
            "name": "PACARAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1336",
            "province_id": "132",
            "ubigeo": "12",
            "code": "150512",
            "name": "QUILMANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1337",
            "province_id": "132",
            "ubigeo": "13",
            "code": "150513",
            "name": "SAN ANTONIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1338",
            "province_id": "132",
            "ubigeo": "14",
            "code": "150514",
            "name": "SAN LUIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1339",
            "province_id": "132",
            "ubigeo": "15",
            "code": "150515",
            "name": "SANTA CRUZ DE FLORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1340",
            "province_id": "132",
            "ubigeo": "16",
            "code": "150516",
            "name": "ZÚÑIGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1341",
            "province_id": "133",
            "ubigeo": "01",
            "code": "150601",
            "name": "HUARAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1342",
            "province_id": "133",
            "ubigeo": "02",
            "code": "150602",
            "name": "ATAVILLOS ALTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1343",
            "province_id": "133",
            "ubigeo": "03",
            "code": "150603",
            "name": "ATAVILLOS BAJO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1344",
            "province_id": "133",
            "ubigeo": "04",
            "code": "150604",
            "name": "AUCALLAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1345",
            "province_id": "133",
            "ubigeo": "05",
            "code": "150605",
            "name": "CHANCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1346",
            "province_id": "133",
            "ubigeo": "06",
            "code": "150606",
            "name": "IHUARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1347",
            "province_id": "133",
            "ubigeo": "07",
            "code": "150607",
            "name": "LAMPIAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1348",
            "province_id": "133",
            "ubigeo": "08",
            "code": "150608",
            "name": "PACARAOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1349",
            "province_id": "133",
            "ubigeo": "09",
            "code": "150609",
            "name": "SAN MIGUEL DE ACOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1350",
            "province_id": "133",
            "ubigeo": "10",
            "code": "150610",
            "name": "SANTA CRUZ DE ANDAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1351",
            "province_id": "133",
            "ubigeo": "11",
            "code": "150611",
            "name": "SUMBILCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1352",
            "province_id": "133",
            "ubigeo": "12",
            "code": "150612",
            "name": "VEINTISIETE DE NOVIEMBRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1353",
            "province_id": "134",
            "ubigeo": "01",
            "code": "150701",
            "name": "MATUCANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1354",
            "province_id": "134",
            "ubigeo": "02",
            "code": "150702",
            "name": "ANTIOQUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1355",
            "province_id": "134",
            "ubigeo": "03",
            "code": "150703",
            "name": "CALLAHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1356",
            "province_id": "134",
            "ubigeo": "04",
            "code": "150704",
            "name": "CARAMPOMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1357",
            "province_id": "134",
            "ubigeo": "05",
            "code": "150705",
            "name": "CHICLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1358",
            "province_id": "134",
            "ubigeo": "06",
            "code": "150706",
            "name": "CUENCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1359",
            "province_id": "134",
            "ubigeo": "07",
            "code": "150707",
            "name": "HUACHUPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1360",
            "province_id": "134",
            "ubigeo": "08",
            "code": "150708",
            "name": "HUANZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1361",
            "province_id": "134",
            "ubigeo": "09",
            "code": "150709",
            "name": "HUAROCHIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1362",
            "province_id": "134",
            "ubigeo": "10",
            "code": "150710",
            "name": "LAHUAYTAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1363",
            "province_id": "134",
            "ubigeo": "11",
            "code": "150711",
            "name": "LANGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1364",
            "province_id": "134",
            "ubigeo": "12",
            "code": "150712",
            "name": "LARAOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1365",
            "province_id": "134",
            "ubigeo": "13",
            "code": "150713",
            "name": "MARIATANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1366",
            "province_id": "134",
            "ubigeo": "14",
            "code": "150714",
            "name": "RICARDO PALMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1367",
            "province_id": "134",
            "ubigeo": "15",
            "code": "150715",
            "name": "SAN ANDRÉS DE TUPICOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1368",
            "province_id": "134",
            "ubigeo": "16",
            "code": "150716",
            "name": "SAN ANTONIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1369",
            "province_id": "134",
            "ubigeo": "17",
            "code": "150717",
            "name": "SAN BARTOLOMÉ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1370",
            "province_id": "134",
            "ubigeo": "18",
            "code": "150718",
            "name": "SAN DAMIAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1371",
            "province_id": "134",
            "ubigeo": "19",
            "code": "150719",
            "name": "SAN JUAN DE IRIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1372",
            "province_id": "134",
            "ubigeo": "20",
            "code": "150720",
            "name": "SAN JUAN DE TANTARANCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1373",
            "province_id": "134",
            "ubigeo": "21",
            "code": "150721",
            "name": "SAN LORENZO DE QUINTI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1374",
            "province_id": "134",
            "ubigeo": "22",
            "code": "150722",
            "name": "SAN MATEO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1375",
            "province_id": "134",
            "ubigeo": "23",
            "code": "150723",
            "name": "SAN MATEO DE OTAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1376",
            "province_id": "134",
            "ubigeo": "24",
            "code": "150724",
            "name": "SAN PEDRO DE CASTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1377",
            "province_id": "134",
            "ubigeo": "25",
            "code": "150725",
            "name": "SAN PEDRO DE HUANCAYRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1378",
            "province_id": "134",
            "ubigeo": "26",
            "code": "150726",
            "name": "SANGALLAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1379",
            "province_id": "134",
            "ubigeo": "27",
            "code": "150727",
            "name": "SANTA CRUZ DE COCACHACRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1380",
            "province_id": "134",
            "ubigeo": "28",
            "code": "150728",
            "name": "SANTA EULALIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1381",
            "province_id": "134",
            "ubigeo": "29",
            "code": "150729",
            "name": "SANTIAGO DE ANCHUCAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1382",
            "province_id": "134",
            "ubigeo": "30",
            "code": "150730",
            "name": "SANTIAGO DE TUNA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1383",
            "province_id": "134",
            "ubigeo": "31",
            "code": "150731",
            "name": "SANTO DOMINGO DE LOS OLLEROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1384",
            "province_id": "134",
            "ubigeo": "32",
            "code": "150732",
            "name": "SURCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1385",
            "province_id": "135",
            "ubigeo": "01",
            "code": "150801",
            "name": "HUACHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1386",
            "province_id": "135",
            "ubigeo": "02",
            "code": "150802",
            "name": "AMBAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1387",
            "province_id": "135",
            "ubigeo": "03",
            "code": "150803",
            "name": "CALETA DE CARQUIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1388",
            "province_id": "135",
            "ubigeo": "04",
            "code": "150804",
            "name": "CHECRAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1389",
            "province_id": "135",
            "ubigeo": "05",
            "code": "150805",
            "name": "HUALMAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1390",
            "province_id": "135",
            "ubigeo": "06",
            "code": "150806",
            "name": "HUAURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1391",
            "province_id": "135",
            "ubigeo": "07",
            "code": "150807",
            "name": "LEONCIO PRADO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1392",
            "province_id": "135",
            "ubigeo": "08",
            "code": "150808",
            "name": "PACCHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1393",
            "province_id": "135",
            "ubigeo": "09",
            "code": "150809",
            "name": "SANTA LEONOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1394",
            "province_id": "135",
            "ubigeo": "10",
            "code": "150810",
            "name": "SANTA MARÍA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1395",
            "province_id": "135",
            "ubigeo": "11",
            "code": "150811",
            "name": "SAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1396",
            "province_id": "135",
            "ubigeo": "12",
            "code": "150812",
            "name": "VEGUETA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1397",
            "province_id": "136",
            "ubigeo": "01",
            "code": "150901",
            "name": "OYON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1398",
            "province_id": "136",
            "ubigeo": "02",
            "code": "150902",
            "name": "ANDAJES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1399",
            "province_id": "136",
            "ubigeo": "03",
            "code": "150903",
            "name": "CAUJUL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1400",
            "province_id": "136",
            "ubigeo": "04",
            "code": "150904",
            "name": "COCHAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1401",
            "province_id": "136",
            "ubigeo": "05",
            "code": "150905",
            "name": "NAVAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1402",
            "province_id": "136",
            "ubigeo": "06",
            "code": "150906",
            "name": "PACHANGARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1403",
            "province_id": "137",
            "ubigeo": "01",
            "code": "151001",
            "name": "YAUYOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1404",
            "province_id": "137",
            "ubigeo": "02",
            "code": "151002",
            "name": "ALIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1405",
            "province_id": "137",
            "ubigeo": "03",
            "code": "151003",
            "name": "ALLAUCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1406",
            "province_id": "137",
            "ubigeo": "04",
            "code": "151004",
            "name": "AYAVIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1407",
            "province_id": "137",
            "ubigeo": "05",
            "code": "151005",
            "name": "AZÁNGARO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1408",
            "province_id": "137",
            "ubigeo": "06",
            "code": "151006",
            "name": "CACRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1409",
            "province_id": "137",
            "ubigeo": "07",
            "code": "151007",
            "name": "CARANIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1410",
            "province_id": "137",
            "ubigeo": "08",
            "code": "151008",
            "name": "CATAHUASI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1411",
            "province_id": "137",
            "ubigeo": "09",
            "code": "151009",
            "name": "CHOCOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1412",
            "province_id": "137",
            "ubigeo": "10",
            "code": "151010",
            "name": "COCHAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1413",
            "province_id": "137",
            "ubigeo": "11",
            "code": "151011",
            "name": "COLONIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1414",
            "province_id": "137",
            "ubigeo": "12",
            "code": "151012",
            "name": "HONGOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1415",
            "province_id": "137",
            "ubigeo": "13",
            "code": "151013",
            "name": "HUAMPARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1416",
            "province_id": "137",
            "ubigeo": "14",
            "code": "151014",
            "name": "HUANCAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1417",
            "province_id": "137",
            "ubigeo": "15",
            "code": "151015",
            "name": "HUANGASCAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1418",
            "province_id": "137",
            "ubigeo": "16",
            "code": "151016",
            "name": "HUANTAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1419",
            "province_id": "137",
            "ubigeo": "17",
            "code": "151017",
            "name": "HUAÑEC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1420",
            "province_id": "137",
            "ubigeo": "18",
            "code": "151018",
            "name": "LARAOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1421",
            "province_id": "137",
            "ubigeo": "19",
            "code": "151019",
            "name": "LINCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1422",
            "province_id": "137",
            "ubigeo": "20",
            "code": "151020",
            "name": "MADEAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1423",
            "province_id": "137",
            "ubigeo": "21",
            "code": "151021",
            "name": "MIRAFLORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1424",
            "province_id": "137",
            "ubigeo": "22",
            "code": "151022",
            "name": "OMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1425",
            "province_id": "137",
            "ubigeo": "23",
            "code": "151023",
            "name": "PUTINZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1426",
            "province_id": "137",
            "ubigeo": "24",
            "code": "151024",
            "name": "QUINCHES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1427",
            "province_id": "137",
            "ubigeo": "25",
            "code": "151025",
            "name": "QUINOCAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1428",
            "province_id": "137",
            "ubigeo": "26",
            "code": "151026",
            "name": "SAN JOAQUÍN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1429",
            "province_id": "137",
            "ubigeo": "27",
            "code": "151027",
            "name": "SAN PEDRO DE PILAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1430",
            "province_id": "137",
            "ubigeo": "28",
            "code": "151028",
            "name": "TANTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1431",
            "province_id": "137",
            "ubigeo": "29",
            "code": "151029",
            "name": "TAURIPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1432",
            "province_id": "137",
            "ubigeo": "30",
            "code": "151030",
            "name": "TOMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1433",
            "province_id": "137",
            "ubigeo": "31",
            "code": "151031",
            "name": "TUPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1434",
            "province_id": "137",
            "ubigeo": "32",
            "code": "151032",
            "name": "VIÑAC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1435",
            "province_id": "137",
            "ubigeo": "33",
            "code": "151033",
            "name": "VITIS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1436",
            "province_id": "138",
            "ubigeo": "01",
            "code": "160101",
            "name": "IQUITOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1437",
            "province_id": "138",
            "ubigeo": "02",
            "code": "160102",
            "name": "ALTO NANAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1438",
            "province_id": "138",
            "ubigeo": "03",
            "code": "160103",
            "name": "FERNANDO LORES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1439",
            "province_id": "138",
            "ubigeo": "04",
            "code": "160104",
            "name": "INDIANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1440",
            "province_id": "138",
            "ubigeo": "05",
            "code": "160105",
            "name": "LAS AMAZONAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1441",
            "province_id": "138",
            "ubigeo": "06",
            "code": "160106",
            "name": "MAZAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1442",
            "province_id": "138",
            "ubigeo": "07",
            "code": "160107",
            "name": "NAPO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1443",
            "province_id": "138",
            "ubigeo": "08",
            "code": "160108",
            "name": "PUNCHANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1444",
            "province_id": "138",
            "ubigeo": "10",
            "code": "160110",
            "name": "TORRES CAUSANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1445",
            "province_id": "138",
            "ubigeo": "12",
            "code": "160112",
            "name": "BELÉN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1446",
            "province_id": "138",
            "ubigeo": "13",
            "code": "160113",
            "name": "SAN JUAN BAUTISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1447",
            "province_id": "139",
            "ubigeo": "01",
            "code": "160201",
            "name": "YURIMAGUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1448",
            "province_id": "139",
            "ubigeo": "02",
            "code": "160202",
            "name": "BALSAPUERTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1449",
            "province_id": "139",
            "ubigeo": "05",
            "code": "160205",
            "name": "JEBEROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1450",
            "province_id": "139",
            "ubigeo": "06",
            "code": "160206",
            "name": "LAGUNAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1451",
            "province_id": "139",
            "ubigeo": "10",
            "code": "160210",
            "name": "SANTA CRUZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1452",
            "province_id": "139",
            "ubigeo": "11",
            "code": "160211",
            "name": "TENIENTE CESAR LÓPEZ ROJAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1453",
            "province_id": "140",
            "ubigeo": "01",
            "code": "160301",
            "name": "NAUTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1454",
            "province_id": "140",
            "ubigeo": "02",
            "code": "160302",
            "name": "PARINARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1455",
            "province_id": "140",
            "ubigeo": "03",
            "code": "160303",
            "name": "TIGRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1456",
            "province_id": "140",
            "ubigeo": "04",
            "code": "160304",
            "name": "TROMPETEROS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1457",
            "province_id": "140",
            "ubigeo": "05",
            "code": "160305",
            "name": "URARINAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1458",
            "province_id": "141",
            "ubigeo": "01",
            "code": "160401",
            "name": "RAMÓN CASTILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1459",
            "province_id": "141",
            "ubigeo": "02",
            "code": "160402",
            "name": "PEBAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1460",
            "province_id": "141",
            "ubigeo": "03",
            "code": "160403",
            "name": "YAVARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1461",
            "province_id": "141",
            "ubigeo": "04",
            "code": "160404",
            "name": "SAN PABLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1462",
            "province_id": "142",
            "ubigeo": "01",
            "code": "160501",
            "name": "REQUENA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1463",
            "province_id": "142",
            "ubigeo": "02",
            "code": "160502",
            "name": "ALTO TAPICHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1464",
            "province_id": "142",
            "ubigeo": "03",
            "code": "160503",
            "name": "CAPELO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1465",
            "province_id": "142",
            "ubigeo": "04",
            "code": "160504",
            "name": "EMILIO SAN MARTÍN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1466",
            "province_id": "142",
            "ubigeo": "05",
            "code": "160505",
            "name": "MAQUIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1467",
            "province_id": "142",
            "ubigeo": "06",
            "code": "160506",
            "name": "PUINAHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1468",
            "province_id": "142",
            "ubigeo": "07",
            "code": "160507",
            "name": "SAQUENA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1469",
            "province_id": "142",
            "ubigeo": "08",
            "code": "160508",
            "name": "SOPLIN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1470",
            "province_id": "142",
            "ubigeo": "09",
            "code": "160509",
            "name": "TAPICHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1471",
            "province_id": "142",
            "ubigeo": "10",
            "code": "160510",
            "name": "JENARO HERRERA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1472",
            "province_id": "142",
            "ubigeo": "11",
            "code": "160511",
            "name": "YAQUERANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1473",
            "province_id": "143",
            "ubigeo": "01",
            "code": "160601",
            "name": "CONTAMANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1474",
            "province_id": "143",
            "ubigeo": "02",
            "code": "160602",
            "name": "INAHUAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1475",
            "province_id": "143",
            "ubigeo": "03",
            "code": "160603",
            "name": "PADRE MÁRQUEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1476",
            "province_id": "143",
            "ubigeo": "04",
            "code": "160604",
            "name": "PAMPA HERMOSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1477",
            "province_id": "143",
            "ubigeo": "05",
            "code": "160605",
            "name": "SARAYACU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1478",
            "province_id": "143",
            "ubigeo": "06",
            "code": "160606",
            "name": "VARGAS GUERRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1479",
            "province_id": "144",
            "ubigeo": "01",
            "code": "160701",
            "name": "BARRANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1480",
            "province_id": "144",
            "ubigeo": "02",
            "code": "160702",
            "name": "CAHUAPANAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1481",
            "province_id": "144",
            "ubigeo": "03",
            "code": "160703",
            "name": "MANSERICHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1482",
            "province_id": "144",
            "ubigeo": "04",
            "code": "160704",
            "name": "MORONA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1483",
            "province_id": "144",
            "ubigeo": "05",
            "code": "160705",
            "name": "PASTAZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1484",
            "province_id": "144",
            "ubigeo": "06",
            "code": "160706",
            "name": "ANDOAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1485",
            "province_id": "145",
            "ubigeo": "01",
            "code": "160801",
            "name": "PUTUMAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1486",
            "province_id": "145",
            "ubigeo": "02",
            "code": "160802",
            "name": "ROSA PANDURO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1487",
            "province_id": "145",
            "ubigeo": "03",
            "code": "160803",
            "name": "TENIENTE MANUEL CLAVERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1488",
            "province_id": "145",
            "ubigeo": "04",
            "code": "160804",
            "name": "YAGUAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1489",
            "province_id": "146",
            "ubigeo": "01",
            "code": "170101",
            "name": "TAMBOPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1490",
            "province_id": "146",
            "ubigeo": "02",
            "code": "170102",
            "name": "INAMBARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1491",
            "province_id": "146",
            "ubigeo": "03",
            "code": "170103",
            "name": "LAS PIEDRAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1492",
            "province_id": "146",
            "ubigeo": "04",
            "code": "170104",
            "name": "LABERINTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1493",
            "province_id": "147",
            "ubigeo": "01",
            "code": "170201",
            "name": "MANU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1494",
            "province_id": "147",
            "ubigeo": "02",
            "code": "170202",
            "name": "FITZCARRALD",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1495",
            "province_id": "147",
            "ubigeo": "03",
            "code": "170203",
            "name": "MADRE DE DIOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1496",
            "province_id": "147",
            "ubigeo": "04",
            "code": "170204",
            "name": "HUEPETUHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1497",
            "province_id": "148",
            "ubigeo": "01",
            "code": "170301",
            "name": "IÑAPARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1498",
            "province_id": "148",
            "ubigeo": "02",
            "code": "170302",
            "name": "IBERIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1499",
            "province_id": "148",
            "ubigeo": "03",
            "code": "170303",
            "name": "TAHUAMANU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1500",
            "province_id": "149",
            "ubigeo": "01",
            "code": "180101",
            "name": "MOQUEGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1501",
            "province_id": "149",
            "ubigeo": "02",
            "code": "180102",
            "name": "CARUMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1502",
            "province_id": "149",
            "ubigeo": "03",
            "code": "180103",
            "name": "CUCHUMBAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1503",
            "province_id": "149",
            "ubigeo": "04",
            "code": "180104",
            "name": "SAMEGUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1504",
            "province_id": "149",
            "ubigeo": "05",
            "code": "180105",
            "name": "SAN CRISTÓBAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1505",
            "province_id": "149",
            "ubigeo": "06",
            "code": "180106",
            "name": "TORATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1506",
            "province_id": "150",
            "ubigeo": "01",
            "code": "180201",
            "name": "OMATE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1507",
            "province_id": "150",
            "ubigeo": "02",
            "code": "180202",
            "name": "CHOJATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1508",
            "province_id": "150",
            "ubigeo": "03",
            "code": "180203",
            "name": "COALAQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1509",
            "province_id": "150",
            "ubigeo": "04",
            "code": "180204",
            "name": "ICHUÑA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1510",
            "province_id": "150",
            "ubigeo": "05",
            "code": "180205",
            "name": "LA CAPILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1511",
            "province_id": "150",
            "ubigeo": "06",
            "code": "180206",
            "name": "LLOQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1512",
            "province_id": "150",
            "ubigeo": "07",
            "code": "180207",
            "name": "MATALAQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1513",
            "province_id": "150",
            "ubigeo": "08",
            "code": "180208",
            "name": "PUQUINA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1514",
            "province_id": "150",
            "ubigeo": "09",
            "code": "180209",
            "name": "QUINISTAQUILLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1515",
            "province_id": "150",
            "ubigeo": "10",
            "code": "180210",
            "name": "UBINAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1516",
            "province_id": "150",
            "ubigeo": "11",
            "code": "180211",
            "name": "YUNGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1517",
            "province_id": "151",
            "ubigeo": "01",
            "code": "180301",
            "name": "ILO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1518",
            "province_id": "151",
            "ubigeo": "02",
            "code": "180302",
            "name": "EL ALGARROBAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1519",
            "province_id": "151",
            "ubigeo": "03",
            "code": "180303",
            "name": "PACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1520",
            "province_id": "152",
            "ubigeo": "01",
            "code": "190101",
            "name": "CHAUPIMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1521",
            "province_id": "152",
            "ubigeo": "02",
            "code": "190102",
            "name": "HUACHON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1522",
            "province_id": "152",
            "ubigeo": "03",
            "code": "190103",
            "name": "HUARIACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1523",
            "province_id": "152",
            "ubigeo": "04",
            "code": "190104",
            "name": "HUAYLLAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1524",
            "province_id": "152",
            "ubigeo": "05",
            "code": "190105",
            "name": "NINACACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1525",
            "province_id": "152",
            "ubigeo": "06",
            "code": "190106",
            "name": "PALLANCHACRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1526",
            "province_id": "152",
            "ubigeo": "07",
            "code": "190107",
            "name": "PAUCARTAMBO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1527",
            "province_id": "152",
            "ubigeo": "08",
            "code": "190108",
            "name": "SAN FRANCISCO DE ASÍS DE YARUSYACAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1528",
            "province_id": "152",
            "ubigeo": "09",
            "code": "190109",
            "name": "SIMON BOLÍVAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1529",
            "province_id": "152",
            "ubigeo": "10",
            "code": "190110",
            "name": "TICLACAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1530",
            "province_id": "152",
            "ubigeo": "11",
            "code": "190111",
            "name": "TINYAHUARCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1531",
            "province_id": "152",
            "ubigeo": "12",
            "code": "190112",
            "name": "VICCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1532",
            "province_id": "152",
            "ubigeo": "13",
            "code": "190113",
            "name": "YANACANCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1533",
            "province_id": "153",
            "ubigeo": "01",
            "code": "190201",
            "name": "YANAHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 16:23:28"
        },
        {
            "id": "1534",
            "province_id": "153",
            "ubigeo": "02",
            "code": "190202",
            "name": "CHACAYAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1535",
            "province_id": "153",
            "ubigeo": "03",
            "code": "190203",
            "name": "GOYLLARISQUIZGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1536",
            "province_id": "153",
            "ubigeo": "04",
            "code": "190204",
            "name": "PAUCAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1537",
            "province_id": "153",
            "ubigeo": "05",
            "code": "190205",
            "name": "SAN PEDRO DE PILLAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1538",
            "province_id": "153",
            "ubigeo": "06",
            "code": "190206",
            "name": "SANTA ANA DE TUSI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1539",
            "province_id": "153",
            "ubigeo": "07",
            "code": "190207",
            "name": "TAPUC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1540",
            "province_id": "153",
            "ubigeo": "08",
            "code": "190208",
            "name": "VILCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1541",
            "province_id": "154",
            "ubigeo": "01",
            "code": "190301",
            "name": "OXAPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1542",
            "province_id": "154",
            "ubigeo": "02",
            "code": "190302",
            "name": "CHONTABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1543",
            "province_id": "154",
            "ubigeo": "03",
            "code": "190303",
            "name": "HUANCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1544",
            "province_id": "154",
            "ubigeo": "04",
            "code": "190304",
            "name": "PALCAZU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1545",
            "province_id": "154",
            "ubigeo": "05",
            "code": "190305",
            "name": "POZUZO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1546",
            "province_id": "154",
            "ubigeo": "06",
            "code": "190306",
            "name": "PUERTO BERMÚDEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1547",
            "province_id": "154",
            "ubigeo": "07",
            "code": "190307",
            "name": "VILLA RICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1548",
            "province_id": "154",
            "ubigeo": "08",
            "code": "190308",
            "name": "CONSTITUCIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1549",
            "province_id": "155",
            "ubigeo": "01",
            "code": "200101",
            "name": "PIURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1550",
            "province_id": "155",
            "ubigeo": "04",
            "code": "200104",
            "name": "CASTILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1551",
            "province_id": "155",
            "ubigeo": "05",
            "code": "200105",
            "name": "ATACAOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1552",
            "province_id": "155",
            "ubigeo": "07",
            "code": "200107",
            "name": "CURA MORI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1553",
            "province_id": "155",
            "ubigeo": "08",
            "code": "200108",
            "name": "EL TALLAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1554",
            "province_id": "155",
            "ubigeo": "09",
            "code": "200109",
            "name": "LA ARENA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1555",
            "province_id": "155",
            "ubigeo": "10",
            "code": "200110",
            "name": "LA UNIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1556",
            "province_id": "155",
            "ubigeo": "11",
            "code": "200111",
            "name": "LAS LOMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1557",
            "province_id": "155",
            "ubigeo": "14",
            "code": "200114",
            "name": "TAMBO GRANDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1558",
            "province_id": "155",
            "ubigeo": "15",
            "code": "200115",
            "name": "VEINTISEIS DE OCTUBRE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1559",
            "province_id": "156",
            "ubigeo": "01",
            "code": "200201",
            "name": "AYABACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1560",
            "province_id": "156",
            "ubigeo": "02",
            "code": "200202",
            "name": "FRIAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1561",
            "province_id": "156",
            "ubigeo": "03",
            "code": "200203",
            "name": "JILILI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1562",
            "province_id": "156",
            "ubigeo": "04",
            "code": "200204",
            "name": "LAGUNAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1563",
            "province_id": "156",
            "ubigeo": "05",
            "code": "200205",
            "name": "MONTERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1564",
            "province_id": "156",
            "ubigeo": "06",
            "code": "200206",
            "name": "PACAIPAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1565",
            "province_id": "156",
            "ubigeo": "07",
            "code": "200207",
            "name": "PAIMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1566",
            "province_id": "156",
            "ubigeo": "08",
            "code": "200208",
            "name": "SAPILLICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1567",
            "province_id": "156",
            "ubigeo": "09",
            "code": "200209",
            "name": "SICCHEZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1568",
            "province_id": "156",
            "ubigeo": "10",
            "code": "200210",
            "name": "SUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1569",
            "province_id": "157",
            "ubigeo": "01",
            "code": "200301",
            "name": "HUANCABAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1570",
            "province_id": "157",
            "ubigeo": "02",
            "code": "200302",
            "name": "CANCHAQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1571",
            "province_id": "157",
            "ubigeo": "03",
            "code": "200303",
            "name": "EL CARMEN DE LA FRONTERA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1572",
            "province_id": "157",
            "ubigeo": "04",
            "code": "200304",
            "name": "HUARMACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1573",
            "province_id": "157",
            "ubigeo": "05",
            "code": "200305",
            "name": "LALAQUIZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1574",
            "province_id": "157",
            "ubigeo": "06",
            "code": "200306",
            "name": "SAN MIGUEL DE EL FAIQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1575",
            "province_id": "157",
            "ubigeo": "07",
            "code": "200307",
            "name": "SONDOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1576",
            "province_id": "157",
            "ubigeo": "08",
            "code": "200308",
            "name": "SONDORILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1577",
            "province_id": "158",
            "ubigeo": "01",
            "code": "200401",
            "name": "CHULUCANAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1578",
            "province_id": "158",
            "ubigeo": "02",
            "code": "200402",
            "name": "BUENOS AIRES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1579",
            "province_id": "158",
            "ubigeo": "03",
            "code": "200403",
            "name": "CHALACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1580",
            "province_id": "158",
            "ubigeo": "04",
            "code": "200404",
            "name": "LA MATANZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1581",
            "province_id": "158",
            "ubigeo": "05",
            "code": "200405",
            "name": "MORROPON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1582",
            "province_id": "158",
            "ubigeo": "06",
            "code": "200406",
            "name": "SALITRAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1583",
            "province_id": "158",
            "ubigeo": "07",
            "code": "200407",
            "name": "SAN JUAN DE BIGOTE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1584",
            "province_id": "158",
            "ubigeo": "08",
            "code": "200408",
            "name": "SANTA CATALINA DE MOSSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1585",
            "province_id": "158",
            "ubigeo": "09",
            "code": "200409",
            "name": "SANTO DOMINGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1586",
            "province_id": "158",
            "ubigeo": "10",
            "code": "200410",
            "name": "YAMANGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1587",
            "province_id": "159",
            "ubigeo": "01",
            "code": "200501",
            "name": "PAITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1588",
            "province_id": "159",
            "ubigeo": "02",
            "code": "200502",
            "name": "AMOTAPE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1589",
            "province_id": "159",
            "ubigeo": "03",
            "code": "200503",
            "name": "ARENAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1590",
            "province_id": "159",
            "ubigeo": "04",
            "code": "200504",
            "name": "COLAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1591",
            "province_id": "159",
            "ubigeo": "05",
            "code": "200505",
            "name": "LA HUACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1592",
            "province_id": "159",
            "ubigeo": "06",
            "code": "200506",
            "name": "TAMARINDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1593",
            "province_id": "159",
            "ubigeo": "07",
            "code": "200507",
            "name": "VICHAYAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1594",
            "province_id": "160",
            "ubigeo": "01",
            "code": "200601",
            "name": "SULLANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1595",
            "province_id": "160",
            "ubigeo": "02",
            "code": "200602",
            "name": "BELLAVISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1596",
            "province_id": "160",
            "ubigeo": "03",
            "code": "200603",
            "name": "IGNACIO ESCUDERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1597",
            "province_id": "160",
            "ubigeo": "04",
            "code": "200604",
            "name": "LANCONES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1598",
            "province_id": "160",
            "ubigeo": "05",
            "code": "200605",
            "name": "MARCAVELICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1599",
            "province_id": "160",
            "ubigeo": "06",
            "code": "200606",
            "name": "MIGUEL CHECA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1600",
            "province_id": "160",
            "ubigeo": "07",
            "code": "200607",
            "name": "QUERECOTILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1601",
            "province_id": "160",
            "ubigeo": "08",
            "code": "200608",
            "name": "SALITRAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1602",
            "province_id": "161",
            "ubigeo": "01",
            "code": "200701",
            "name": "PARIÑAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1603",
            "province_id": "161",
            "ubigeo": "02",
            "code": "200702",
            "name": "EL ALTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1604",
            "province_id": "161",
            "ubigeo": "03",
            "code": "200703",
            "name": "LA BREA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1605",
            "province_id": "161",
            "ubigeo": "04",
            "code": "200704",
            "name": "LOBITOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1606",
            "province_id": "161",
            "ubigeo": "05",
            "code": "200705",
            "name": "LOS ORGANOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1607",
            "province_id": "161",
            "ubigeo": "06",
            "code": "200706",
            "name": "MANCORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1608",
            "province_id": "162",
            "ubigeo": "01",
            "code": "200801",
            "name": "SECHURA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1609",
            "province_id": "162",
            "ubigeo": "02",
            "code": "200802",
            "name": "BELLAVISTA DE LA UNIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1610",
            "province_id": "162",
            "ubigeo": "03",
            "code": "200803",
            "name": "BERNAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1611",
            "province_id": "162",
            "ubigeo": "04",
            "code": "200804",
            "name": "CRISTO NOS VALGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1612",
            "province_id": "162",
            "ubigeo": "05",
            "code": "200805",
            "name": "VICE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1613",
            "province_id": "162",
            "ubigeo": "06",
            "code": "200806",
            "name": "RINCONADA LLICUAR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1614",
            "province_id": "163",
            "ubigeo": "01",
            "code": "210101",
            "name": "PUNO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1615",
            "province_id": "163",
            "ubigeo": "02",
            "code": "210102",
            "name": "ACORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1616",
            "province_id": "163",
            "ubigeo": "03",
            "code": "210103",
            "name": "AMANTANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1617",
            "province_id": "163",
            "ubigeo": "04",
            "code": "210104",
            "name": "ATUNCOLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1618",
            "province_id": "163",
            "ubigeo": "05",
            "code": "210105",
            "name": "CAPACHICA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1619",
            "province_id": "163",
            "ubigeo": "06",
            "code": "210106",
            "name": "CHUCUITO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1620",
            "province_id": "163",
            "ubigeo": "07",
            "code": "210107",
            "name": "COATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1621",
            "province_id": "163",
            "ubigeo": "08",
            "code": "210108",
            "name": "HUATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1622",
            "province_id": "163",
            "ubigeo": "09",
            "code": "210109",
            "name": "MAÑAZO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1623",
            "province_id": "163",
            "ubigeo": "10",
            "code": "210110",
            "name": "PAUCARCOLLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1624",
            "province_id": "163",
            "ubigeo": "11",
            "code": "210111",
            "name": "PICHACANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1625",
            "province_id": "163",
            "ubigeo": "12",
            "code": "210112",
            "name": "PLATERIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1626",
            "province_id": "163",
            "ubigeo": "13",
            "code": "210113",
            "name": "SAN ANTONIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1627",
            "province_id": "163",
            "ubigeo": "14",
            "code": "210114",
            "name": "TIQUILLACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1628",
            "province_id": "163",
            "ubigeo": "15",
            "code": "210115",
            "name": "VILQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1629",
            "province_id": "164",
            "ubigeo": "01",
            "code": "210201",
            "name": "AZÁNGARO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1630",
            "province_id": "164",
            "ubigeo": "02",
            "code": "210202",
            "name": "ACHAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1631",
            "province_id": "164",
            "ubigeo": "03",
            "code": "210203",
            "name": "ARAPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1632",
            "province_id": "164",
            "ubigeo": "04",
            "code": "210204",
            "name": "ASILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1633",
            "province_id": "164",
            "ubigeo": "05",
            "code": "210205",
            "name": "CAMINACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1634",
            "province_id": "164",
            "ubigeo": "06",
            "code": "210206",
            "name": "CHUPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1635",
            "province_id": "164",
            "ubigeo": "07",
            "code": "210207",
            "name": "JOSÉ DOMINGO CHOQUEHUANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1636",
            "province_id": "164",
            "ubigeo": "08",
            "code": "210208",
            "name": "MUÑANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1637",
            "province_id": "164",
            "ubigeo": "09",
            "code": "210209",
            "name": "POTONI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1638",
            "province_id": "164",
            "ubigeo": "10",
            "code": "210210",
            "name": "SAMAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1639",
            "province_id": "164",
            "ubigeo": "11",
            "code": "210211",
            "name": "SAN ANTON",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1640",
            "province_id": "164",
            "ubigeo": "12",
            "code": "210212",
            "name": "SAN JOSÉ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1641",
            "province_id": "164",
            "ubigeo": "13",
            "code": "210213",
            "name": "SAN JUAN DE SALINAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1642",
            "province_id": "164",
            "ubigeo": "14",
            "code": "210214",
            "name": "SANTIAGO DE PUPUJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1643",
            "province_id": "164",
            "ubigeo": "15",
            "code": "210215",
            "name": "TIRAPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1644",
            "province_id": "165",
            "ubigeo": "01",
            "code": "210301",
            "name": "MACUSANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1645",
            "province_id": "165",
            "ubigeo": "02",
            "code": "210302",
            "name": "AJOYANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1646",
            "province_id": "165",
            "ubigeo": "03",
            "code": "210303",
            "name": "AYAPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1647",
            "province_id": "165",
            "ubigeo": "04",
            "code": "210304",
            "name": "COASA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1648",
            "province_id": "165",
            "ubigeo": "05",
            "code": "210305",
            "name": "CORANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1649",
            "province_id": "165",
            "ubigeo": "06",
            "code": "210306",
            "name": "CRUCERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1650",
            "province_id": "165",
            "ubigeo": "07",
            "code": "210307",
            "name": "ITUATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1651",
            "province_id": "165",
            "ubigeo": "08",
            "code": "210308",
            "name": "OLLACHEA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1652",
            "province_id": "165",
            "ubigeo": "09",
            "code": "210309",
            "name": "SAN GABAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1653",
            "province_id": "165",
            "ubigeo": "10",
            "code": "210310",
            "name": "USICAYOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1654",
            "province_id": "166",
            "ubigeo": "01",
            "code": "210401",
            "name": "JULI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1655",
            "province_id": "166",
            "ubigeo": "02",
            "code": "210402",
            "name": "DESAGUADERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1656",
            "province_id": "166",
            "ubigeo": "03",
            "code": "210403",
            "name": "HUACULLANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1657",
            "province_id": "166",
            "ubigeo": "04",
            "code": "210404",
            "name": "KELLUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1658",
            "province_id": "166",
            "ubigeo": "05",
            "code": "210405",
            "name": "PISACOMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1659",
            "province_id": "166",
            "ubigeo": "06",
            "code": "210406",
            "name": "POMATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1660",
            "province_id": "166",
            "ubigeo": "07",
            "code": "210407",
            "name": "ZEPITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1661",
            "province_id": "167",
            "ubigeo": "01",
            "code": "210501",
            "name": "ILAVE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1662",
            "province_id": "167",
            "ubigeo": "02",
            "code": "210502",
            "name": "CAPAZO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1663",
            "province_id": "167",
            "ubigeo": "03",
            "code": "210503",
            "name": "PILCUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1664",
            "province_id": "167",
            "ubigeo": "04",
            "code": "210504",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1665",
            "province_id": "167",
            "ubigeo": "05",
            "code": "210505",
            "name": "CONDURIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1666",
            "province_id": "168",
            "ubigeo": "01",
            "code": "210601",
            "name": "HUANCANE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1667",
            "province_id": "168",
            "ubigeo": "02",
            "code": "210602",
            "name": "COJATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1668",
            "province_id": "168",
            "ubigeo": "03",
            "code": "210603",
            "name": "HUATASANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1669",
            "province_id": "168",
            "ubigeo": "04",
            "code": "210604",
            "name": "INCHUPALLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1670",
            "province_id": "168",
            "ubigeo": "05",
            "code": "210605",
            "name": "PUSI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1671",
            "province_id": "168",
            "ubigeo": "06",
            "code": "210606",
            "name": "ROSASPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1672",
            "province_id": "168",
            "ubigeo": "07",
            "code": "210607",
            "name": "TARACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1673",
            "province_id": "168",
            "ubigeo": "08",
            "code": "210608",
            "name": "VILQUE CHICO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1674",
            "province_id": "169",
            "ubigeo": "01",
            "code": "210701",
            "name": "LAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1675",
            "province_id": "169",
            "ubigeo": "02",
            "code": "210702",
            "name": "CABANILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1676",
            "province_id": "169",
            "ubigeo": "03",
            "code": "210703",
            "name": "CALAPUJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1677",
            "province_id": "169",
            "ubigeo": "04",
            "code": "210704",
            "name": "NICASIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1678",
            "province_id": "169",
            "ubigeo": "05",
            "code": "210705",
            "name": "OCUVIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1679",
            "province_id": "169",
            "ubigeo": "06",
            "code": "210706",
            "name": "PALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1680",
            "province_id": "169",
            "ubigeo": "07",
            "code": "210707",
            "name": "PARATIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1681",
            "province_id": "169",
            "ubigeo": "08",
            "code": "210708",
            "name": "PUCARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1682",
            "province_id": "169",
            "ubigeo": "09",
            "code": "210709",
            "name": "SANTA LUCIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1683",
            "province_id": "169",
            "ubigeo": "10",
            "code": "210710",
            "name": "VILAVILA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1684",
            "province_id": "170",
            "ubigeo": "01",
            "code": "210801",
            "name": "AYAVIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1685",
            "province_id": "170",
            "ubigeo": "02",
            "code": "210802",
            "name": "ANTAUTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1686",
            "province_id": "170",
            "ubigeo": "03",
            "code": "210803",
            "name": "CUPI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1687",
            "province_id": "170",
            "ubigeo": "04",
            "code": "210804",
            "name": "LLALLI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1688",
            "province_id": "170",
            "ubigeo": "05",
            "code": "210805",
            "name": "MACARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1689",
            "province_id": "170",
            "ubigeo": "06",
            "code": "210806",
            "name": "NUÑOA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1690",
            "province_id": "170",
            "ubigeo": "07",
            "code": "210807",
            "name": "ORURILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1691",
            "province_id": "170",
            "ubigeo": "08",
            "code": "210808",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1692",
            "province_id": "170",
            "ubigeo": "09",
            "code": "210809",
            "name": "UMACHIRI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1693",
            "province_id": "171",
            "ubigeo": "01",
            "code": "210901",
            "name": "MOHO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1694",
            "province_id": "171",
            "ubigeo": "02",
            "code": "210902",
            "name": "CONIMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1695",
            "province_id": "171",
            "ubigeo": "03",
            "code": "210903",
            "name": "HUAYRAPATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1696",
            "province_id": "171",
            "ubigeo": "04",
            "code": "210904",
            "name": "TILALI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1697",
            "province_id": "172",
            "ubigeo": "01",
            "code": "211001",
            "name": "PUTINA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1698",
            "province_id": "172",
            "ubigeo": "02",
            "code": "211002",
            "name": "ANANEA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1699",
            "province_id": "172",
            "ubigeo": "03",
            "code": "211003",
            "name": "PEDRO VILCA APAZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1700",
            "province_id": "172",
            "ubigeo": "04",
            "code": "211004",
            "name": "QUILCAPUNCU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1701",
            "province_id": "172",
            "ubigeo": "05",
            "code": "211005",
            "name": "SINA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1702",
            "province_id": "173",
            "ubigeo": "01",
            "code": "211101",
            "name": "JULIACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1703",
            "province_id": "173",
            "ubigeo": "02",
            "code": "211102",
            "name": "CABANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1704",
            "province_id": "173",
            "ubigeo": "03",
            "code": "211103",
            "name": "CABANILLAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1705",
            "province_id": "173",
            "ubigeo": "04",
            "code": "211104",
            "name": "CARACOTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1706",
            "province_id": "174",
            "ubigeo": "01",
            "code": "211201",
            "name": "SANDIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1707",
            "province_id": "174",
            "ubigeo": "02",
            "code": "211202",
            "name": "CUYOCUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1708",
            "province_id": "174",
            "ubigeo": "03",
            "code": "211203",
            "name": "LIMBANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1709",
            "province_id": "174",
            "ubigeo": "04",
            "code": "211204",
            "name": "PATAMBUCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1710",
            "province_id": "174",
            "ubigeo": "05",
            "code": "211205",
            "name": "PHARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1711",
            "province_id": "174",
            "ubigeo": "06",
            "code": "211206",
            "name": "QUIACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1712",
            "province_id": "174",
            "ubigeo": "07",
            "code": "211207",
            "name": "SAN JUAN DEL ORO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1713",
            "province_id": "174",
            "ubigeo": "08",
            "code": "211208",
            "name": "YANAHUAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1714",
            "province_id": "174",
            "ubigeo": "09",
            "code": "211209",
            "name": "ALTO INAMBARI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1715",
            "province_id": "174",
            "ubigeo": "10",
            "code": "211210",
            "name": "SAN PEDRO DE PUTINA PUNCO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1716",
            "province_id": "175",
            "ubigeo": "01",
            "code": "211301",
            "name": "YUNGUYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1717",
            "province_id": "175",
            "ubigeo": "02",
            "code": "211302",
            "name": "ANAPIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1718",
            "province_id": "175",
            "ubigeo": "03",
            "code": "211303",
            "name": "COPANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1719",
            "province_id": "175",
            "ubigeo": "04",
            "code": "211304",
            "name": "CUTURAPI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1720",
            "province_id": "175",
            "ubigeo": "05",
            "code": "211305",
            "name": "OLLARAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1721",
            "province_id": "175",
            "ubigeo": "06",
            "code": "211306",
            "name": "TINICACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1722",
            "province_id": "175",
            "ubigeo": "07",
            "code": "211307",
            "name": "UNICACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1723",
            "province_id": "176",
            "ubigeo": "01",
            "code": "220101",
            "name": "MOYOBAMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1724",
            "province_id": "176",
            "ubigeo": "02",
            "code": "220102",
            "name": "CALZADA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1725",
            "province_id": "176",
            "ubigeo": "03",
            "code": "220103",
            "name": "HABANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1726",
            "province_id": "176",
            "ubigeo": "04",
            "code": "220104",
            "name": "JEPELACIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1727",
            "province_id": "176",
            "ubigeo": "05",
            "code": "220105",
            "name": "SORITOR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1728",
            "province_id": "176",
            "ubigeo": "06",
            "code": "220106",
            "name": "YANTALO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1729",
            "province_id": "177",
            "ubigeo": "01",
            "code": "220201",
            "name": "BELLAVISTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1730",
            "province_id": "177",
            "ubigeo": "02",
            "code": "220202",
            "name": "ALTO BIAVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1731",
            "province_id": "177",
            "ubigeo": "03",
            "code": "220203",
            "name": "BAJO BIAVO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1732",
            "province_id": "177",
            "ubigeo": "04",
            "code": "220204",
            "name": "HUALLAGA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1733",
            "province_id": "177",
            "ubigeo": "05",
            "code": "220205",
            "name": "SAN PABLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1734",
            "province_id": "177",
            "ubigeo": "06",
            "code": "220206",
            "name": "SAN RAFAEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1735",
            "province_id": "178",
            "ubigeo": "01",
            "code": "220301",
            "name": "SAN JOSÉ DE SISA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1736",
            "province_id": "178",
            "ubigeo": "02",
            "code": "220302",
            "name": "AGUA BLANCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1737",
            "province_id": "178",
            "ubigeo": "03",
            "code": "220303",
            "name": "SAN MARTÍN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1738",
            "province_id": "178",
            "ubigeo": "04",
            "code": "220304",
            "name": "SANTA ROSA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1739",
            "province_id": "178",
            "ubigeo": "05",
            "code": "220305",
            "name": "SHATOJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1740",
            "province_id": "179",
            "ubigeo": "01",
            "code": "220401",
            "name": "SAPOSOA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1741",
            "province_id": "179",
            "ubigeo": "02",
            "code": "220402",
            "name": "ALTO SAPOSOA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1742",
            "province_id": "179",
            "ubigeo": "03",
            "code": "220403",
            "name": "EL ESLABÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1743",
            "province_id": "179",
            "ubigeo": "04",
            "code": "220404",
            "name": "PISCOYACU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1744",
            "province_id": "179",
            "ubigeo": "05",
            "code": "220405",
            "name": "SACANCHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1745",
            "province_id": "179",
            "ubigeo": "06",
            "code": "220406",
            "name": "TINGO DE SAPOSOA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1746",
            "province_id": "180",
            "ubigeo": "01",
            "code": "220501",
            "name": "LAMAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1747",
            "province_id": "180",
            "ubigeo": "02",
            "code": "220502",
            "name": "ALONSO DE ALVARADO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1748",
            "province_id": "180",
            "ubigeo": "03",
            "code": "220503",
            "name": "BARRANQUITA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1749",
            "province_id": "180",
            "ubigeo": "04",
            "code": "220504",
            "name": "CAYNARACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1750",
            "province_id": "180",
            "ubigeo": "05",
            "code": "220505",
            "name": "CUÑUMBUQUI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1751",
            "province_id": "180",
            "ubigeo": "06",
            "code": "220506",
            "name": "PINTO RECODO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1752",
            "province_id": "180",
            "ubigeo": "07",
            "code": "220507",
            "name": "RUMISAPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1753",
            "province_id": "180",
            "ubigeo": "08",
            "code": "220508",
            "name": "SAN ROQUE DE CUMBAZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1754",
            "province_id": "180",
            "ubigeo": "09",
            "code": "220509",
            "name": "SHANAO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1755",
            "province_id": "180",
            "ubigeo": "10",
            "code": "220510",
            "name": "TABALOSOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1756",
            "province_id": "180",
            "ubigeo": "11",
            "code": "220511",
            "name": "ZAPATERO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1757",
            "province_id": "181",
            "ubigeo": "01",
            "code": "220601",
            "name": "JUANJUÍ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1758",
            "province_id": "181",
            "ubigeo": "02",
            "code": "220602",
            "name": "CAMPANILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1759",
            "province_id": "181",
            "ubigeo": "03",
            "code": "220603",
            "name": "HUICUNGO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1760",
            "province_id": "181",
            "ubigeo": "04",
            "code": "220604",
            "name": "PACHIZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1761",
            "province_id": "181",
            "ubigeo": "05",
            "code": "220605",
            "name": "PAJARILLO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1762",
            "province_id": "182",
            "ubigeo": "01",
            "code": "220701",
            "name": "PICOTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1763",
            "province_id": "182",
            "ubigeo": "02",
            "code": "220702",
            "name": "BUENOS AIRES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1764",
            "province_id": "182",
            "ubigeo": "03",
            "code": "220703",
            "name": "CASPISAPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1765",
            "province_id": "182",
            "ubigeo": "04",
            "code": "220704",
            "name": "PILLUANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1766",
            "province_id": "182",
            "ubigeo": "05",
            "code": "220705",
            "name": "PUCACACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1767",
            "province_id": "182",
            "ubigeo": "06",
            "code": "220706",
            "name": "SAN CRISTÓBAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1768",
            "province_id": "182",
            "ubigeo": "07",
            "code": "220707",
            "name": "SAN HILARIÓN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1769",
            "province_id": "182",
            "ubigeo": "08",
            "code": "220708",
            "name": "SHAMBOYACU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1770",
            "province_id": "182",
            "ubigeo": "09",
            "code": "220709",
            "name": "TINGO DE PONASA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1771",
            "province_id": "182",
            "ubigeo": "10",
            "code": "220710",
            "name": "TRES UNIDOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1772",
            "province_id": "183",
            "ubigeo": "01",
            "code": "220801",
            "name": "RIOJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1773",
            "province_id": "183",
            "ubigeo": "02",
            "code": "220802",
            "name": "AWAJUN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1774",
            "province_id": "183",
            "ubigeo": "03",
            "code": "220803",
            "name": "ELÍAS SOPLIN VARGAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1775",
            "province_id": "183",
            "ubigeo": "04",
            "code": "220804",
            "name": "NUEVA CAJAMARCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1776",
            "province_id": "183",
            "ubigeo": "05",
            "code": "220805",
            "name": "PARDO MIGUEL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1777",
            "province_id": "183",
            "ubigeo": "06",
            "code": "220806",
            "name": "POSIC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1778",
            "province_id": "183",
            "ubigeo": "07",
            "code": "220807",
            "name": "SAN FERNANDO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1779",
            "province_id": "183",
            "ubigeo": "08",
            "code": "220808",
            "name": "YORONGOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1780",
            "province_id": "183",
            "ubigeo": "09",
            "code": "220809",
            "name": "YURACYACU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1781",
            "province_id": "184",
            "ubigeo": "01",
            "code": "220901",
            "name": "TARAPOTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1782",
            "province_id": "184",
            "ubigeo": "02",
            "code": "220902",
            "name": "ALBERTO LEVEAU",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1783",
            "province_id": "184",
            "ubigeo": "03",
            "code": "220903",
            "name": "CACATACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1784",
            "province_id": "184",
            "ubigeo": "04",
            "code": "220904",
            "name": "CHAZUTA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1785",
            "province_id": "184",
            "ubigeo": "05",
            "code": "220905",
            "name": "CHIPURANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1786",
            "province_id": "184",
            "ubigeo": "06",
            "code": "220906",
            "name": "EL PORVENIR",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1787",
            "province_id": "184",
            "ubigeo": "07",
            "code": "220907",
            "name": "HUIMBAYOC",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1788",
            "province_id": "184",
            "ubigeo": "08",
            "code": "220908",
            "name": "JUAN GUERRA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1789",
            "province_id": "184",
            "ubigeo": "09",
            "code": "220909",
            "name": "LA BANDA DE SHILCAYO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1790",
            "province_id": "184",
            "ubigeo": "10",
            "code": "220910",
            "name": "MORALES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1791",
            "province_id": "184",
            "ubigeo": "11",
            "code": "220911",
            "name": "PAPAPLAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1792",
            "province_id": "184",
            "ubigeo": "12",
            "code": "220912",
            "name": "SAN ANTONIO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1793",
            "province_id": "184",
            "ubigeo": "13",
            "code": "220913",
            "name": "SAUCE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1794",
            "province_id": "184",
            "ubigeo": "14",
            "code": "220914",
            "name": "SHAPAJA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1795",
            "province_id": "185",
            "ubigeo": "01",
            "code": "221001",
            "name": "TOCACHE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1796",
            "province_id": "185",
            "ubigeo": "02",
            "code": "221002",
            "name": "NUEVO PROGRESO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1797",
            "province_id": "185",
            "ubigeo": "03",
            "code": "221003",
            "name": "POLVORA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1798",
            "province_id": "185",
            "ubigeo": "04",
            "code": "221004",
            "name": "SHUNTE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1799",
            "province_id": "185",
            "ubigeo": "05",
            "code": "221005",
            "name": "UCHIZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1800",
            "province_id": "186",
            "ubigeo": "01",
            "code": "230101",
            "name": "TACNA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1801",
            "province_id": "186",
            "ubigeo": "02",
            "code": "230102",
            "name": "ALTO DE LA ALIANZA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1802",
            "province_id": "186",
            "ubigeo": "03",
            "code": "230103",
            "name": "CALANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1803",
            "province_id": "186",
            "ubigeo": "04",
            "code": "230104",
            "name": "CIUDAD NUEVA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1804",
            "province_id": "186",
            "ubigeo": "05",
            "code": "230105",
            "name": "INCLAN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1805",
            "province_id": "186",
            "ubigeo": "06",
            "code": "230106",
            "name": "PACHIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1806",
            "province_id": "186",
            "ubigeo": "07",
            "code": "230107",
            "name": "PALCA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1807",
            "province_id": "186",
            "ubigeo": "08",
            "code": "230108",
            "name": "POCOLLAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1808",
            "province_id": "186",
            "ubigeo": "09",
            "code": "230109",
            "name": "SAMA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1809",
            "province_id": "186",
            "ubigeo": "10",
            "code": "230110",
            "name": "CORONEL GREGORIO ALBARRACÍN LANCHIPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1810",
            "province_id": "187",
            "ubigeo": "01",
            "code": "230201",
            "name": "CANDARAVE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1811",
            "province_id": "187",
            "ubigeo": "02",
            "code": "230202",
            "name": "CAIRANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1812",
            "province_id": "187",
            "ubigeo": "03",
            "code": "230203",
            "name": "CAMILACA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1813",
            "province_id": "187",
            "ubigeo": "04",
            "code": "230204",
            "name": "CURIBAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1814",
            "province_id": "187",
            "ubigeo": "05",
            "code": "230205",
            "name": "HUANUARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1815",
            "province_id": "187",
            "ubigeo": "06",
            "code": "230206",
            "name": "QUILAHUANI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1816",
            "province_id": "188",
            "ubigeo": "01",
            "code": "230301",
            "name": "LOCUMBA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1817",
            "province_id": "188",
            "ubigeo": "02",
            "code": "230302",
            "name": "ILABAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1818",
            "province_id": "188",
            "ubigeo": "03",
            "code": "230303",
            "name": "ITE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1819",
            "province_id": "189",
            "ubigeo": "01",
            "code": "230401",
            "name": "TARATA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1820",
            "province_id": "189",
            "ubigeo": "02",
            "code": "230402",
            "name": "HÉROES ALBARRACÍN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1821",
            "province_id": "189",
            "ubigeo": "03",
            "code": "230403",
            "name": "ESTIQUE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1822",
            "province_id": "189",
            "ubigeo": "04",
            "code": "230404",
            "name": "ESTIQUE-PAMPA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1823",
            "province_id": "189",
            "ubigeo": "05",
            "code": "230405",
            "name": "SITAJARA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1824",
            "province_id": "189",
            "ubigeo": "06",
            "code": "230406",
            "name": "SUSAPAYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1825",
            "province_id": "189",
            "ubigeo": "07",
            "code": "230407",
            "name": "TARUCACHI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1826",
            "province_id": "189",
            "ubigeo": "08",
            "code": "230408",
            "name": "TICACO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1827",
            "province_id": "190",
            "ubigeo": "01",
            "code": "240101",
            "name": "TUMBES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1828",
            "province_id": "190",
            "ubigeo": "02",
            "code": "240102",
            "name": "CORRALES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1829",
            "province_id": "190",
            "ubigeo": "03",
            "code": "240103",
            "name": "LA CRUZ",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1830",
            "province_id": "190",
            "ubigeo": "04",
            "code": "240104",
            "name": "PAMPAS DE HOSPITAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1831",
            "province_id": "190",
            "ubigeo": "05",
            "code": "240105",
            "name": "SAN JACINTO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1832",
            "province_id": "190",
            "ubigeo": "06",
            "code": "240106",
            "name": "SAN JUAN DE LA VIRGEN",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1833",
            "province_id": "191",
            "ubigeo": "01",
            "code": "240201",
            "name": "ZORRITOS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1834",
            "province_id": "191",
            "ubigeo": "02",
            "code": "240202",
            "name": "CASITAS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1835",
            "province_id": "191",
            "ubigeo": "03",
            "code": "240203",
            "name": "CANOAS DE PUNTA SAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1836",
            "province_id": "192",
            "ubigeo": "01",
            "code": "240301",
            "name": "ZARUMILLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1837",
            "province_id": "192",
            "ubigeo": "02",
            "code": "240302",
            "name": "AGUAS VERDES",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1838",
            "province_id": "192",
            "ubigeo": "03",
            "code": "240303",
            "name": "MATAPALO",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1839",
            "province_id": "192",
            "ubigeo": "04",
            "code": "240304",
            "name": "PAPAYAL",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1840",
            "province_id": "193",
            "ubigeo": "01",
            "code": "250101",
            "name": "CALLERIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1841",
            "province_id": "193",
            "ubigeo": "02",
            "code": "250102",
            "name": "CAMPOVERDE",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1842",
            "province_id": "193",
            "ubigeo": "03",
            "code": "250103",
            "name": "IPARIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1843",
            "province_id": "193",
            "ubigeo": "04",
            "code": "250104",
            "name": "MASISEA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1844",
            "province_id": "193",
            "ubigeo": "05",
            "code": "250105",
            "name": "YARINACOCHA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1845",
            "province_id": "193",
            "ubigeo": "06",
            "code": "250106",
            "name": "NUEVA REQUENA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1846",
            "province_id": "193",
            "ubigeo": "07",
            "code": "250107",
            "name": "MANANTAY",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1847",
            "province_id": "194",
            "ubigeo": "01",
            "code": "250201",
            "name": "RAYMONDI",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1848",
            "province_id": "194",
            "ubigeo": "02",
            "code": "250202",
            "name": "SEPAHUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1849",
            "province_id": "194",
            "ubigeo": "03",
            "code": "250203",
            "name": "TAHUANIA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1850",
            "province_id": "194",
            "ubigeo": "04",
            "code": "250204",
            "name": "YURUA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1851",
            "province_id": "195",
            "ubigeo": "01",
            "code": "250301",
            "name": "PADRE ABAD",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1852",
            "province_id": "195",
            "ubigeo": "02",
            "code": "250302",
            "name": "IRAZOLA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1853",
            "province_id": "195",
            "ubigeo": "03",
            "code": "250303",
            "name": "CURIMANA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1854",
            "province_id": "195",
            "ubigeo": "04",
            "code": "250304",
            "name": "NESHUYA",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1855",
            "province_id": "195",
            "ubigeo": "05",
            "code": "250305",
            "name": "ALEXANDER VON HUMBOLDT",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "1856",
            "province_id": "196",
            "ubigeo": "01",
            "code": "250401",
            "name": "PURUS",
            "created_at": "2019-01-21 13:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $districts = json_decode($districts);

        foreach ($districts as $district) {
        	DB::table('admin_districts')->insert([
	            'province_id'	=> $district->province_id,
	            'ubigeo'		=> $district->ubigeo,
	            'code' 			=> $district->code,
	            'name' 			=> $district->name,
	            'created_at'	=> $district->created_at,
	            'updated_at'	=> $district->updated_at
	        ]);
        }
    }
}
