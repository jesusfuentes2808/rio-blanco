<?php

use Illuminate\Database\Seeder;

class UserRolesMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 1
       	]);

       	DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 2
       	]);

       	DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 3
       	]);

       	DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 4
       	]);

       	DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 5
       	]);

       	DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 6
       	]);

       	DB::table('admin_role_menus')->insert([
            'role_id' => 1,
            'menu_id' => 7
       	]);
    }
}
