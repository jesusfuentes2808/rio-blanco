<?php

use Illuminate\Database\Seeder;

class SaleRatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('basic_sale_rates')->insert([
        	'name' => 'Tarifa 1',
        ]);

        DB::table('basic_sale_rates')->insert([
        	'name' => 'Tarifa 2',
        ]);

        DB::table('basic_sale_rates')->insert([
        	'name' => 'Tarifa 3',
        ]);
    }
}
