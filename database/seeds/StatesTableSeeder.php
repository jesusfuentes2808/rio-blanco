<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = '
			[
        {
            "id": "1",
            "country_id": "175",
            "ubigeo": "01",
            "name": "AMAZONAS",
            "iso": "AMA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "country_id": "175",
            "ubigeo": "02",
            "name": "ÁNCASH",
            "iso": "ANC",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "country_id": "175",
            "ubigeo": "03",
            "name": "APURÍMAC",
            "iso": "APU",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "country_id": "175",
            "ubigeo": "04",
            "name": "AREQUIPA",
            "iso": "ARE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "country_id": "175",
            "ubigeo": "05",
            "name": "AYACUCHO",
            "iso": "AYA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "country_id": "175",
            "ubigeo": "06",
            "name": "CAJAMARCA",
            "iso": "CAJ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "country_id": "175",
            "ubigeo": "07",
            "name": "CALLAO",
            "iso": "CAL",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "country_id": "175",
            "ubigeo": "08",
            "name": "CUSCO",
            "iso": "CUS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "9",
            "country_id": "175",
            "ubigeo": "09",
            "name": "HUANCAVELICA",
            "iso": "HUV",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:34"
        },
        {
            "id": "10",
            "country_id": "175",
            "ubigeo": "10",
            "name": "HUÁNUCO",
            "iso": "HUC",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:34"
        },
        {
            "id": "11",
            "country_id": "175",
            "ubigeo": "11",
            "name": "ICA",
            "iso": "ICA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "12",
            "country_id": "175",
            "ubigeo": "12",
            "name": "JUNÍN",
            "iso": "JUN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "13",
            "country_id": "175",
            "ubigeo": "13",
            "name": "LA LIBERTAD",
            "iso": "LAL",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "14",
            "country_id": "175",
            "ubigeo": "14",
            "name": "LAMBAYEQUE",
            "iso": "LAM",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "15",
            "country_id": "175",
            "ubigeo": "15",
            "name": "LIMA",
            "iso": "LIM",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "16",
            "country_id": "175",
            "ubigeo": "16",
            "name": "LORETO",
            "iso": "LOR",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "17",
            "country_id": "175",
            "ubigeo": "17",
            "name": "MADRE DE DIOS",
            "iso": "MDD",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:34"
        },
        {
            "id": "18",
            "country_id": "175",
            "ubigeo": "18",
            "name": "MOQUEGUA",
            "iso": "MOQ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "19",
            "country_id": "175",
            "ubigeo": "19",
            "name": "PASCO",
            "iso": "PAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "20",
            "country_id": "175",
            "ubigeo": "20",
            "name": "PIURA",
            "iso": "PIU",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "21",
            "country_id": "175",
            "ubigeo": "21",
            "name": "PUNO",
            "iso": "PUN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "22",
            "country_id": "175",
            "ubigeo": "22",
            "name": "SAN MARTÍN",
            "iso": "SAM",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:34"
        },
        {
            "id": "23",
            "country_id": "175",
            "ubigeo": "23",
            "name": "TACNA",
            "iso": "TAC",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "24",
            "country_id": "175",
            "ubigeo": "24",
            "name": "TUMBES",
            "iso": "TUM",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "25",
            "country_id": "175",
            "ubigeo": "25",
            "name": "UCAYALI",
            "iso": "UCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $states = json_decode($states);

        foreach ($states as $state) {
        	DB::table('admin_states')->insert([
	            'country_id' 	=> $state->country_id,
	            'ubigeo'		=> $state->ubigeo,
	            'name' 			=> $state->name,
	            'iso' 			=> $state->iso,
	            'created_at'	=> $state->created_at,
	            'updated_at'	=> $state->updated_at
	        ]);
        }
    }
}
