<?php

use Illuminate\Database\Seeder;

class AddressZoneTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $AddressZoneType = '[
        {
            "id": "1",
            "code": "01",
            "short_name": "URB.",
            "name": "URBANIZACIÓN",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "2",
            "code": "02",
            "short_name": "P.J.",
            "name": "PUEBLO JOVEN",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "3",
            "code": "03",
            "short_name": "U.V.",
            "name": "UNIDAD VECINAL",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "4",
            "code": "04",
            "short_name": "C.H.",
            "name": "CONJUNTO HABITACIONAL",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "5",
            "code": "05",
            "short_name": "A.H.",
            "name": "ASENTAMIENTO HUMANO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "6",
            "code": "06",
            "short_name": "COO.",
            "name": "COOPERATIVA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "7",
            "code": "07",
            "short_name": "RES.",
            "name": "RESIDENCIAL",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "8",
            "code": "08",
            "short_name": "Z.I.",
            "name": "ZONA INDUSTRIAL",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "9",
            "code": "09",
            "short_name": "GRU.",
            "name": "GRUPO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "10",
            "code": "10",
            "short_name": "CAS.",
            "name": "CASERÍO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "11",
            "code": "11",
            "short_name": "FND.",
            "name": "FUNDO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "12",
            "code": "99",
            "short_name": "OTRO",
            "name": "OTROS",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        }
    ]
        ';

        $AddressZoneType = json_decode($AddressZoneType);

		foreach ($AddressZoneType as $zone) {
			DB::table('admin_address_zone_type')->insert([
				'code'    		=> $zone->code,
				'short_name'    => $zone->short_name,
				'name'			=> $zone->name,
				'created_at'    => $zone->created_at,
			]);
		}
    }
}
