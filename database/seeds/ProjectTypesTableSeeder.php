<?php

use Illuminate\Database\Seeder;

class ProjectTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ProjectTypes = '[
        {
            "id": "1",
            "name": "CONSTRUCCIÓN",
            "created_at": "2019-02-06 05:39:10",
            "updated_at": "2019-02-06 05:39:10"
        },
        {
            "id": "2",
            "name": "MINERÍA",
            "created_at": "2019-02-06 05:39:10",
            "updated_at": "2019-02-06 05:39:10"
        },
        {
            "id": "3",
            "name": "INDUSTRIAL",
            "created_at": "2019-02-06 05:39:10",
            "updated_at": "2019-02-06 05:39:10"
        },
        {
            "id": "4",
            "name": "SOCIALES",
            "created_at": "2019-02-06 05:39:10",
            "updated_at": "2019-02-06 05:39:10"
        },
        {
            "id": "5",
            "name": "EDUCATIVOS",
            "created_at": "2019-02-06 05:39:10",
            "updated_at": "2019-02-06 05:39:10"
        },
        {
            "id": "6",
            "name": "OTROS",
            "created_at": "2019-02-06 05:39:10",
            "updated_at": "2019-02-06 05:39:10"
        }
    ]
        ';

        $ProjectTypes = json_decode($ProjectTypes);

		foreach ($ProjectTypes as $protypes) {
			DB::table('project_types')->insert([
				'name'			=> $protypes->name,
				'created_at'    => $protypes->created_at,
				'updated_at'    => $protypes->updated_at
			]);
		}
    }
}
