<?php

use Illuminate\Database\Seeder;

class PaymentTermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $PaymentTerms = '
      [
        {
            "id": "1",
            "name": "PAGO INMEDIATO",
            "days_number": "0",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "name": "FACTURA 7 DÍAS",
            "days_number": "7",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "name": "FACTURA A 10 DÍAS",
            "days_number": "10",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "name": "FACTURA A 15 DÍAS",
            "days_number": "15",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "name": "FACTURA A 30 DÍAS",
            "days_number": "30",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "name": "FACTURA A 60 DÍAS",
            "days_number": "60",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "name": "FACTURA A 90 DÍAS",
            "days_number": "90",
            "value": "balance",
            "value_amount": "0",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "name": "50% ADELANTADO Y SALDO CONTRA ENTREGA",
            "days_number": "0",
            "value": "porcentaje",
            "value_amount": "50",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $PaymentTerms = json_decode($PaymentTerms);

        foreach ($PaymentTerms as $terms) {
          DB::table('basic_payment_terms')->insert([
            'name'            => $terms->name,
            'days_number'     => $terms->days_number,
            'value'           => $terms->value,
            'value_amount'    => $terms->value_amount,
            'created_at'      => $terms->created_at,
            'updated_at'      => $terms->updated_at
          ]);
        }
    }
}
