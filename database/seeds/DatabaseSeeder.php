<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MenuHorizontalSeeder::class);
        $this->call(MenuVerticalSeeder::class);
        $this->call(SubMenuVerticalSeeder::class);
        $this->call(UserRolesMenuSeeder::class);
        $this->call(RoleActionsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(PartnerCategoriesTableSeeder::class);
        $this->call(DocumentTypesTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(SaleRatesTableSeeder::class);
        /* $this->call(PaymentTermsTableSeeder::class);
        $this->call(PaymentTypesTableSeeder::class); */
        $this->call(UnitMeasureCategoriesTableSeeder::class);
        $this->call(UnitMeasuresTableSeeder::class);
        $this->call(CostTypesTableSeeder::class);
        $this->call(TypeCostCenterTableSeeder::class);

        $this->call(AddressstreetTableSeeder::class);
        $this->call(AddressZoneTypeTableSeeder::class);
        $this->call(ProjectTypesTableSeeder::class);

        //pesados
        
        
        
        
    }
}

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'username' => 'admin',
            'password' => \Hash::make(123456),
            'name' => 'Administrador',
            'lastname' => 'Administrador',
            'email' => 'admin@mail.com',
            'status' => 1,
            'signature' => 'firma',
            'role_id' => 1,
            'picture_name_random' => 'n'
        ]);
    }
}

