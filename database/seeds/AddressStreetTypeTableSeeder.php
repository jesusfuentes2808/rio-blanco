<?php

use Illuminate\Database\Seeder;

class AddressstreetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Addressstreet = '
      [
        {
            "id": "1",
            "code": "01",
            "short_name": "AV.",
            "name": "AVENIDA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "2",
            "code": "02",
            "short_name": "JR.",
            "name": "JIRÓN",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "3",
            "code": "03",
            "short_name": "CAL.",
            "name": "CALLE",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "4",
            "code": "04",
            "short_name": "PA.",
            "name": "PASAJE",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "5",
            "code": "05",
            "short_name": "ALAM.",
            "name": "ALAMEDA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "6",
            "code": "06",
            "short_name": "MAL.",
            "name": "MALECÓN",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "7",
            "code": "07",
            "short_name": "OV.",
            "name": "OVALO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "8",
            "code": "08",
            "short_name": "PARQ.",
            "name": "PARQUE",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "9",
            "code": "09",
            "short_name": "PLAZA",
            "name": "PLAZA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "10",
            "code": "10",
            "short_name": "CAR.",
            "name": "CARRETERA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "11",
            "code": "13",
            "short_name": "TR.",
            "name": "TROCHA ",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "12",
            "code": "14",
            "short_name": "CR.",
            "name": "CAMINO RURAL",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "13",
            "code": "15",
            "short_name": "BA.",
            "name": "BAJADA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "14",
            "code": "16",
            "short_name": "GA.",
            "name": "GALERIA ",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "15",
            "code": "17",
            "short_name": "PROL.",
            "name": "PROLONGACIÓN ",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "16",
            "code": "18",
            "short_name": "PAS.",
            "name": "PASEO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "17",
            "code": "19",
            "short_name": "PL.",
            "name": "PLAZUELA",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "18",
            "code": "20",
            "short_name": "PO.",
            "name": "PORTAL",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "19",
            "code": "21",
            "short_name": "CAF.",
            "name": "CAMINO AFIRMADO",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "20",
            "code": "22",
            "short_name": "TC.",
            "name": "TROCHA CARROZABLE",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        },
        {
            "id": "21",
            "code": "99",
            "short_name": "OTRO",
            "name": "OTROS",
            "created_at": "2019-02-06 20:00:00",
            "updated_at": "2019-02-06 20:00:00"
        }
    ]
        ';

        $Addressstreet = json_decode($Addressstreet);

        foreach ($Addressstreet as $street) {
          DB::table('admin_address_street_type')->insert([
            'code'           => $street->code,
            'short_name'     => $street->short_name,
            'name'           => $street->name,
            'created_at'     => $street->created_at,
            'updated_at'     => $street->updated_at
          ]);
        }
    }
}
