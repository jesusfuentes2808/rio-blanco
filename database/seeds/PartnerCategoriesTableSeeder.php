<?php

use Illuminate\Database\Seeder;

class PartnerCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_partner_categories')->insert([
        	'name' => 'PERSONA NATURAL'
        ]);

        DB::table('admin_partner_categories')->insert([
        	'name' => 'PERSONA JURIDICA'
        ]);
    }
}
