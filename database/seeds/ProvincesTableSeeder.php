<?php

use Illuminate\Database\Seeder;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces = '
			[
        {
            "id": "1",
            "state_id": "1",
            "ubigeo": "01",
            "code": "0101",
            "name": "CHACHAPOYAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "state_id": "1",
            "ubigeo": "02",
            "code": "0102",
            "name": "BAGUA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "state_id": "1",
            "ubigeo": "03",
            "code": "0103",
            "name": "BONGARÁ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "state_id": "1",
            "ubigeo": "04",
            "code": "0104",
            "name": "CONDORCANQUI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "state_id": "1",
            "ubigeo": "05",
            "code": "0105",
            "name": "LUYA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "state_id": "1",
            "ubigeo": "06",
            "code": "0106",
            "name": "RODRÍGUEZ DE MENDOZA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "state_id": "1",
            "ubigeo": "07",
            "code": "0107",
            "name": "UTCUBAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "state_id": "2",
            "ubigeo": "01",
            "code": "0201",
            "name": "HUARAZ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "9",
            "state_id": "2",
            "ubigeo": "02",
            "code": "0202",
            "name": "AIJA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "10",
            "state_id": "2",
            "ubigeo": "03",
            "code": "0203",
            "name": "ANTONIO RAYMONDI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "11",
            "state_id": "2",
            "ubigeo": "04",
            "code": "0204",
            "name": "ASUNCIÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "12",
            "state_id": "2",
            "ubigeo": "05",
            "code": "0205",
            "name": "BOLOGNESI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "13",
            "state_id": "2",
            "ubigeo": "06",
            "code": "0206",
            "name": "CARHUAZ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "14",
            "state_id": "2",
            "ubigeo": "07",
            "code": "0207",
            "name": "CARLOS FERMÍN FITZCARRALD",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "15",
            "state_id": "2",
            "ubigeo": "08",
            "code": "0208",
            "name": "CASMA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "16",
            "state_id": "2",
            "ubigeo": "09",
            "code": "0209",
            "name": "CORONGO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "17",
            "state_id": "2",
            "ubigeo": "10",
            "code": "0210",
            "name": "HUARI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "18",
            "state_id": "2",
            "ubigeo": "11",
            "code": "0211",
            "name": "HUARMEY",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "19",
            "state_id": "2",
            "ubigeo": "12",
            "code": "0212",
            "name": "HUAYLAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "20",
            "state_id": "2",
            "ubigeo": "13",
            "code": "0213",
            "name": "MARISCAL LUZURIAGA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "21",
            "state_id": "2",
            "ubigeo": "14",
            "code": "0214",
            "name": "OCROS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "22",
            "state_id": "2",
            "ubigeo": "15",
            "code": "0215",
            "name": "PALLASCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "23",
            "state_id": "2",
            "ubigeo": "16",
            "code": "0216",
            "name": "POMABAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "24",
            "state_id": "2",
            "ubigeo": "17",
            "code": "0217",
            "name": "RECUAY",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "25",
            "state_id": "2",
            "ubigeo": "18",
            "code": "0218",
            "name": "SANTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "26",
            "state_id": "2",
            "ubigeo": "19",
            "code": "0219",
            "name": "SIHUAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "27",
            "state_id": "2",
            "ubigeo": "20",
            "code": "0220",
            "name": "YUNGAY",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "28",
            "state_id": "3",
            "ubigeo": "01",
            "code": "0301",
            "name": "ABANCAY",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "29",
            "state_id": "3",
            "ubigeo": "02",
            "code": "0302",
            "name": "ANDAHUAYLAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "30",
            "state_id": "3",
            "ubigeo": "03",
            "code": "0303",
            "name": "ANTABAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "31",
            "state_id": "3",
            "ubigeo": "04",
            "code": "0304",
            "name": "AYMARAES",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "32",
            "state_id": "3",
            "ubigeo": "05",
            "code": "0305",
            "name": "COTABAMBAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "33",
            "state_id": "3",
            "ubigeo": "06",
            "code": "0306",
            "name": "CHINCHEROS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "34",
            "state_id": "3",
            "ubigeo": "07",
            "code": "0307",
            "name": "GRAU",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "35",
            "state_id": "4",
            "ubigeo": "01",
            "code": "0401",
            "name": "AREQUIPA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "36",
            "state_id": "4",
            "ubigeo": "02",
            "code": "0402",
            "name": "CAMANÁ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "37",
            "state_id": "4",
            "ubigeo": "03",
            "code": "0403",
            "name": "CARAVELÍ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "38",
            "state_id": "4",
            "ubigeo": "04",
            "code": "0404",
            "name": "CASTILLA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "39",
            "state_id": "4",
            "ubigeo": "05",
            "code": "0405",
            "name": "CAYLLOMA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "40",
            "state_id": "4",
            "ubigeo": "06",
            "code": "0406",
            "name": "CONDESUYOS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "41",
            "state_id": "4",
            "ubigeo": "07",
            "code": "0407",
            "name": "ISLAY",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "42",
            "state_id": "4",
            "ubigeo": "08",
            "code": "0408",
            "name": "LA UNIÒN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "43",
            "state_id": "5",
            "ubigeo": "01",
            "code": "0501",
            "name": "HUAMANGA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "44",
            "state_id": "5",
            "ubigeo": "02",
            "code": "0502",
            "name": "CANGALLO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "45",
            "state_id": "5",
            "ubigeo": "03",
            "code": "0503",
            "name": "HUANCA SANCOS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "46",
            "state_id": "5",
            "ubigeo": "04",
            "code": "0504",
            "name": "HUANTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "47",
            "state_id": "5",
            "ubigeo": "05",
            "code": "0505",
            "name": "LA MAR",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "48",
            "state_id": "5",
            "ubigeo": "06",
            "code": "0506",
            "name": "LUCANAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "49",
            "state_id": "5",
            "ubigeo": "07",
            "code": "0507",
            "name": "PARINACOCHAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "50",
            "state_id": "5",
            "ubigeo": "08",
            "code": "0508",
            "name": "PÀUCAR DEL SARA SARA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "51",
            "state_id": "5",
            "ubigeo": "09",
            "code": "0509",
            "name": "SUCRE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "52",
            "state_id": "5",
            "ubigeo": "10",
            "code": "0510",
            "name": "VÍCTOR FAJARDO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "53",
            "state_id": "5",
            "ubigeo": "11",
            "code": "0511",
            "name": "VILCAS HUAMÁN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "54",
            "state_id": "6",
            "ubigeo": "01",
            "code": "0601",
            "name": "CAJAMARCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "55",
            "state_id": "6",
            "ubigeo": "02",
            "code": "0602",
            "name": "CAJABAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "56",
            "state_id": "6",
            "ubigeo": "03",
            "code": "0603",
            "name": "CELENDÍN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "57",
            "state_id": "6",
            "ubigeo": "04",
            "code": "0604",
            "name": "CHOTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "58",
            "state_id": "6",
            "ubigeo": "05",
            "code": "0605",
            "name": "CONTUMAZÁ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "59",
            "state_id": "6",
            "ubigeo": "06",
            "code": "0606",
            "name": "CUTERVO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "60",
            "state_id": "6",
            "ubigeo": "07",
            "code": "0607",
            "name": "HUALGAYOC",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "61",
            "state_id": "6",
            "ubigeo": "08",
            "code": "0608",
            "name": "JAÉN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "62",
            "state_id": "6",
            "ubigeo": "09",
            "code": "0609",
            "name": "SAN IGNACIO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "63",
            "state_id": "6",
            "ubigeo": "10",
            "code": "0610",
            "name": "SAN MARCOS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "64",
            "state_id": "6",
            "ubigeo": "11",
            "code": "0611",
            "name": "SAN MIGUEL",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "65",
            "state_id": "6",
            "ubigeo": "12",
            "code": "0612",
            "name": "SAN PABLO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "66",
            "state_id": "6",
            "ubigeo": "13",
            "code": "0613",
            "name": "SANTA CRUZ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "67",
            "state_id": "7",
            "ubigeo": "01",
            "code": "0701",
            "name": "PROV. CONST. DEL CALLAO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "68",
            "state_id": "8",
            "ubigeo": "01",
            "code": "0801",
            "name": "CUSCO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "69",
            "state_id": "8",
            "ubigeo": "02",
            "code": "0802",
            "name": "ACOMAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "70",
            "state_id": "8",
            "ubigeo": "03",
            "code": "0803",
            "name": "ANTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "71",
            "state_id": "8",
            "ubigeo": "04",
            "code": "0804",
            "name": "CALCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "72",
            "state_id": "8",
            "ubigeo": "05",
            "code": "0805",
            "name": "CANAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "73",
            "state_id": "8",
            "ubigeo": "06",
            "code": "0806",
            "name": "CANCHIS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "74",
            "state_id": "8",
            "ubigeo": "07",
            "code": "0807",
            "name": "CHUMBIVILCAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "75",
            "state_id": "8",
            "ubigeo": "08",
            "code": "0808",
            "name": "ESPINAR",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "76",
            "state_id": "8",
            "ubigeo": "09",
            "code": "0809",
            "name": "LA CONVENCIÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "77",
            "state_id": "8",
            "ubigeo": "10",
            "code": "0810",
            "name": "PARURO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "78",
            "state_id": "8",
            "ubigeo": "11",
            "code": "0811",
            "name": "PAUCARTAMBO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "79",
            "state_id": "8",
            "ubigeo": "12",
            "code": "0812",
            "name": "QUISPICANCHI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "80",
            "state_id": "8",
            "ubigeo": "13",
            "code": "0813",
            "name": "URUBAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "81",
            "state_id": "9",
            "ubigeo": "01",
            "code": "0901",
            "name": "HUANCAVELICA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "82",
            "state_id": "9",
            "ubigeo": "02",
            "code": "0902",
            "name": "ACOBAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "83",
            "state_id": "9",
            "ubigeo": "03",
            "code": "0903",
            "name": "ANGARAES",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "84",
            "state_id": "9",
            "ubigeo": "04",
            "code": "0904",
            "name": "CASTROVIRREYNA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "85",
            "state_id": "9",
            "ubigeo": "05",
            "code": "0905",
            "name": "CHURCAMPA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "86",
            "state_id": "9",
            "ubigeo": "06",
            "code": "0906",
            "name": "HUAYTARÁ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "87",
            "state_id": "9",
            "ubigeo": "07",
            "code": "0907",
            "name": "TAYACAJA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "88",
            "state_id": "10",
            "ubigeo": "01",
            "code": "1001",
            "name": "HUÁNUCO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "89",
            "state_id": "10",
            "ubigeo": "02",
            "code": "1002",
            "name": "AMBO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "90",
            "state_id": "10",
            "ubigeo": "03",
            "code": "1003",
            "name": "DOS DE MAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "91",
            "state_id": "10",
            "ubigeo": "04",
            "code": "1004",
            "name": "HUACAYBAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "92",
            "state_id": "10",
            "ubigeo": "05",
            "code": "1005",
            "name": "HUAMALÍES",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "93",
            "state_id": "10",
            "ubigeo": "06",
            "code": "1006",
            "name": "LEONCIO PRADO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "94",
            "state_id": "10",
            "ubigeo": "07",
            "code": "1007",
            "name": "MARAÑÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "95",
            "state_id": "10",
            "ubigeo": "08",
            "code": "1008",
            "name": "PACHITEA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "96",
            "state_id": "10",
            "ubigeo": "09",
            "code": "1009",
            "name": "PUERTO INCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "97",
            "state_id": "10",
            "ubigeo": "10",
            "code": "1010",
            "name": "LAURICOCHA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "98",
            "state_id": "10",
            "ubigeo": "11",
            "code": "1011",
            "name": "YAROWILCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "99",
            "state_id": "11",
            "ubigeo": "01",
            "code": "1101",
            "name": "ICA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "100",
            "state_id": "11",
            "ubigeo": "02",
            "code": "1102",
            "name": "CHINCHA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "101",
            "state_id": "11",
            "ubigeo": "03",
            "code": "1103",
            "name": "NAZCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "102",
            "state_id": "11",
            "ubigeo": "04",
            "code": "1104",
            "name": "PALPA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "103",
            "state_id": "11",
            "ubigeo": "05",
            "code": "1105",
            "name": "PISCO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "104",
            "state_id": "12",
            "ubigeo": "01",
            "code": "1201",
            "name": "HUANCAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "105",
            "state_id": "12",
            "ubigeo": "02",
            "code": "1202",
            "name": "CONCEPCIÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "106",
            "state_id": "12",
            "ubigeo": "03",
            "code": "1203",
            "name": "CHANCHAMAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "107",
            "state_id": "12",
            "ubigeo": "04",
            "code": "1204",
            "name": "JAUJA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "108",
            "state_id": "12",
            "ubigeo": "05",
            "code": "1205",
            "name": "JUNÍN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "109",
            "state_id": "12",
            "ubigeo": "06",
            "code": "1206",
            "name": "SATIPO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "110",
            "state_id": "12",
            "ubigeo": "07",
            "code": "1207",
            "name": "TARMA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "111",
            "state_id": "12",
            "ubigeo": "08",
            "code": "1208",
            "name": "YAULI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "112",
            "state_id": "12",
            "ubigeo": "09",
            "code": "1209",
            "name": "CHUPACA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "113",
            "state_id": "13",
            "ubigeo": "01",
            "code": "1301",
            "name": "TRUJILLO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "114",
            "state_id": "13",
            "ubigeo": "02",
            "code": "1302",
            "name": "ASCOPE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "115",
            "state_id": "13",
            "ubigeo": "03",
            "code": "1303",
            "name": "BOLÍVAR",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "116",
            "state_id": "13",
            "ubigeo": "04",
            "code": "1304",
            "name": "CHEPÉN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "117",
            "state_id": "13",
            "ubigeo": "05",
            "code": "1305",
            "name": "JULCÁN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "118",
            "state_id": "13",
            "ubigeo": "06",
            "code": "1306",
            "name": "OTUZCO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "119",
            "state_id": "13",
            "ubigeo": "07",
            "code": "1307",
            "name": "PACASMAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "120",
            "state_id": "13",
            "ubigeo": "08",
            "code": "1308",
            "name": "PATAZ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "121",
            "state_id": "13",
            "ubigeo": "09",
            "code": "1309",
            "name": "SÁNCHEZ CARRIÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "122",
            "state_id": "13",
            "ubigeo": "10",
            "code": "1310",
            "name": "SANTIAGO DE CHUCO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "123",
            "state_id": "13",
            "ubigeo": "11",
            "code": "1311",
            "name": "GRAN CHIMÚ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "124",
            "state_id": "13",
            "ubigeo": "12",
            "code": "1312",
            "name": "VIRÚ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "125",
            "state_id": "14",
            "ubigeo": "01",
            "code": "1401",
            "name": "CHICLAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "126",
            "state_id": "14",
            "ubigeo": "02",
            "code": "1402",
            "name": "FERREÑAFE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "127",
            "state_id": "14",
            "ubigeo": "03",
            "code": "1403",
            "name": "LAMBAYEQUE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "128",
            "state_id": "15",
            "ubigeo": "01",
            "code": "1501",
            "name": "LIMA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "129",
            "state_id": "15",
            "ubigeo": "02",
            "code": "1502",
            "name": "BARRANCA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "130",
            "state_id": "15",
            "ubigeo": "03",
            "code": "1503",
            "name": "CAJATAMBO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "131",
            "state_id": "15",
            "ubigeo": "04",
            "code": "1504",
            "name": "CANTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "132",
            "state_id": "15",
            "ubigeo": "05",
            "code": "1505",
            "name": "CAÑETE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "133",
            "state_id": "15",
            "ubigeo": "06",
            "code": "1506",
            "name": "HUARAL",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "134",
            "state_id": "15",
            "ubigeo": "07",
            "code": "1507",
            "name": "HUAROCHIRÍ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "135",
            "state_id": "15",
            "ubigeo": "08",
            "code": "1508",
            "name": "HUAURA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "136",
            "state_id": "15",
            "ubigeo": "09",
            "code": "1509",
            "name": "OYÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "137",
            "state_id": "15",
            "ubigeo": "10",
            "code": "1510",
            "name": "YAUYOS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "138",
            "state_id": "16",
            "ubigeo": "01",
            "code": "1601",
            "name": "MAYNAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "139",
            "state_id": "16",
            "ubigeo": "02",
            "code": "1602",
            "name": "ALTO AMAZONAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "140",
            "state_id": "16",
            "ubigeo": "03",
            "code": "1603",
            "name": "LORETO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "141",
            "state_id": "16",
            "ubigeo": "04",
            "code": "1604",
            "name": "MARISCAL RAMÓN CASTILLA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "142",
            "state_id": "16",
            "ubigeo": "05",
            "code": "1605",
            "name": "REQUENA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "143",
            "state_id": "16",
            "ubigeo": "06",
            "code": "1606",
            "name": "UCAYALI",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "144",
            "state_id": "16",
            "ubigeo": "07",
            "code": "1607",
            "name": "DATEM DEL MARAÑÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "145",
            "state_id": "16",
            "ubigeo": "08",
            "code": "1608",
            "name": "PUTUMAYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "146",
            "state_id": "17",
            "ubigeo": "01",
            "code": "1701",
            "name": "TAMBOPATA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "147",
            "state_id": "17",
            "ubigeo": "02",
            "code": "1702",
            "name": "MANU",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "148",
            "state_id": "17",
            "ubigeo": "03",
            "code": "1703",
            "name": "TAHUAMANU",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "149",
            "state_id": "18",
            "ubigeo": "01",
            "code": "1801",
            "name": "MARISCAL NIETO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "150",
            "state_id": "18",
            "ubigeo": "02",
            "code": "1802",
            "name": "GENERAL SÁNCHEZ CERRO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "151",
            "state_id": "18",
            "ubigeo": "03",
            "code": "1803",
            "name": "ILO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "152",
            "state_id": "19",
            "ubigeo": "01",
            "code": "1901",
            "name": "PASCO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "153",
            "state_id": "19",
            "ubigeo": "02",
            "code": "1902",
            "name": "DANIEL ALCIDES CARRIÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "154",
            "state_id": "19",
            "ubigeo": "03",
            "code": "1903",
            "name": "OXAPAMPA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "155",
            "state_id": "20",
            "ubigeo": "01",
            "code": "2001",
            "name": "PIURA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "156",
            "state_id": "20",
            "ubigeo": "02",
            "code": "2002",
            "name": "AYABACA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "157",
            "state_id": "20",
            "ubigeo": "03",
            "code": "2003",
            "name": "HUANCABAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "158",
            "state_id": "20",
            "ubigeo": "04",
            "code": "2004",
            "name": "MORROPÓN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "159",
            "state_id": "20",
            "ubigeo": "05",
            "code": "2005",
            "name": "PAITA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "160",
            "state_id": "20",
            "ubigeo": "06",
            "code": "2006",
            "name": "SULLANA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "161",
            "state_id": "20",
            "ubigeo": "07",
            "code": "2007",
            "name": "TALARA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "162",
            "state_id": "20",
            "ubigeo": "08",
            "code": "2008",
            "name": "SECHURA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "163",
            "state_id": "21",
            "ubigeo": "01",
            "code": "2101",
            "name": "PUNO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "164",
            "state_id": "21",
            "ubigeo": "02",
            "code": "2102",
            "name": "AZÁNGARO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "165",
            "state_id": "21",
            "ubigeo": "03",
            "code": "2103",
            "name": "CARABAYA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "166",
            "state_id": "21",
            "ubigeo": "04",
            "code": "2104",
            "name": "CHUCUITO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "167",
            "state_id": "21",
            "ubigeo": "05",
            "code": "2105",
            "name": "EL COLLAO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "168",
            "state_id": "21",
            "ubigeo": "06",
            "code": "2106",
            "name": "HUANCANÉ",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "169",
            "state_id": "21",
            "ubigeo": "07",
            "code": "2107",
            "name": "LAMPA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "170",
            "state_id": "21",
            "ubigeo": "08",
            "code": "2108",
            "name": "MELGAR",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "171",
            "state_id": "21",
            "ubigeo": "09",
            "code": "2109",
            "name": "MOHO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "172",
            "state_id": "21",
            "ubigeo": "10",
            "code": "2110",
            "name": "SAN ANTONIO DE PUTINA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "173",
            "state_id": "21",
            "ubigeo": "11",
            "code": "2111",
            "name": "SAN ROMÁN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "174",
            "state_id": "21",
            "ubigeo": "12",
            "code": "2112",
            "name": "SANDIA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "175",
            "state_id": "21",
            "ubigeo": "13",
            "code": "2113",
            "name": "YUNGUYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "176",
            "state_id": "22",
            "ubigeo": "01",
            "code": "2201",
            "name": "MOYOBAMBA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "177",
            "state_id": "22",
            "ubigeo": "02",
            "code": "2202",
            "name": "BELLAVISTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "178",
            "state_id": "22",
            "ubigeo": "03",
            "code": "2203",
            "name": "EL DORADO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "179",
            "state_id": "22",
            "ubigeo": "04",
            "code": "2204",
            "name": "HUALLAGA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "180",
            "state_id": "22",
            "ubigeo": "05",
            "code": "2205",
            "name": "LAMAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "181",
            "state_id": "22",
            "ubigeo": "06",
            "code": "2206",
            "name": "MARISCAL CÁCERES",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "182",
            "state_id": "22",
            "ubigeo": "07",
            "code": "2207",
            "name": "PICOTA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "183",
            "state_id": "22",
            "ubigeo": "08",
            "code": "2208",
            "name": "RIOJA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "184",
            "state_id": "22",
            "ubigeo": "09",
            "code": "2209",
            "name": "SAN MARTÍN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "185",
            "state_id": "22",
            "ubigeo": "10",
            "code": "2210",
            "name": "TOCACHE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "186",
            "state_id": "23",
            "ubigeo": "01",
            "code": "2301",
            "name": "TACNA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "187",
            "state_id": "23",
            "ubigeo": "02",
            "code": "2302",
            "name": "CANDARAVE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "188",
            "state_id": "23",
            "ubigeo": "03",
            "code": "2303",
            "name": "JORGE BASADRE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "189",
            "state_id": "23",
            "ubigeo": "04",
            "code": "2304",
            "name": "TARATA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "190",
            "state_id": "24",
            "ubigeo": "01",
            "code": "2401",
            "name": "TUMBES",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "191",
            "state_id": "24",
            "ubigeo": "02",
            "code": "2402",
            "name": "CONTRALMIRANTE VILLAR",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "192",
            "state_id": "24",
            "ubigeo": "03",
            "code": "2403",
            "name": "ZARUMILLA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "193",
            "state_id": "25",
            "ubigeo": "01",
            "code": "2501",
            "name": "CORONEL PORTILLO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "194",
            "state_id": "25",
            "ubigeo": "02",
            "code": "2502",
            "name": "ATALAYA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "195",
            "state_id": "25",
            "ubigeo": "03",
            "code": "2503",
            "name": "PADRE ABAD",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "196",
            "state_id": "25",
            "ubigeo": "04",
            "code": "2504",
            "name": "PURÚS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $provinces = json_decode($provinces);

        foreach ($provinces as $province) {
        	DB::table('admin_provinces')->insert([
	            'state_id'	 	=> $province->state_id,
	            'ubigeo'		=> $province->ubigeo,
	            'code' 			=> $province->code,
	            'name' 			=> $province->name,
	            'created_at'	=> $province->created_at,
	            'updated_at'	=> $province->updated_at
	        ]);
        }
    }
}
