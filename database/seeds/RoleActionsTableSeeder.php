<?php

use Illuminate\Database\Seeder;

class RoleActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_role_actions')->insert([
        	'role_id' => 1,
          'module' => 'users',
          'actions' => 'VIEW|CREATE|UPDATE|DELETE'
       	]);

       	DB::table('admin_role_actions')->insert([
        	'role_id' => 1,
          'module' => 'companies',
          'actions' => 'VIEW|CREATE|UPDATE|DELETE'
       	]);

       	DB::table('admin_role_actions')->insert([
          'role_id' => 1,
          'module' => 'clients',
          'actions' => 'VIEW|CREATE|UPDATE|DELETE'
        ]);
    }
}
