<?php

use Illuminate\Database\Seeder;

class CostTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $costTypes = '
			[
				{
					"id": "1",
					"code": "D",
					"name": "DIRECTO",
					"created_at": "2019-01-21 23:00:00",
					"updated_at": "2019-01-21 23:00:00"
				},
				{
					"id": "2",
					"code": "I",
					"name": "INDIRECTO",
					"created_at": "2019-01-21 23:00:00",
					"updated_at": "2019-01-21 23:00:00"
				}
			]
       ';

       $costTypes = json_decode($costTypes);

       foreach ($costTypes as $costType) {
       		DB::table('basic_cost_types')->insert([
       			'code' => $costType->code,
       			'name' => $costType->name
       		]);
       }
    }
}
