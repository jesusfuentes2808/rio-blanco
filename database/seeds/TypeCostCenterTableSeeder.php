<?php

use Illuminate\Database\Seeder;

class TypeCostCenterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeCostCenters = '
			[
        {
            "id": "1",
            "code": "A",
            "name": "APOYO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "code": "I",
            "name": "INGENIERIA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "code": "E",
            "name": "EQUIPOS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "code": "F",
            "name": "FAMILIA EQUIPOS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "code": "S",
            "name": "SUBCONTRATISTAS",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $typeCostCenters = json_decode($typeCostCenters);

        foreach ($typeCostCenters as $type) {
        	DB::table('basic_cost_center_types')->insert([
        		'code' => $type->code,
        		'name' => $type->name,
        		'created_at'	=> $type->created_at,
	            'updated_at'	=> $type->updated_at
        	]);
        }
    }
}
