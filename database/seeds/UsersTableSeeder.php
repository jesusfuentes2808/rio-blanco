<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'username' => 'admin',
            'password' => \Hash::make(123456),
            'name' => 'Administrador',
            'lastname' => 'Administrador',
            'email' => 'admin@mail.com',
            'status' => 1,
            'signature' => 'firma',
            'role_id' => 1,
            'picture_name_random' => 'n'
        ]);
    }
}
