<?php

use Illuminate\Database\Seeder;

class DocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $documentTypes = '[
        {
            "id": "1",
            "code": "0",
            "short_name": "DOC",
            "name": "DOC. TRIB. NO DOMICIALDO SIN RUC",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "code": "1",
            "short_name": "DNI",
            "name": "DOC. NACIONAL DE IDENTIDAD",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "code": "4",
            "short_name": "CEX",
            "name": "CARNET DE EXTRANJERIA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "code": "6",
            "short_name": "RUC",
            "name": "REG. UNICO DE CONTRIBUYENTES",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "code": "7",
            "short_name": "PAS",
            "name": "PASAPORTE",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "code": "A",
            "short_name": "CED",
            "name": "CED. DIPLOMATICA DE IDENTIDAD",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "code": "B",
            "short_name": "NOD",
            "name": "DOC. IDENT. PAIS RESIDENCIA NO DOMICILIADO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "8",
            "code": "C",
            "short_name": "TIN",
            "name": "TAX IDENTIFICATION NUMBER - TIN - DOC TRIB PP.NN.",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "9",
            "code": "D",
            "short_name": "IND",
            "name": "IDENTIFICATION NUMBER - IN - DOC TRIB PP. JJ.",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]';

		$documentTypes = json_decode($documentTypes);

		foreach ($documentTypes as $document) {
			DB::table('admin_document_types')->insert([
				'code' 			=> $document->code,
				'short_name' 	=> $document->short_name,
				'name' 			=> $document->name,
				'created_at'	=> $document->created_at,
				'updated_at'	=> $document->updated_at
			]);
		}

    }
}
