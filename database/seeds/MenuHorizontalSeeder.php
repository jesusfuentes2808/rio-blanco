<?php

use Illuminate\Database\Seeder;

class MenuHorizontalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $MenuHorizontal = '
      [
        {
            "id": "1",
            "name": "Administración",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "name": "Compras",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "name": "Ventas",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "name": "Inventario",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "name": "Activos",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "name": "Subcontratos",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "7",
            "name": "Caja",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $MenuHorizontal = json_decode($MenuHorizontal);

        foreach ($MenuHorizontal as $mhorizontal) {
          DB::table('admin_menu_horizontals')->insert([
            'name'            => $mhorizontal->name,
            'created_at'      => $mhorizontal->created_at,
            'updated_at'      => $mhorizontal->updated_at
          ]);
        }
    }
}
