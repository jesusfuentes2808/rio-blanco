<?php

use Illuminate\Database\Seeder;

class UnitMeasureCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $unitMeasureCategories = '
			[
        {
            "id": "1",
            "name": "UNIDAD",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "2",
            "name": "PESO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "3",
            "name": "TIEMPO DE TRABAJO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "4",
            "name": "LONGITUD / DIATANCIA",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "5",
            "name": "VOLUMEN",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        },
        {
            "id": "6",
            "name": "OTRO",
            "created_at": "2019-01-21 23:40:33",
            "updated_at": "2019-01-21 23:40:33"
        }
    ]
        ';

        $unitMeasureCategories = json_decode($unitMeasureCategories);

        foreach ($unitMeasureCategories as $unitCategory) {
        	DB::table('basic_unit_measure_categories')->insert([
        		'name' 			=> $unitCategory->name,
        		'created_at'	=> $unitCategory->created_at,
	            'updated_at'	=> $unitCategory->updated_at
        	]);
        }
    }
}
