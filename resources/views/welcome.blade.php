<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Rio Blanco</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="img/favicon.ico"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- global css -->
        <link type="text/css" href="{{ env('APP_URL') . mix('/css/app.css') }}" rel="stylesheet"/>
        <link type="text/css" href="css/font-awesome.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="css/custom.css">
        <link rel="stylesheet" href="css/custom_css/skins/skin-default.css" type="text/css" id="skin"/>
        <link rel="stylesheet" href="css/buttons_sass.css">
        <link rel="stylesheet" type="text/css" href="css/sweetalert.min.css" />
        <link rel="stylesheet" href="js/lcswitch/css/lc_switch.css">
        <link rel="stylesheet" href="js/iCheck/css/all.css">
        <!-- end of global css -->
        <script>
        window.Laravel = <?php echo json_encode([
                                                'csrfToken'     => csrf_token(),
                                                'asset_storage' => asset('/storage/event/'),
                                                'asset_storage_profile' => asset('/storage/event/150x150_'),
                                                'asset_storage_company' => asset('/storage/company/150x150_'),
                                                'status'=>['Inactivo','Activo'],
                                            ]);
        ?>;
        </script>
    </head>

    <body class="skin-default">

        <div id="vue_header"></div>

        <div id="app"></div>
        @routes
        <script src="js/app.js" type="text/javascript"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/lcswitch/js/lc_switch.min.js"></script>
        <script src="js/iCheck/js/icheck.js"></script>
        <script src="{{ env('APP_URL') . mix('/js/main.js') }}" type="text/javascript"></script>
        <script type="text/javascript" src="js/sweetalert.min.js"></script>
        <!-- end of page level js -->
    </body>

</html>
