import VueRouter from 'vue-router';


import Home from './components/Home';
import Companies from './components/companies/Index';
import CreateCompany from './components/companies/Create';

import Users from './components/users/Index';
import CreateUser from './components/users/Create';

import Clients from './components/clients/Index';
import CreateClient from './components/clients/Create';

import Banks from './components/banks/Index';
import CreateBank from './components/banks/Create';

import TypeAccounts from './components/typeAccounts/Index';
import CreateTypeAccount from './components/typeAccounts/Create';

import PartnerCategories from './components/partnerCategories/Index';
import CreatePartnerCategories from './components/partnerCategories/Create';

import DocumentsType from './components/documentsType/Index';
import CreateDocumentsType from './components/documentsType/Create';

import SalesRate from './components/salesRate/Index';
import CreateSalesRate from './components/salesRate/Create';

import PaymentConditions from './components/paymentConditions/Index';
import CreatePaymentConditions from './components/paymentConditions/Create';

import DataContacts from './components/dataContacts/Index';
import CreateDataContacts from './components/dataContacts/Create';

import TypeAddresses from './components/typeAddresses/Index';
import CreateTypeAddresses from './components/typeAddresses/Create';

import CostType from './components/costTypes/Index';
import CreateCostType from './components/costTypes/Create';

import TypeCostCenters from './components/typeCostCenters/Index';
import CreateTypeCostCenters from './components/typeCostCenters/Create';

import CostCenters from './components/costCenters/Index';
import CreateCostCenters from './components/costCenters/Create';

import UnitMeasureCategories from './components/unitMeasureCategories/Index';
import CreateUnitMeasureCategories from './components/unitMeasureCategories/Create';

import ResponsibleType from './components/responsibleTypes/Index';
import CreateResponsibleType from './components/responsibleTypes/Create';

import Responsibles from './components/responsibles/Index';
import CreateResponsible from './components/responsibles/Create';

import Managements from './components/managements/Index';
import CreateManagement from './components/managements/Create';

import Sections from './components/sections/Index';
import CreateSection from './components/sections/Create';

import SubSections from './components/subSections/Index';
import CreateSubSection from './components/subSections/Create';

import UnitMeasures from './components/unitMeasures/Index';
import CreateUnitMeasure from './components/unitMeasures/Create';

import Projects from './components/projects/Index';
import CreateProjects from './components/projects/Create';

import ProjectTypes from './components/projectTypes/Index';
import CreateProjectTypes from './components/projectTypes/Create';

import FiscalYear from './components/fiscalYear/Index';
import CreateFiscalYear from './components/fiscalYear/Create';

import Periods from './components/periods/Index';
import CreatePeriods from './components/periods/Create';

const routes = [
  { path: '/', component: Home },

  { path: '/users', component: Users },
  { path: '/users/:id', component: CreateUser },

  { path: '/companies', component: Companies },
  { path: '/companies/:id', component: CreateCompany },

  { path: '/clients', component: Clients },
  { path: '/clients/:id', component: CreateClient },

  { path: '/banks', component: Banks },
  { path: '/banks/:id', component: CreateBank },

  { path: '/type-accounts', component: TypeAccounts },
  { path: '/type-accounts/:id', component: CreateTypeAccount },

  { path: '/partner-category', component: PartnerCategories },
  { path: '/partner-category/:id', component: CreatePartnerCategories },

  { path: '/documents-type', component: DocumentsType },
  { path: '/documents-type/:id', component: CreateDocumentsType },

  { path: '/sales-rate', component: SalesRate },
  { path: '/sales-rate/:id', component: CreateSalesRate },

  { path: '/payment-conditions', component: PaymentConditions },
  { path: '/payment-conditions/:id', component: CreatePaymentConditions },

  { path: '/type-contacts', component: DataContacts },
  { path: '/type-contacts/:id', component: CreateDataContacts },

  { path: '/type-addresses', component: TypeAddresses },
  { path: '/type-addresses/:id', component: CreateTypeAddresses },

  { path: '/type-cost-center', component: TypeCostCenters },
  { path: '/type-cost-center/:id', component: CreateTypeCostCenters },

  { path: '/cost-types', component: CostType },
  { path: '/cost-types/:id', component: CreateCostType },

  { path: '/unit-measure-categories', component: UnitMeasureCategories },
  { path: '/unit-measure-categories/:id', component: CreateUnitMeasureCategories },

  { path: '/responsible-types', component: ResponsibleType },
  { path: '/responsible-types/:id', component: CreateResponsibleType },

  { path: '/responsibles', component: Responsibles },
  { path: '/responsibles/:id', component: CreateResponsible },

  { path: '/cost-center', component: CostCenters },
  { path: '/cost-center/:id', component: CreateCostCenters },
  
  { path: '/managements', component: Managements },
  { path: '/managements/:id', component: CreateManagement },

  { path: '/sections', component: Sections },
  { path: '/sections/:id', component: CreateSection },

  { path: '/sub-sections', component: SubSections },
  { path: '/sub-sections/:id', component: CreateSubSection },

  { path: '/unit-measures', component: UnitMeasures },
  { path: '/unit-measures/:id', component: CreateUnitMeasure },

  { path: '/projects', component: Projects },
  { path: '/projects/:id', component: CreateProjects },

  { path: '/project-types', component: ProjectTypes },
  { path: '/project-types/:id', component: CreateProjectTypes },

  { path: '/fiscal-year', component: FiscalYear },
  { path: '/fiscal-year/:id', component: CreateFiscalYear },

  { path: '/periods/:parent', component: Periods },
  { path: '/periods/:parent/:id', component: CreatePeriods },
];

const router = new VueRouter({
  routes // forma corta para routes: routes
});

export default router;