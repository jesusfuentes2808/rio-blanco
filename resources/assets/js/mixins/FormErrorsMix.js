export default{
  data: () => {
    return {
      errors: {}
    };
  },

  methods: {
    setErrors(errors){
      this.errors = {};
      
  		for (let errorName in errors){
        Object.defineProperty(this.errors, errorName, {value: errors[errorName][0]});
      }
  	}
  }
}