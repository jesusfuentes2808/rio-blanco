export default{
  methods: {
    getState(state, states){
    	let s = states.filter(element => element.id == state);

    	return s[0];
    },

    getProvince(state, states, province){
      let s = this.getState(state, states);

    	let p = s.provinces.filter(element => element.id == province);

    	return p[0];
    },

    getDistrict(state, states, province, district, districts){
        let p = this.getProvince(state, states, province);

    	let d = p.districts.filter(element => element.id == district);

    	return d[0];
    },

    getTypeDirection(type, types){
        let t = types.filter(element => element.id == type);

        return t[0];
    },

    getBank(bank, banks){
      let b = banks.filter(element => element.id == bank);

      return b[0];
    },

    getTypeAccount(type, accounts){
      let t = accounts.filter(element => element.id == type);

      return t[0];
    },

    getCurrency(currency, currencies){
      let c = currencies.filter(element => element.id == currency);

      return c[0];
    }
  }
}