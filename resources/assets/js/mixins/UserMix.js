import axios from 'axios';

export default{
	data: () => {
		return {
			user: null
		};
	},
	created(){
		axios.post('/auth-user').then((response) => {
			this.user = response.data;
		});
	}
}