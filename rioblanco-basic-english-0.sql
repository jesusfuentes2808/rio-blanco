/*
 Navicat Premium Data Transfer

 Source Server         : RIOBLANCO
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : gestionpymes

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 28/11/2017 15:37:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for basic_cost_center
-- ----------------------------
DROP TABLE IF EXISTS `basic_cost_center`;
CREATE TABLE `basic_cost_center`  (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(11) UNSIGNED NOT NULL,
  `cost_center_code` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'codigo centro de costos no puede ser menor a 4 digitos',
  `name_cost_center` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Nombre centro de costos',
  `id_um` int(11) UNSIGNED NOT NULL COMMENT 'id unidad de medida',
  `basic_managements_id` int(11) UNSIGNED NOT NULL,
  `basic_section_id` int(3) UNSIGNED NOT NULL,
  `basic_cost_type_id` smallint(3) UNSIGNED NOT NULL,
  `basic_cost_center_type_id` smallint(3) UNSIGNED NOT NULL,
  `supply_other_project` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'suministro a otro proyecto 1 si 0 no',
  `level` smallint(1) UNSIGNED NULL DEFAULT NULL COMMENT 'Es para dar sangria de los nombres',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` smallint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1 activo, 0 inactivo',
  `basic_work_group_id` smallint(3) UNSIGNED NOT NULL,
  `father_id` int(32) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_cost_center_code`(`cost_center_code`) USING BTREE,
  INDEX `fk_basic_cost_center_admin_unidad_medida1_idx`(`id_um`) USING BTREE,
  INDEX `fk_basic_cost_center_admin_proyecto1_idx`(`id_proyecto`) USING BTREE,
  INDEX `fk_basic_cost_center_basic_managements1_idx`(`basic_managements_id`) USING BTREE,
  INDEX `fk_basic_cost_center_basic_section1_idx`(`basic_section_id`) USING BTREE,
  INDEX `fk_basic_cost_center_basic_work_group1_idx`(`basic_work_group_id`) USING BTREE,
  INDEX `fk_basic_cost_center_basic_cost_type1_idx`(`basic_cost_type_id`) USING BTREE,
  INDEX `fk_basic_cost_center_basic_cost_center_type1_idx`(`basic_cost_center_type_id`) USING BTREE,
  INDEX `fk_basic_cost_center_basic_cost_center1_idx`(`father_id`) USING BTREE,
  CONSTRAINT `fk_basic_cost_center_admin_proyecto1` FOREIGN KEY (`id_proyecto`) REFERENCES `project_project` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fk_basic_cost_center_admin_unidad_medida1` FOREIGN KEY (`id_um`) REFERENCES `basic_unit_measure` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_basic_cost_center_basic_cost_center1` FOREIGN KEY (`father_id`) REFERENCES `basic_cost_center` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_basic_cost_center_basic_cost_center_type1` FOREIGN KEY (`basic_cost_center_type_id`) REFERENCES `basic_cost_center_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_basic_cost_center_basic_cost_type1` FOREIGN KEY (`basic_cost_type_id`) REFERENCES `basic_cost_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_basic_cost_center_basic_managements1` FOREIGN KEY (`basic_managements_id`) REFERENCES `basic_managements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_basic_cost_center_basic_section1` FOREIGN KEY (`basic_section_id`) REFERENCES `basic_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_basic_cost_center_basic_work_group1` FOREIGN KEY (`basic_work_group_id`) REFERENCES `basic_work_group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_cost_center_type
-- ----------------------------
DROP TABLE IF EXISTS `basic_cost_center_type`;
CREATE TABLE `basic_cost_center_type`  (
  `id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'solo puede ser de una letra ejemplo A',
  `name_cc_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'puede ser apoyo equipamento familia ingenieria',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_cost_type
-- ----------------------------
DROP TABLE IF EXISTS `basic_cost_type`;
CREATE TABLE `basic_cost_type`  (
  `id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code_cost_type` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'D' COMMENT 'Solo puede ser D o I',
  `name_cost_type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Solo puede ser 2 tipos Directo o Indirecto',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_managements
-- ----------------------------
DROP TABLE IF EXISTS `basic_managements`;
CREATE TABLE `basic_managements`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name_management` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'nombre de la gerencia',
  `name_position` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'nombre del cargo',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_payment_methods_type
-- ----------------------------
DROP TABLE IF EXISTS `basic_payment_methods_type`;
CREATE TABLE `basic_payment_methods_type`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_payment_methods_type` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'online y offline',
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_payments_conditions
-- ----------------------------
DROP TABLE IF EXISTS `basic_payments_conditions`;
CREATE TABLE `basic_payments_conditions`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_payment_conditions` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Contado Contraentrega Factura 15 dias etc',
  `number_days` int(10) NULL DEFAULT NULL COMMENT 'numero de dias de credito o plazo para pagar',
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_payments_methods
-- ----------------------------
DROP TABLE IF EXISTS `basic_payments_methods`;
CREATE TABLE `basic_payments_methods`  (
  `id` int(3) UNSIGNED NOT NULL,
  `name_payments_methods` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Efectivo Cheque Tarjeta Credito etc',
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `basic_payment_methods_type_id` int(3) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_basic_payments_methods_basic_payment_methods_type1_idx`(`basic_payment_methods_type_id`) USING BTREE,
  CONSTRAINT `fk_basic_payments_methods_basic_payment_methods_type1` FOREIGN KEY (`basic_payment_methods_type_id`) REFERENCES `basic_payment_methods_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_responsible
-- ----------------------------
DROP TABLE IF EXISTS `basic_responsible`;
CREATE TABLE `basic_responsible`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_responsible` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'nombre del responsable',
  `position_responsible` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cargo del responsable',
  `basic_responsible_type_id` tinyint(3) UNSIGNED NOT NULL,
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_basic_responsible_basic_responsible_type1_idx`(`basic_responsible_type_id`) USING BTREE,
  CONSTRAINT `fk_basic_responsible_basic_responsible_type1` FOREIGN KEY (`basic_responsible_type_id`) REFERENCES `basic_responsible_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_responsible_type
-- ----------------------------
DROP TABLE IF EXISTS `basic_responsible_type`;
CREATE TABLE `basic_responsible_type`  (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'E empleado C controlador equipos R responsable S supervisor',
  `name_responsible_type` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Responsable  Abastecedor  Controlador de Equipos   Operador  Conductor  Etc',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_section
-- ----------------------------
DROP TABLE IF EXISTS `basic_section`;
CREATE TABLE `basic_section`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'T1',
  `name_section` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'nombre tramo por ejemplo TRAMO I',
  `create_user_id` int(3) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(3) UNSIGNED NULL DEFAULT NULL,
  `coment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_sub_section
-- ----------------------------
DROP TABLE IF EXISTS `basic_sub_section`;
CREATE TABLE `basic_sub_section`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `basic_section_id` int(3) UNSIGNED NOT NULL,
  `name_subsection` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_basic_sub_section_basic_section1_idx`(`basic_section_id`) USING BTREE,
  CONSTRAINT `fk_basic_sub_section_basic_section1` FOREIGN KEY (`basic_section_id`) REFERENCES `basic_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_turn
-- ----------------------------
DROP TABLE IF EXISTS `basic_turn`;
CREATE TABLE `basic_turn`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'D' COMMENT 'D dia N noche',
  `name_turn` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `coment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_unit_measure
-- ----------------------------
DROP TABLE IF EXISTS `basic_unit_measure`;
CREATE TABLE `basic_unit_measure`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `um_sunat_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'codigo para facturacion electronica definido por la sunat',
  `um_sunat_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'nombre unidad de medida definido por la sunat',
  `um_own_code` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'codigo propio utilizado para facturas o cotizaciones y otros',
  `um_own_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'nombre codigo propio',
  `coment` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'sunat y solo para uso interno',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `um_type` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'referencia mediano grande',
  `basic_um_category_id` int(3) UNSIGNED NULL DEFAULT NULL,
  `rounding_um` decimal(12, 2) NOT NULL COMMENT 'redondeo unidad de medida',
  `factor_um` decimal(12, 12) NOT NULL COMMENT 'factor unidad de medida para conversiones',
  `active` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '1 activo 0 inactivo',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `codigo_um_sunat_UNIQUE`(`um_sunat_code`) USING BTREE,
  UNIQUE INDEX `codigo_um_propio_UNIQUE`(`um_own_code`) USING BTREE,
  INDEX `fk_basic_unit_measure_basic_unit_measure_category1_idx`(`basic_um_category_id`) USING BTREE,
  CONSTRAINT `fk_basic_unit_measure_basic_unit_measure_category1` FOREIGN KEY (`basic_um_category_id`) REFERENCES `basic_unit_measure_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Catalogo Nro 03 Facturacion Electronica' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_unit_measure_category
-- ----------------------------
DROP TABLE IF EXISTS `basic_unit_measure_category`;
CREATE TABLE `basic_unit_measure_category`  (
  `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_um_category` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'unidad peso tiempo trabajo longitud distancia',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_work_front
-- ----------------------------
DROP TABLE IF EXISTS `basic_work_front`;
CREATE TABLE `basic_work_front`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_work_front` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'nombre frente de trabajo o sector',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `coment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `basic_sub_section_id` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_basic_work_front_basic_sub_section1_idx`(`basic_sub_section_id`) USING BTREE,
  CONSTRAINT `fk_basic_work_front_basic_sub_section1` FOREIGN KEY (`basic_sub_section_id`) REFERENCES `basic_sub_section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for basic_work_group
-- ----------------------------
DROP TABLE IF EXISTS `basic_work_group`;
CREATE TABLE `basic_work_group`  (
  `id` smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'Para calculo horas por grupos',
  `name_work_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'HTB Horas Trabajadas',
  `create_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  `create_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `write_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `write_user_id` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Para agrupar horas trabajadas sólo como recurso para reportes' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
