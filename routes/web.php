<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\State;

Route::get('/login', 'HomeController@slogin')->name('login');

Route::post('sign-in', 'HomeController@signIn');

Route::middleware(['auth', 'web'])->group(function (){
	Route::get('/', 'HomeController@index');

	Route::post('/auth-user', 'HomeController@authUser');

	Route::resource('/users', 'UsersController');
	Route::resource('/currencies', 'CurrenciesController');
	Route::get('/companies/parent', 'CompaniesController@allParent');
	Route::resource('/companies', 'CompaniesController');
	Route::resource('/clients', 'ClientsController');
	Route::resource('/banks', 'BanksController');
	
	Route::resource('/type-accounts', 'TypeAccountsController');
	Route::resource('/partner-categories', 'PartnerCategoriesController');
	Route::resource('/documents-type', 'DocumentsTypeController');
	Route::resource('/sales-rate', 'SalesRateController');
	Route::resource('/payment-conditions', 'PaymentConditionController');
	Route::resource('/data-contacts', 'DataContactsController');
	Route::resource('/type-addresses', 'TypeAddressesController');
	Route::resource('/cost-types', 'CostTypesController');

	Route::get('/cost-center/parent', 'CostCenterController@listParent');
	Route::resource('/cost-center', 'CostCenterController');
	Route::resource('/type-cost-center', 'TypeCostCenterController');
	Route::resource('/unit-measure-categories', 'UnitMeasureCategoriesController');
	Route::resource('/responsible-types', 'ResponsibleTypesController');
	Route::resource('/responsibles', 'ResponsiblesController');
	Route::resource('/managements', 'ManagementsController');
	Route::resource('/sections', 'SectionsController');
	Route::resource('/sub-sections', 'SubSectionsController');
	Route::resource('/unit-measures', 'UnitMeasuresController');
	Route::resource('/projects', 'ProjectsController');
	Route::resource('/project-type', 'ProjectTypesController');

	Route::get('/roles/list', 'RoleController@list')->name('roles.list');
	Route::resource('/roles', 'RoleController');

	Route::get('/countries', 'HomeController@countries');

	Route::get('/states', function (){
		return State::orderBy('id', 'ASC')->with(['provinces.districts'])->get();
	});

	Route::get('/menu-horizontal', 'HomeController@menuHorizontal');
	Route::get('/menu-horizontal/{id}', 'HomeController@getMenuVertical');

	Route::post('/client-information', 'HomeController@queryInformation');

	Route::get('/fiscal-year/current','FiscalYearsController@currentYear')->name('fiscal-year.current.year');
	Route::resource('/fiscal-year', 'FiscalYearsController');

	Route::get('/store/{id}', 'PeriodsController@store')->name('periods.temp');
	Route::get('/periods/{id}', 'PeriodsController@getById')->name('periods.getById');
	Route::resource('/periods', 'PeriodsController');

	Route::resource('/work-groups', 'WorkGroupsController');

	Route::resource('/type-adress-zone', 'TypeAddressZoneController');
	Route::resource('/type-adress-street', 'TypeAddressStreetController');


});
